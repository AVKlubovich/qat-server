#include "Common.h"
#include "AddMaskGeneration.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

#include "permissions/UserPermission.h"


RegisterCommand(qat_server::AddMaskGeneration, "add_data")

using namespace qat_server;


AddMaskGeneration::AddMaskGeneration(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> AddMaskGeneration::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto incomingData = _context._packet.body().toMap();
    auto data = incomingData.value("body").toMap();

//    auto listDepartments = permissions::UserPermission::instance().existCommand(data.value("idUser").toLongLong(), signature());
//    if (!listDepartments.count())
//    {
//        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
//                  "Permissions", signature());
//        return QSharedPointer<network::Response>();
//    }
    auto table = data["table"].toString();
    QString sqlInsertQuery;
    QString sqlSelectQuery;
    if (table == "company")
    {
        sqlInsertQuery = QString("INSERT INTO overall_schema.company (name) VALUES ('%1')")
                .arg(data["name"].toString());
        sqlSelectQuery = "SELECT * FROM overall_schema.company";
    }
    if (table == "departments")
    {
        sqlInsertQuery = QString("INSERT INTO overall_schema.departments (name, id_company) VALUES ('%1', %2)")
                .arg(data["name"].toString()).arg(data["idCompany"].toString());
        sqlSelectQuery = "SELECT * FROM overall_schema.departments";
    }
    if (table == "complaints_nature")
    {
        sqlInsertQuery = QString("INSERT INTO complaints_schema.complaints_nature"
                                 " (data_nature, hint_essence, days_to_review, id_company, id_target, id_visiable, cost, type)"
                                 " VALUES ('%1', '%2', %3, %4, %5, 1, %6, %7)")
                .arg(data["name"].toString()).arg(data["essence"].toString()).arg(data["day"].toString())
                .arg(data["idCompany"].toString()).arg(data["idTarget"].toString()).arg(data["cost"].toString()).arg(data["type"].toString());
        sqlSelectQuery = "SELECT * FROM complaints_schema.complaints_nature";
    }
    if (table == "complaints_target")
    {
        sqlInsertQuery = QString("INSERT INTO complaints_schema.complaints_target"
                                 " (data_target) VALUES ('%1')")
                .arg(data["name"].toString());
        sqlSelectQuery = "SELECT * FROM complaints_schema.complaints_target";
    }

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto query = wraper->query();

    query.prepare(sqlInsertQuery);
    const auto insertResult = wraper->execQuery(query);
    if (!insertResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error", signature());
        return network::ResponseShp();
    }

    query.prepare(sqlSelectQuery);
    const auto selectResult = wraper->execQuery(query);
    if (!selectResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error", signature());
        return network::ResponseShp();
    }

    const auto resultList = database::DBHelpers::queryToVariant(query);
    QVariantMap responseMap;
    responseMap[table] = resultList;

    QVariantMap head;
    head["type"] = signature();

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = responseMap;
    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}
