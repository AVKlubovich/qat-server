#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class AddMaskGeneration
        : public core::Command
        , public core::CommandCreator<AddMaskGeneration>
    {
        friend class QSharedPointer<AddMaskGeneration>;

        AddMaskGeneration(const Context& newContext);
    public:
        ~AddMaskGeneration() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
