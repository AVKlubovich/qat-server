#include "Common.h"
#include "DeleteMaskGeneration.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

#include "permissions/UserPermission.h"

RegisterCommand(qat_server::DeleteMaskGeneration, "delete_data")


namespace qat_server
{

    DeleteMaskGeneration::DeleteMaskGeneration(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> DeleteMaskGeneration::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        auto& responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        auto incomingData = _context._packet.body().toMap();
        auto data = incomingData.value("body").toMap();

//        auto listDepartments = permissions::UserPermission::instance().existCommand(data.value("idUser").toLongLong(), signature());
//        if (!listDepartments.count())
//        {
//            sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
//                      "Permissions", signature());
//            return QSharedPointer<network::Response>();
//        }
        auto table = data["table"].toString();
        QString sqlInsertQuery;
        QString sqlSelectQuery;
        if (table == "company")
        {
            sqlInsertQuery = QString("UPDATE overall_schema.company SET \"isVisible\" = 0 WHERE id = %1")
                    .arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM overall_schema.company";
        }
        if (table == "complaints_nature")
        {
            sqlInsertQuery = QString("UPDATE complaints_schema.complaints_nature SET"
                                     " id_visiable = 0 WHERE id = %1")
                    .arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM complaints_schema.complaints_nature";
        }
        if (table == "complaints_target")
        {
            sqlInsertQuery = QString("UPDATE complaints_schema.complaints_target SET \"isVisible\" = 0 WHERE id = %1")
                                     .arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM complaints_schema.complaints_target";
        }
        if (table == "departments")
        {
            sqlInsertQuery = QString("UPDATE overall_schema.departments SET \"isVisible\" = 0 WHERE id = %1")
                                     .arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM overall_schema.departments";
        }

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto query = wraper->query();

        query.prepare(sqlInsertQuery);
        const auto insertResult = wraper->execQuery(query);
        if (!insertResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        query.prepare(sqlSelectQuery);
        const auto selectResult = wraper->execQuery(query);
        if (!selectResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(query);
        QVariantMap responseMap;
        responseMap[table] = resultList;

        QVariantMap head;
        head["type"] = signature();

        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = responseMap;
        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
