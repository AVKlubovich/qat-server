#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class DeleteMaskGeneration
        : public core::Command
        , public core::CommandCreator<DeleteMaskGeneration>
    {
        friend class QSharedPointer<DeleteMaskGeneration>;

        DeleteMaskGeneration(const Context& newContext);
    public:
        ~DeleteMaskGeneration() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
