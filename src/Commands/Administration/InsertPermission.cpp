#include "Common.h"
#include "InsertPermission.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

#include "permissions/UserPermission.h"

RegisterCommand(qat_server::InsertPermission, "save_permissions")


namespace qat_server
{

    InsertPermission::InsertPermission(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> InsertPermission::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        auto& responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        auto incomingData = _context._packet.body().toMap();
        auto data = incomingData.value("body").toMap();

        auto listDepartments = permissions::UserPermission::instance().existCommand(data.value("idUser").toLongLong(), signature());
        if (!listDepartments.count())
        {
            sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                      "Permissions", signature());
            return QSharedPointer<network::Response>();
        }

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto query = wraper->query();

        auto mapCommands = data.value("idCommands").toMap();
        auto listCommands = mapCommands.keys();
        auto listidDepartments = data.value("idsDepartament").toList();

        for (auto idDepartment : listidDepartments)
        {
            for (auto command : listCommands)
            {
                query.prepare("SELECT users_schema.insertpermission(:idUser, :idDepartament, :idCommand)");
                query.bindValue(":idUser", data.value("id_user").toLongLong());
                query.bindValue(":idDepartament", idDepartment);
                query.bindValue(":idCommand", command.toLongLong());

                const auto permissionResult = wraper->execQuery(query);
                if (!permissionResult)
                {
                    // TODO: need to add log
                    qDebug() << __FUNCTION__ << "error:" << query.lastError();
                    sendError(query.lastError().text(), "db_error", signature());
                    //Q_ASSERT(false);
                    return QSharedPointer<network::Response>();
                }
            }

            query.prepare("SELECT id FROM users_schema.commands");
            auto permissionResult = wraper->execQuery(query);
            if (!permissionResult)
            {
                // TODO: need to add log
                qDebug() << __FUNCTION__ << "error:" << query.lastError();
                sendError(query.lastError().text(), "db_error", signature());
                //Q_ASSERT(false);
                return QSharedPointer<network::Response>();
            }
            else
            {
                QStringList listSQLCommands;
                while (query.next()) {
                    listSQLCommands << query.value(0).toString();
                }

                for (auto command : listCommands)
                    for (auto sqlCommand : listSQLCommands)
                        if(command == sqlCommand)
                            listSQLCommands.removeAll(sqlCommand);

                for (auto command : listSQLCommands)
                {
                    auto delQuery = wraper->query();

                    const QString sql = QString("DELETE FROM users_schema.permissions AS p WHERE p.id_user = %1 "
                                                "AND p.id_department=%2 AND p.id_command = %3")
                            .arg(data.value("id_user").toString())
                            .arg(idDepartment.toString())
                            .arg(command);

                    qDebug() << sql;
                    delQuery.prepare(sql);
                    const auto deleteResult = wraper->execQuery(delQuery);
                    if (!deleteResult)
                    {
                        // TODO: need to add log
                        qDebug() << __FUNCTION__ << "error:" << delQuery.lastError();
                        sendError(delQuery.lastError().text(), "db_error", signature());
                        Q_ASSERT(false);
                        return QSharedPointer<network::Response>();
                    }
                }
            }
        }

        auto sQuery = QString(
                    "SELECT id_department, id_command FROM users_schema.permissions WHERE id_user = :idUser"
                    );

        query.prepare(sQuery);
        query.bindValue(":idUser", data.value("id_user").toLongLong());
        if (!wraper->execQuery(query))
        {
            qDebug() << query.lastError();
            Q_ASSERT(true);
        }

        const auto resultList = database::DBHelpers::queryToVariant(query);

        permissions::UserPermission::instance().setUsersPermissions();

        QVariantMap head;
        head["type"] = signature();

        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = resultList;
        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
