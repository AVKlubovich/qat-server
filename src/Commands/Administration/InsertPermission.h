#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class InsertPermission
        : public core::Command
        , public core::CommandCreator<InsertPermission>
    {
        friend class QSharedPointer<InsertPermission>;

        InsertPermission(const Context& newContext);
    public:
        ~InsertPermission() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
