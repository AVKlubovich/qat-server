#include "Common.h"
#include "InsertPermissionNew.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

#include "permissions/UserPermission.h"

RegisterCommand(qat_server::InsertPermissionNew, "save_permissions_new")


namespace qat_server
{

    InsertPermissionNew::InsertPermissionNew(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> InsertPermissionNew::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        auto& responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        auto incomingData = _context._packet.body().toMap();
        auto data = incomingData.value("body").toMap();

        auto listDepartments = permissions::UserPermission::instance().existCommand(data.value("idUser").toLongLong(), signature());
        if (!listDepartments.count())
        {
            sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                      "Permissions", signature());
            return QSharedPointer<network::Response>();
        }

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto query = wraper->query();

        auto mapPermissions = data.value("permissions").toMap();
        auto idUser = data.value("id_user").toULongLong();

        auto sQuery = QString("DELETE FROM users_schema.permissions WHERE id_user=:idUser");
        query.prepare(sQuery);
        query.bindValue(":idUser", idUser);
        if (!wraper->execQuery(query))
        {
            qDebug() << query.lastError();
            Q_ASSERT(true);
        }

        for (auto it = mapPermissions.begin(); it != mapPermissions.end(); ++it)
        {
            auto idDepartment = it.key().toULongLong();
            auto listCommands = it.value().toList();
            for (auto command : listCommands)
            {
                query.prepare("SELECT users_schema.insertpermission(:idUser, :idDepartament, :idCommand)");
                query.bindValue(":idUser", idUser);
                query.bindValue(":idDepartament", idDepartment);
                query.bindValue(":idCommand", command.toLongLong());

                const auto permissionResult = wraper->execQuery(query);
                if (!permissionResult)
                {
                    // TODO: need to add log
                    qDebug() << __FUNCTION__ << "error:" << query.lastError();
                    sendError(query.lastError().text(), "db_error", signature());
                    Q_ASSERT(false);
                    return QSharedPointer<network::Response>();
                }
            }
        }

        permissions::UserPermission::instance().setUsersPermissions();

        QVariantMap head;
        head["type"] = signature();
        head["status"] = 1;

        QVariantMap body;
        body["status"] = 1;

        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = body;
        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
