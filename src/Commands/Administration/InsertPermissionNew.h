#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class InsertPermissionNew
        : public core::Command
        , public core::CommandCreator<InsertPermissionNew>
    {
        friend class QSharedPointer<InsertPermissionNew>;

        InsertPermissionNew(const Context& newContext);
    public:
        ~InsertPermissionNew() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
