#include "Common.h"
#include "UpdateMaskGeneration.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

#include "permissions/UserPermission.h"

RegisterCommand(qat_server::UpdateMaskGeneration, "update_data")


namespace qat_server
{

    UpdateMaskGeneration::UpdateMaskGeneration(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> UpdateMaskGeneration::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        auto& responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        auto incomingData = _context._packet.body().toMap();
        auto data = incomingData.value("body").toMap();

//        auto listDepartments = permissions::UserPermission::instance().existCommand(data.value("idUser").toLongLong(), signature());
//        if (!listDepartments.count())
//        {
//            sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
//                      "Permissions", signature());
//            return QSharedPointer<network::Response>();
//        }
        auto table = data["table"].toString();
        QString sqlInsertQuery;
        QString sqlSelectQuery;
        if (table == "company")
        {
            sqlInsertQuery = QString("UPDATE overall_schema.company SET name = '%1' WHERE id = %2")
                    .arg(data["name"].toString()).arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM overall_schema.company";
        }
        if (table == "complaints_nature")
        {
            sqlInsertQuery = QString("UPDATE complaints_schema.complaints_nature SET"
                                     " data_nature = '%1', hint_essence = '%2', days_to_review = %3,"
                                     " id_company = %4, id_target = %5, cost = %6, type = %7"
                                     " WHERE id = %8")
                    .arg(data["name"].toString())
                    .arg(data["essence"].toString())
                    .arg(data["day"].toString())
                    .arg(data["idCompany"].toString())
                    .arg(data["idTarget"].toString())
                    .arg(data["cost"].toString())
                    .arg(data["type"].toString())
                    .arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM complaints_schema.complaints_nature";
        }
        if (table == "complaints_target")
        {
            sqlInsertQuery = QString("UPDATE complaints_schema.complaints_target SET"
                                     " data_target = '%1' WHERE id = %2")
                    .arg(data["name"].toString()).arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM complaints_schema.complaints_target";
        }
        if (table == "departments")
        {
            sqlInsertQuery = QString("UPDATE overall_schema.departments SET name = '%1', id_company = %2 WHERE id = %3")
                    .arg(data["name"].toString()).arg(data["idCompany"].toString()).arg(data["id"].toString());
            sqlSelectQuery = "SELECT * FROM overall_schema.departments";
        }

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto query = wraper->query();

        query.prepare(sqlInsertQuery);
        const auto insertResult = wraper->execQuery(query);
        if (!insertResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        query.prepare(sqlSelectQuery);
        const auto selectResult = wraper->execQuery(query);
        if (!selectResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(query);
        QVariantMap responseMap;
        responseMap[table] = resultList;

        QVariantMap head;
        head["type"] = signature();

        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = responseMap;
        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
