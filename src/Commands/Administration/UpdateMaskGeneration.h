#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class UpdateMaskGeneration
        : public core::Command
        , public core::CommandCreator<UpdateMaskGeneration>
    {
        friend class QSharedPointer<UpdateMaskGeneration>;

        UpdateMaskGeneration(const Context& newContext);
    public:
        ~UpdateMaskGeneration() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
