#include "Common.h"
#include "Version.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::Version, "get_version")


namespace qat_server
{

    Version::Version(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> Version::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;
        auto & responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto versionQuery = wraper->query();

        versionQuery.prepare("SELECT * FROM overall_schema.info");
        const auto versionResult = wraper->execQuery(versionQuery);
        if (!versionResult)
        {
            qDebug() << __FUNCTION__ << "error:" << versionQuery.lastError() << endl;
            sendError(versionQuery.lastError().text(), "db_error", signature());
            // NOTE: uncomment to debug
            Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(versionQuery);

        QVariantMap head;
        head["type"] = signature();
        head["status"] = 1;

        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = QVariant::fromValue(resultList);

        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
