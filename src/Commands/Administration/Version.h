#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class Version
        : public core::Command
        , public core::CommandCreator<Version>
    {
        friend class QSharedPointer<Version>;

        Version(const Context& newContext);
    public:
        ~Version() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
