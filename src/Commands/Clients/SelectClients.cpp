#include "Common.h"
#include "SelectClients.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::SelectClients, "get_select_clients")


using namespace qat_server;

SelectClients::SelectClients(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectClients::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto incomingData = _context._packet.body().toMap();
    const auto dateStart = incomingData.value("date_start");
    const auto dateEnd = incomingData.value("date_end");

    QVariantMap resultMap;

    if(incomingData.contains("date_start") &&
       incomingData.contains("date_end"))
    {
        /*const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectReviewsQuery = wraper->query();

        selectReviewsQuery.prepare("SELECT * FROM reviews_schema.reviews "
                                  "WHERE reviews_schema.reviews.date_create BETWEEN :dateStart AND :dateEnd");
        selectReviewsQuery.bindValue(":dateStart", dateStart);
        selectReviewsQuery.bindValue(":dateEnd", dateEnd);
        const auto selectReviewsResult = wraper->execQuery(selectReviewsQuery);
        if (!selectReviewsResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << selectReviewsQuery.lastError();
            sendError(selectReviewsQuery.lastError().text(), "db_error", signature());
            //Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(selectReviewsQuery);
        resultMap["reviews"] = QVariant::fromValue(resultList);*/
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
