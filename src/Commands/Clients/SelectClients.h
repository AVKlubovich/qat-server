#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectClients
        : public core::Command
        , public core::CommandCreator<SelectClients>
    {
        friend class QSharedPointer<SelectClients>;

        SelectClients(const Context& newContext);
    public:
        ~SelectClients() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
