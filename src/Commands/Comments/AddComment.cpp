#include "Common.h"
#include "AddComment.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(qat_server::AddComment, "add_comment")


using namespace qat_server;

AddComment::AddComment(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> AddComment::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto idUser = incomingData["id_user"];
    const auto comment = incomingData["comment"];
    const auto schema = incomingData["schema"].toString();
    const auto id = incomingData["id"];

    QString columnId;
    QString table;
    if (schema == "complaints_schema")
    {
        columnId = "id_complaint";
        table = "\"complaints_сomment\"";
    }
    else
    {
        columnId = "id_reviews";
        table = "reviews_comment";
    }


    QString updateCommentsStr =
            "INSERT INTO %1.%2 (%3, comment, date_create, id_user) "
            "VALUES (:id, :comment, now(), :idUser)"
            ;
    const QString sqlQuery = updateCommentsStr
                             .arg(schema)
                             .arg(table)
                             .arg(columnId);

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto updateQuery = wraper->query();

    updateQuery.prepare(sqlQuery);
    updateQuery.bindValue(":id", id);
    updateQuery.bindValue(":comment", comment);
    updateQuery.bindValue(":idUser", idUser);

    const auto updateComplaintResult = wraper->execQuery(updateQuery);
    if (!updateComplaintResult)
    {
        // TODO: db_error
        sendError("updateQuery", "db_error", signature());
        return QSharedPointer<network::Response>();
    }

    addRecordHistoryLog(incomingData);

    QVariantMap head;
    head["type_command"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void AddComment::addRecordHistoryLog(QVariantMap body)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertQuery = wraper->query();

    const auto idUser = body["id_user"];
    const auto comment = body["comment"];
    const auto schema = body["schema"].toString();
    const auto id = body["id"];

    insertQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    insertQuery.bindValue(":idUser", idUser);
    const auto nameUserResult = wraper->execQuery(insertQuery);
    if (!nameUserResult)
    {
        // TODO: db_error
        sendError("name user command add_comment", "db_error", signature());
        return;
    }

    QString nameUser;
    if (insertQuery.first())
        nameUser = insertQuery.value("name").toString();


    QString columnId;
    if (schema == "complaints_schema")
        columnId = "id_complaint";
    else
        columnId = "id_reviews";

    QString strComment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
            " оставил комментарий: " + comment.toString();

    QString updateHistoryLogStr =
            "INSERT INTO %1.\"historyLog\" (%2, id_user, signature, comment) "
            "VALUES (:id, :idUser, :signature, :comment)"
            ;
    const QString sqlQuery = updateHistoryLogStr
                             .arg(schema)
                             .arg(columnId);

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", id);
    insertQuery.bindValue(":idUser", idUser);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", strComment);

    const auto updateComplaintResult = wraper->execQuery(insertQuery);
    if (!updateComplaintResult)
    {
        // TODO: db_error
        sendError("historyLog command add_comment", "db_error", signature());
        return;
    }
}
