#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class AddComment :
            public core::Command,
            public core::CommandCreator<AddComment>
    {
        friend class QSharedPointer<AddComment>;

        AddComment(const Context& newContext);
    public:
        ~AddComment() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(QVariantMap body);
    };

}
