#include "Common.h"
#include "ListHistory.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::ListHistory, "get_history")


using namespace qat_server;


ListHistory::ListHistory(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> ListHistory::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto id = incomingData.value("id").toLongLong();
    const auto schema = incomingData.value("schema").toString();

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectCommentsQuery = wraper->query();

    QString columnId;
    QString table;
    if (schema == "complaints_schema")
    {
        columnId = "complaint";
        table = "";
    }
    else
    {
        columnId = "reviews";
        table = "reviews_comment";
    }

    const auto & sqlComments = QString("SELECT comment"
                                     " FROM %1.\"historyLog\" AS history"
                                     " WHERE history.id_%2 = :id")
            .arg(schema).arg(columnId);

    selectCommentsQuery.prepare(sqlComments);
    selectCommentsQuery.bindValue(":id", id);

    const auto selectCommentsResult = wraper->execQuery(selectCommentsQuery);
    if (!selectCommentsResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectCommentsQuery.lastError();
        sendError(selectCommentsQuery.lastError().text(), "db_error", signature());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto resultList = database::DBHelpers::queryToVariant(selectCommentsQuery);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultList);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
