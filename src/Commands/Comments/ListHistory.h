#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class ListHistory
        : public core::Command
        , public core::CommandCreator<ListHistory>
    {
        friend class QSharedPointer<ListHistory>;

        ListHistory(const Context& newContext);
    public:
        ~ListHistory() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
