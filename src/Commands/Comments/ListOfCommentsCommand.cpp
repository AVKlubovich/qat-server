#include "Common.h"
#include "ListOfCommentsCommand.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::ListOfCommentsCommand, "get_comments")


using namespace qat_server;


ListOfCommentsCommand::ListOfCommentsCommand(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> ListOfCommentsCommand::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto id = incomingData.value("id");
    const auto schema = incomingData.value("schema").toString();

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectCommentsQuery = wraper->query();

    QString columnId;
    QString table;
    if (schema == "complaints_schema")
    {
        columnId = "complaint";
        table = "\"complaints_сomment\"";
    }
    else
    {
        columnId = "reviews";
        table = "reviews_comment";
    }

    const auto sqlComments = QString("SELECT "
                                     " to_char( comments.date_create, 'DD.MM.YYYY hh:mm:ss' ) || ' пользователь ' || users.name || ' добавил комментарий: ' || comments.comment AS comment"
                                     " FROM %1.%2 AS comments"
                                     " INNER JOIN users_schema.users AS users ON (users.id = comments.id_user) "
                                     " WHERE comments.id_%3 = :id")
            .arg(schema).arg(table).arg(columnId);

    qDebug()<< sqlComments;

    selectCommentsQuery.prepare(sqlComments);
    selectCommentsQuery.bindValue(":id", id);

    const auto selectCommentsResult = wraper->execQuery(selectCommentsQuery);
    if (!selectCommentsResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectCommentsQuery.lastError();
        sendError(selectCommentsQuery.lastError().text(), "db_error", signature());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto resultList = database::DBHelpers::queryToVariant(selectCommentsQuery);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultList);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
