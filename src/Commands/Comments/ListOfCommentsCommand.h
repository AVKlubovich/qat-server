#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class ListOfCommentsCommand
        : public core::Command
        , public core::CommandCreator<ListOfCommentsCommand>
    {
        friend class QSharedPointer<ListOfCommentsCommand>;

        ListOfCommentsCommand(const Context& newContext);
    public:
        ~ListOfCommentsCommand() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
