#include "Common.h"
#include "ComplaintChallenge.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "permissions/UserPermission.h"

#include "Definitions.h"

RegisterCommand(qat_server::ComplaintChallenge, "complaint_challenge")


using namespace qat_server;

ComplaintChallenge::ComplaintChallenge(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> ComplaintChallenge::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    if (!incomingData.contains("id") || !incomingData.contains("id_user"))
    {
        sendError("Нет необходимых параметров", "error parametrs", signature());
        return QSharedPointer<network::Response>();
    }

    const auto idComplaint = incomingData.value("id").toULongLong();
    const auto idUser = incomingData.value("id_user").toULongLong();

    const QString updateComplaintStr(
        "UPDATE complaints_schema.complaints "
        "SET "
        "id_type = 1 "
        "WHERE id = :id");


    _wraper = database::DBManager::instance().getDBWraper();
    auto updateComplaintQuery = _wraper->query();
    updateComplaintQuery.prepare(updateComplaintStr);
    updateComplaintQuery.bindValue(":id", idComplaint);

    const auto updateComplaintResult = updateComplaintQuery.exec();
    if (!updateComplaintResult)
    {
        qDebug() << __FUNCTION__ << "error:" << updateComplaintQuery.lastError().text();
        sendError("updateComplaintQuery", "db_error", signature());
        _wraper->rollback();
        return QSharedPointer<network::Response>();
    }

    if (!saveCommentsHistoryLog(idComplaint, idUser))
    {
        _wraper->rollback();
        return network::ResponseShp();
    }

    QVariantMap head;
    QVariantMap result;
    head["type"] = signature();
    head["status"] = 1;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariantMap();
    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool ComplaintChallenge::saveCommentsHistoryLog(const int idComplaint, const int idUser)
{
    auto insertQuery = _wraper->query();
    const QString commentStr = QString("Жалоба обжалована. Статус жалобы изменен на положительный.");

    QString sqlQuery = QString(
                           "INSERT INTO complaints_schema.\"historyLog\" "
                           "(id_complaint, id_user, signature, comment, date_create) "
                           "VALUES (:id, :idUser, :signature, :comment, now())"
                           );
    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", idComplaint);
    insertQuery.bindValue(":idUser", idUser);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", commentStr);

    const auto insertHistoryLogResult = insertQuery.exec();
    if (!insertHistoryLogResult)
    {
        qDebug() << __FUNCTION__ << "error:" << insertQuery.lastError().text();
        sendError("Could not add history log", "db_error", signature());
        return false;
    }
    return true;
}
