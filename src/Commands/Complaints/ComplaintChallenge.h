#pragma once

#include "server-core/Commands/UserCommand.h"


namespace database
{
    class DBWraper;
    typedef QSharedPointer<DBWraper> DBWraperShp;
}

namespace qat_server
{

    class ComplaintChallenge :
            public core::Command,
            public core::CommandCreator<ComplaintChallenge>
    {
        friend class QSharedPointer<ComplaintChallenge>;

        ComplaintChallenge(const Context& newContext);
    public:
        ~ComplaintChallenge() override = default;
        QSharedPointer<network::Response> exec() override;

    private:
        bool saveCommentsHistoryLog(const int idComplaint, const int idUser);

    private:
        database::DBWraperShp _wraper;
    };

}
