#include "Common.h"
#include "InsertComplaint.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange/WebRequest.h"

#include "permissions/UserPermission.h"

RegisterCommand(qat_server::InsertComplaint, "insert_complaint")


using namespace qat_server;

InsertComplaint::InsertComplaint(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> InsertComplaint::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto incomingData = _context._packet.body().toMap();
    auto incomingBody = incomingData["body"].toMap();

    auto listDepartments = permissions::UserPermission::instance().existCommand(incomingBody["idUser"].toLongLong(), signature());
    if (!listDepartments.count())
    {
        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                                 "Permissions", signature());
        return QSharedPointer<network::Response>();
    }

    _wraper = database::DBManager::instance().getDBWraper();
    _wraper->startTransaction();

    quint64 idComplaint;

    if (incomingBody.contains("id_company") &&
        incomingBody.contains("id_order") &&
        incomingBody.contains("id_status") &&
//        incomingBody.contains("id_department") &&
        incomingBody.contains("id_source") &&
        incomingBody.contains("id_nature") &&
        incomingBody.contains("id_user_create") &&
//        incomingBody.contains("id_user") &&
        incomingBody.contains("hint_essence") &&
        incomingBody.contains("id_type"))
    {
        const auto selectDaysToReview = QString(
            "SELECT days_to_review FROM complaints_schema.complaints_nature WHERE id = :idNature");
        auto selectDTRQuery = _wraper->query();
        selectDTRQuery.prepare(selectDaysToReview);
        selectDTRQuery.bindValue(":idNature", incomingBody["id_nature"].toInt());
        if (!selectDTRQuery.exec())
        {
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << selectDTRQuery.lastError();
            sendError(selectDTRQuery.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }
        const auto resultList = database::DBHelpers::queryToVariant(selectDTRQuery);
        if (resultList.isEmpty())
        {
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "Days to review do not load";
            sendError(QObject::tr("Days to review do not load"), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        const auto daysToReview = resultList.first().toMap().value("days_to_review").toString();

        QString columns("INSERT INTO complaints_schema.complaints "
                        "(id_company, id_order, id_status, id_department, id_source, id_nature, date_create, days_to_process, essence, id_type, id_user_create, rating, feedback");
        QString values("VALUES(:id_company, :id_order, :id_status, :id_department, :id_source, :id_nature, localtimestamp, "
                "(select localtimestamp + interval '" + daysToReview + " day'), :hint_essence, :id_type, :id_user_create, :rating, :feedback");
        if (incomingBody.contains("other_user_create"))
        {
            columns += ", other_user_create)";
            values += ", :other_user_create) RETURNING id";
        }
        else
        {
            columns += ")";
            values += ") RETURNING id";
        }
        auto insertComplaintStr = columns + values;

        auto insertComplaintQuery = _wraper->query();
        insertComplaintQuery.prepare(insertComplaintStr);
        insertComplaintQuery.bindValue(":id_company", incomingBody["id_company"]);
        insertComplaintQuery.bindValue(":id_order", incomingBody["id_order"]);
        insertComplaintQuery.bindValue(":id_status", incomingBody["id_status"]);
        insertComplaintQuery.bindValue(":id_department", 1);
        insertComplaintQuery.bindValue(":id_source", incomingBody["id_source"]);
        insertComplaintQuery.bindValue(":id_nature", incomingBody["id_nature"]);
        insertComplaintQuery.bindValue(":hint_essence", incomingBody["hint_essence"]);
        insertComplaintQuery.bindValue(":id_type", incomingBody["id_type"]);
        insertComplaintQuery.bindValue(":id_user_create", incomingBody["id_user_create"]);
        insertComplaintQuery.bindValue(":rating", incomingBody["rating"]);
        insertComplaintQuery.bindValue(":feedback", incomingBody["feedback"]);
        if (incomingBody.contains("other_user_create"))
            insertComplaintQuery.bindValue(":other_user_create", incomingBody["other_user_create"]);

        auto insertComplaintResult = _wraper->execQuery(insertComplaintQuery);
        if (!insertComplaintResult)
        {
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertComplaintQuery.lastError();
            sendError(insertComplaintQuery.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        insertComplaintQuery.first();
        idComplaint = insertComplaintQuery.value(0).toLongLong();

        const auto idOrder = incomingBody["id_order"].toULongLong();
        QVariantMap orderData;
        if (idOrder != 0 &&
           getOrderInfo(incomingBody["id_company"].toULongLong(),
                        incomingBody["id_order"].toULongLong(),
                        orderData))
        {
            if (!saveOrderInfo(idComplaint, orderData))
            {
                qDebug() << "1111111111111111111" << "error";
            }
        }

        insertComplaintStr = QString(
            "INSERT INTO complaints_schema.\"complaints_сomment\" "
            "(id_complaint, comment, date_create) "
            "VALUES(:idCompalint, :comment, localtimestamp)"
            );
        insertComplaintQuery.prepare(insertComplaintStr);
        insertComplaintQuery.bindValue(":idCompalint", idComplaint);
        insertComplaintQuery.bindValue(":comment", incomingBody["comment"]);
        insertComplaintResult = _wraper->execQuery(insertComplaintQuery);
        if (!insertComplaintResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertComplaintQuery.lastError();
            sendError(insertComplaintQuery.lastError().text(), "db_error", signature());
            return QSharedPointer<network::Response>();
        }

        if (incomingBody.contains("review"))
        {
            insertComplaintStr = QString(
                "INSERT INTO complaints_schema.complaint_to_review "
                "(id_complaint, id_review) "
                "VALUES(:idCompalint, :idReview)"
                );
            insertComplaintQuery.prepare(insertComplaintStr);
            insertComplaintQuery.bindValue(":idCompalint", idComplaint);
            insertComplaintQuery.bindValue(":idReview", incomingBody["review"]);
            insertComplaintResult = _wraper->execQuery(insertComplaintQuery);
            if (!insertComplaintResult)
            {
                // TODO: db_error
                _wraper->rollback();
                qDebug() << insertComplaintQuery.lastQuery();
                qDebug() << __FUNCTION__ << "error:" << insertComplaintQuery.lastError();
                sendError(insertComplaintQuery.lastError().text(), "db_error", signature());
                return QSharedPointer<network::Response>();
            }
        }

        // NOTE: Add attachments
        if (incomingBody.contains("attachments"))
        {
            QVariantList attachmentsList = incomingBody["attachments"].toList();
            for (QVariant & fileAttachment : attachmentsList)
            {
                QVariantMap fileAttachmentMap = fileAttachment.toMap();
                QString fileUuid = fileAttachmentMap["uuid"].toString();
                QString fileName = fileAttachmentMap["file_name"].toString();
                int fileSize = fileAttachmentMap["size"].toInt();
                int fileChunks = fileAttachmentMap["chunks"].toInt();
                const auto insertFileQueryStr = QString(
                    "INSERT INTO complaints_schema.complaint_files "
                    "(id_complaint, file_uuid, file_name, file_size, file_chunks, time_create) "
                    "VALUES(:id_complaint, :file_uuid, :file_name, :file_size, :file_chunks, localtimestamp)"
                    );
                auto insertFileQuery = _wraper->query();
                insertFileQuery.prepare(insertFileQueryStr);
                insertFileQuery.bindValue(":id_complaint", idComplaint);
                insertFileQuery.bindValue(":file_uuid", fileUuid);
                insertFileQuery.bindValue(":file_name", fileName);
                insertFileQuery.bindValue(":file_size", fileSize);
                insertFileQuery.bindValue(":file_chunks", fileChunks);
                auto insertFileQueryResult = _wraper->execQuery(insertFileQuery);
                if (!insertFileQueryResult)
                {
                    // TODO: db_error
                    _wraper->rollback();
                    qDebug() << insertFileQuery.lastQuery();
                    qDebug() << __FUNCTION__ << "error:" << insertFileQuery.lastError();
                    sendError(insertFileQuery.lastError().text(), "db_error", signature());
                    return QSharedPointer<network::Response>();
                }
            }
        }

        addRecordHistoryLog(incomingBody, idComplaint);

        if (!_wraper->commit())
        {
            _wraper->rollback();

            qDebug() << __FUNCTION__ << "database error";
            sendError("database error", "db_error", signature());
            return QSharedPointer<network::Response>();
        }
    }
    else
    {
        // TODO: need to add log
        _wraper->rollback();
        qDebug() << __FUNCTION__ << "error: ERROR SEND PARAMETRS!";
        sendError("ERROR SEND PARAMETRS!", "", signature());
        return QSharedPointer<network::Response>();
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["type_command"] = signature();
    body["status"] = 1;
    body["id_complaint"] = idComplaint;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool InsertComplaint::getOrderInfo(const quint64 idCompany, const quint64 idOrder, QVariantMap & outData)
{
    const auto selectUrlStr = QString(
        "SELECT api "
        "FROM overall_schema.company "
        "WHERE id = :companyId");
    auto selectUrl = _wraper->query();
    selectUrl.prepare(selectUrlStr);
    selectUrl.bindValue(":companyId", idCompany);

    const auto selectUrlResult = selectUrl.exec();
    if (!selectUrlResult)
    {
        _wraper->rollback();
        qDebug() << __FUNCTION__ << "error:" << selectUrl.lastError();
        sendError(selectUrl.lastError().text(), "db_error", signature());
        return false;
    }

    const auto resultList = database::DBHelpers::queryToVariant(selectUrl);
    if (resultList.isEmpty())
    {
        _wraper->rollback();
        qDebug() << __FUNCTION__ << "Urls list is empty";
        sendError(QObject::tr("Urls list is empty"), "db_error", signature());
        return false;
    }

    const auto url = resultList.first().toMap().value("api").toString();
    auto webRequest = QSharedPointer<network::WebRequest>::create("get_okk_info_by_order");

    QVariantMap arguments;
    arguments["order_id"] = QString::number(idOrder);
    webRequest->setEncrypt(true);
    webRequest->setArguments(arguments);
    webRequest->setUrl(url);
    webRequest->setCallback(nullptr);

    network::WebRequestManager::instance()->sendRequestCurrentThread(webRequest);

    const auto data = webRequest->reply();
    webRequest->release();

    const auto doc = QJsonDocument::fromJson(data);
    auto jobj = doc.object();
    outData = jobj.toVariantMap();

    return true;
}

bool InsertComplaint::saveOrderInfo(const quint64 idComplaint, const QVariantMap & orderData)
{
    const auto itObj = orderData.find("obj");
    if (itObj == orderData.end())
    {
        qDebug() << "Error: " << "itObj == orderData.end()";
        return false;
    }

    const auto obj = itObj.value().toMap();
    const auto itOperator = obj.find("user_in");
    const auto itLogist = obj.find("user_out");
    const auto itCar = obj.find("auto");
    const auto itDriver = obj.find("driver");
    const auto itOrder = obj.find("order");
    const auto itObjEnd = obj.end();

    if (itOperator == itObjEnd ||
        itLogist == itObjEnd ||
        itCar == itObjEnd ||
        itDriver == itObjEnd ||
        itOrder == itObjEnd)
    {
        qDebug() << "Error: " << "itOperator == itObjEnd || itLogist == itObjEnd || itCar == itObjEnd || itDriver == itObjEnd || itOrder == itObjEnd";
        qDebug() << obj;
        return false;
    }

    const auto operatorObj = itOperator.value().toMap();
    const auto logistObj = itLogist.value().toMap();
    const auto carObj = itCar.value().toMap();
    const auto driverObj = itDriver.value().toMap();
    const auto orderObj = itOrder.value().toMap();

    const auto updateOrderStr = QString(
        "UPDATE complaints_schema.complaints "
        "SET date_order=:date_order "
        "WHERE id=:id_complaint"
        );
    auto updateOrderQuery = _wraper->query();
    updateOrderQuery.prepare(updateOrderStr);
    updateOrderQuery.bindValue(":date_order", orderObj["date"]);
    updateOrderQuery.bindValue(":id_complaint", idComplaint);
    const auto updateOrderQueryResult = _wraper->execQuery(updateOrderQuery);
    if (!updateOrderQueryResult)
    {
        // TODO: db_error
        _wraper->rollback();
        qDebug() << __FUNCTION__ << "error:" << updateOrderQuery.lastError().text();
        return false;
    }

    if (obj.contains("client"))
    {
        const QString& clientNumber = obj["client"].toMap()["phone"].toString();

        const auto updateOrderStr = QString(
            "UPDATE complaints_schema.complaints "
            "SET client_number = :clientNumber "
            "WHERE id = :complaintId"
            );
        auto updateClientNumberQuery = _wraper->query();
        updateClientNumberQuery.prepare(updateOrderStr);
        updateClientNumberQuery.bindValue(":clientNumber", clientNumber);
        updateClientNumberQuery.bindValue(":complaintId", idComplaint);

        if (!updateClientNumberQuery.exec())
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "db_error:" << updateClientNumberQuery.lastError().text();
            return false;
        }
    }

    const auto itIdOperator = operatorObj.find("id");
    if (itIdOperator != operatorObj.end() &&
        itIdOperator.value().toULongLong() != 0)
    {
        const auto insertOperatorStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_operators "
            "(id_complaint, id_worker, fio, phone) "
            "VALUES(:id_complaint, :id, :fio, :phone)"
            );
        auto insertOperatorQuery = _wraper->query();
        insertOperatorQuery.prepare(insertOperatorStr);
        insertOperatorQuery.bindValue(":id_complaint", idComplaint);
        insertOperatorQuery.bindValue(":id", operatorObj["id"]);
        insertOperatorQuery.bindValue(":fio", operatorObj["fio"]);
        insertOperatorQuery.bindValue(":phone", operatorObj["phone"]);
        const auto insertOperatorQueryResult = _wraper->execQuery(insertOperatorQuery);
        if (!insertOperatorQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertOperatorQuery.lastError().text();
            return false;
        }
    }

    const auto itIdLogist = logistObj.find("id");
    if (itIdLogist != logistObj.end() &&
        itIdLogist.value().toULongLong() != 0)
    {
        const auto insertLogistStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_logists "
            "(id_complaint, id_worker, fio, phone) "
            "VALUES(:id_complaint, :id, :fio, :phone)"
            );
        auto insertLogistQuery = _wraper->query();
        insertLogistQuery.prepare(insertLogistStr);
        insertLogistQuery.bindValue(":id_complaint", idComplaint);
        insertLogistQuery.bindValue(":id", logistObj["id"]);
        insertLogistQuery.bindValue(":fio", logistObj["fio"]);
        insertLogistQuery.bindValue(":phone", logistObj["phone"]);
        const auto insertLogistQueryResult = _wraper->execQuery(insertLogistQuery);
        if (!insertLogistQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertLogistQuery.lastError().text();
            return false;
        }
    }

    const auto itIdCar = carObj.find("id");
    if (itIdCar != carObj.end() &&
        itIdCar.value().toULongLong() != 0)
    {
        const auto insertCarStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_cars "
            "(id_complaint, id_car, number, model, color) "
            "VALUES(:id_complaint, :id, :number, :model, :color)"
            );
        auto insertCarQuery = _wraper->query();
        insertCarQuery.prepare(insertCarStr);
        insertCarQuery.bindValue(":id_complaint", idComplaint);
        insertCarQuery.bindValue(":id", carObj["id"]);
        insertCarQuery.bindValue(":number", carObj["number"]);
        insertCarQuery.bindValue(":model", carObj["model"]);
        insertCarQuery.bindValue(":color", carObj["color"]);
        const auto insertCarQueryResult = _wraper->execQuery(insertCarQuery);
        if (!insertCarQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertCarQuery.lastError().text();
            return false;
        }
    }

    const auto itIdDriver = driverObj.find("id");
    if (itIdDriver != driverObj.end() &&
        itIdDriver.value().toULongLong() != 0)
    {
        const auto insertDriverStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_drivers "
            "("
            "id_complaint, "
            "id_worker, "
            "surname, "
            "firstname, "
            "middlename, "
            "date_come, "
            "whose, "
            "id_park, "
            "\"column\", "
            "%1 "
            "%3 "
            "form_type "
            ") "
            "VALUES"
            "("
            ":id_complaint, "
            ":id, "
            ":surname, "
            ":firstname, "
            ":middlename, "
            ":date_come, "
            ":whose, "
            ":id_park, "
            ":column, "
            "%2 "
            "%4 "
            ":form_type "
            ")"
            )
            .arg(driverObj.contains("city") ? "id_city, " : "")
            .arg(driverObj.contains("city") ? ":id_city, " : "")
            .arg(driverObj.contains("rating") ? "rating, rating_count, " : "")
            .arg(driverObj.contains("rating") ? ":rating, :rating_count, " : "");

        auto insertDriverQuery = _wraper->query();
        insertDriverQuery.prepare(insertDriverStr);
        insertDriverQuery.bindValue(":id_complaint", idComplaint);
        insertDriverQuery.bindValue(":id", driverObj["id"]);
        insertDriverQuery.bindValue(":surname", driverObj["family"]);
        insertDriverQuery.bindValue(":firstname", driverObj["name"]);
        insertDriverQuery.bindValue(":middlename", driverObj["sec_fam"]);
        insertDriverQuery.bindValue(":date_come", driverObj["date_come"]);
        insertDriverQuery.bindValue(":whose", driverObj["our"]);
        insertDriverQuery.bindValue(":id_park", driverObj["park"]);
        insertDriverQuery.bindValue(":column", driverObj["column"]);
        if (driverObj.contains("city"))
            insertDriverQuery.bindValue(":id_city", driverObj["city"]);
        insertDriverQuery.bindValue(":form_type", driverObj["form"]);
        if (driverObj.contains("rating"))
        {
            insertDriverQuery.bindValue(":rating", driverObj["rating"]);
            insertDriverQuery.bindValue(":rating_count", driverObj["cnt_rating"]);
        }
        const auto insertDriverQueryResult = _wraper->execQuery(insertDriverQuery);
        if (!insertDriverQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertDriverQuery.lastError().text();
            return false;
        }

        const auto phones = driverObj.value("phones").toList();
        for (const auto phone: phones)
        {
            const auto strPhone = phone.toString();
            if (strPhone.isEmpty())
                continue;

            const auto insertDriverPhoneStr = QString(
                "INSERT INTO complaints_schema.complaints_orders_driver_phones "
                "("
                "id_complaint, "
                "phone"
                ") VALUES("
                ":id_complaint, "
                ":phone"
                ")"
                );
            auto insertDriverPhoneQuery = _wraper->query();
            insertDriverPhoneQuery.prepare(insertDriverPhoneStr);
            insertDriverPhoneQuery.bindValue(":id_complaint", idComplaint);
            insertDriverPhoneQuery.bindValue(":phone", strPhone);
            const auto insertDriverPhoneQueryResult = _wraper->execQuery(insertDriverPhoneQuery);
            if (!insertDriverPhoneQueryResult)
            {
                // TODO: db_error
                _wraper->rollback();
                qDebug() << __FUNCTION__ << "error:" << insertDriverPhoneQuery.lastError().text();
                return false;
            }
        }
    }
    return true;
}

void InsertComplaint::addRecordHistoryLog(QVariantMap body, quint64 idComplaint)
{
    auto insertQuery = _wraper->query();

    const auto idUser = body["idUser"].toLongLong();
    const auto comment = body["comment"];

    insertQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    insertQuery.bindValue(":idUser", idUser);
    const auto nameUserResult = _wraper->execQuery(insertQuery);
    if (!nameUserResult)
    {
        // TODO: db_error
        _wraper->rollback();
        sendError("name user command add_comment", "db_error", signature());
        return;
    }

    QString nameUser;
    if (insertQuery.first())
        nameUser = insertQuery.value("name").toString();

    QStringList listHistoryLog;
    listHistoryLog << QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                      " создал жалобу со статусом: новая";

    if (!comment.toString().isEmpty())
        listHistoryLog << QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                          " оставил комментарий: " + comment.toString();

    if (body.contains("review"))
    {
        const auto strCommentReview = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                                      " создал жалобу № "+ QString::number(idComplaint) +" из отзыва № " + body["review"].toString();

        listHistoryLog <<  QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                           " создал жалобу из отзыва № " + body["review"].toString();

        const auto & sqlQuery = QString(
                "INSERT INTO reviews_schema.\"historyLog\" "
                "(id_reviews, signature, comment) "
                "VALUES (:id, :signature, :comment)");

        insertQuery.prepare(sqlQuery);
        insertQuery.bindValue(":id", body["review"]);
        insertQuery.bindValue(":signature", signature());
        insertQuery.bindValue(":comment", strCommentReview);

        const auto updateReviewResult = _wraper->execQuery(insertQuery);
        if (!updateReviewResult)
        {
            // TODO: db_error
            _wraper->rollback();
            sendError("historyLog command add_comment review", "db_error", signature());
            return;
        }
    }

    for (auto i = 0; i < listHistoryLog.count(); ++i)
    {
        const auto & commentStr = listHistoryLog.at(i);
        QString sqlQuery =
                "INSERT INTO complaints_schema.\"historyLog\" (id_complaint, id_user, signature, comment, date_create) "
                "VALUES (:id, :idUser, :signature, :comment, now())";

        if (i == 0)
            sqlQuery =
                    "INSERT INTO complaints_schema.\"historyLog\" (id_complaint, id_user, signature, comment, date_create, id_status) "
                    "VALUES (:id, :idUser, :signature, :comment, now(), 1)";

        insertQuery.prepare(sqlQuery);
        insertQuery.bindValue(":id", idComplaint);
        insertQuery.bindValue(":idUser", idUser);
        insertQuery.bindValue(":signature", signature());
        insertQuery.bindValue(":comment", commentStr);

        const auto updateComplaintResult = _wraper->execQuery(insertQuery);
        if (!updateComplaintResult)
        {
            // TODO: db_error
            _wraper->rollback();
            sendError("historyLog command add_comment", "db_error", signature());
            return;
        }
    }
}
