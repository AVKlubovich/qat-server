#pragma once

#include "server-core/Commands/UserCommand.h"


namespace database
{
    class DBWraper;
    typedef QSharedPointer<DBWraper> DBWraperShp;
}

namespace qat_server
{

    class InsertComplaint :
            public core::Command,
            public core::CommandCreator<InsertComplaint>
    {
        friend class QSharedPointer<InsertComplaint>;

        InsertComplaint(const Context& newContext);
        bool getOrderInfo(const quint64 idCompany, const quint64 idOrder, QVariantMap & outData);
        bool saveOrderInfo(const quint64 idComplaint, const QVariantMap & orderData);

    public:
        ~InsertComplaint() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(QVariantMap body, quint64 idComplaint);

    private:
        database::DBWraperShp _wraper;
    };

}
