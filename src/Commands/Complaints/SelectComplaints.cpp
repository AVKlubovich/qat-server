#include "Common.h"
#include "SelectComplaints.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::SelectComplaints, "get_select_complaints")


using namespace qat_server;

SelectComplaints::SelectComplaints(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectComplaints::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    auto listDepartments = permissions::UserPermission::instance()
            .existCommand(incomingData["idUser"].toLongLong(), signature());
    if (!listDepartments.count())
    {
        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                                 "Permissions", signature());
        return QSharedPointer<network::Response>();
    }

    const auto dateStart = incomingData.value("date_start");
    const auto dateEnd = incomingData.value("date_end");

    const auto itIdCompany = incomingData.find("id_company");
    const auto itIdOrder = incomingData.find("id_order");

    const auto itNameTable = incomingData.find("table");
    const auto itIdTarget = incomingData.find("id");
    const auto itIdType = incomingData.find("type");

    QVariantMap resultMap;

    if(incomingData.contains("date_start") &&
            incomingData.contains("date_end"))
    {
        const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectComplaintsQuery = wraper->query();

        const auto findDepartment = QString(
                    " AND c.id_department IN (%1)"
                    ).arg(permissions::UserPermission::instance()
                          .stringlistDepartmentUser(incomingData["idUser"].toLongLong()).join(", "));

        auto sql = QString("SELECT * FROM complaints_schema.complaints AS c "
                           "WHERE c.date_create BETWEEN :dateStart AND :dateEnd") + findDepartment;

        if(itIdCompany != incomingData.end() &&
                itIdOrder != incomingData.end())
        {
            sql += " AND c.id_company = :id_company AND c.id_order = :id_order";
        }

        if(itNameTable != incomingData.end() &&
                itIdTarget != incomingData.end() &&
                itIdType != incomingData.end())
        {
            QString type = "1, 2";
            if (itIdType.value().toInt() == 1)
                type = "1";
            else if(itIdType.value().toInt() == 2)
                type = "2";
            sql = QString("SELECT c.* FROM complaints_schema.complaints AS c "
                          "INNER JOIN complaints_schema.complaints_orders_%1 as d ON (d.id_complaint = c.id) "
                          "WHERE (c.date_create BETWEEN :dateStart AND :dateEnd) "
                          "AND d.id = :id AND c.id_type IN (%2)").arg(itNameTable.value().toString()).arg(type)
                    + findDepartment;
        }

        selectComplaintsQuery.prepare(sql);
        selectComplaintsQuery.bindValue(":dateStart", dateStart);
        selectComplaintsQuery.bindValue(":dateEnd", dateEnd);

        if(itIdCompany != incomingData.end() &&
                itIdOrder != incomingData.end())
        {
            selectComplaintsQuery.bindValue(":id_company", itIdCompany.value());
            selectComplaintsQuery.bindValue(":id_order", itIdOrder.value());
        }

        if(itNameTable != incomingData.end() &&
                itIdTarget != incomingData.end() &&
                itIdType != incomingData.end())
        {
            selectComplaintsQuery.bindValue(":id", itIdTarget.value());
        }

        const auto selectComplaintsResult = wraper->execQuery(selectComplaintsQuery);
        if (!selectComplaintsResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << selectComplaintsQuery.lastError();
            sendError(selectComplaintsQuery.lastError().text(), "db_error", signature());
            Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(selectComplaintsQuery);
        resultMap["complaints"] = QVariant::fromValue(resultList);

        QStringList listIdComplaints;
        for (auto element : resultList)
            listIdComplaints << element.toMap()["id"].toString();
        resultMap["complaints_orders_cars"] = selectInfoCar(listIdComplaints);
        resultMap["complaints_orders_driver_phones"] = selectInfoPhones(listIdComplaints);

        for (auto element : selectIdWorker())
        {
            switch (element.toMap()["id"].toInt()) {
            case Driver:
                resultMap["complaints_orders_drivers"] = selectInfoWorker(Driver, dateStart, dateEnd, findDepartment);
                break;
            case Operator:
                resultMap["complaints_orders_operators"] = selectInfoWorker(Operator, dateStart, dateEnd, findDepartment);
                break;
            case Logist:
                resultMap["complaints_orders_logists"] = selectInfoWorker(Logist, dateStart, dateEnd, findDepartment);
                break;
            default:
                break;
            }
        }
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

QVariantList SelectComplaints::selectIdWorker()
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();

    const auto sql = QString("SELECT id FROM complaints_schema.complaints_target");
    selectQuery.prepare(sql);
    const auto selectResult = wraper->execQuery(selectQuery);
    if (!selectResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        Q_ASSERT(false);
    }

    return database::DBHelpers::queryToVariant(selectQuery);
}

QVariantList SelectComplaints::selectInfoWorker(const int idWorker, const QVariant dateStart,
                                                const QVariant dateEnd, const QString & findDepartment)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();

    const auto sqlIdComplaints = QString("SELECT c.id FROM complaints_schema.complaints as c"
                                         " INNER JOIN complaints_schema.complaints_nature AS n ON(c.id_nature = n.id)"
                                         " WHERE n.id_target = :idWorker AND (c.date_create BETWEEN :dateStart AND :dateEnd)")
            + findDepartment;
    selectQuery.prepare(sqlIdComplaints);
    selectQuery.bindValue(":idWorker", idWorker);
    selectQuery.bindValue(":dateStart", dateStart);
    selectQuery.bindValue(":dateEnd", dateEnd);

    const auto selectResult = wraper->execQuery(selectQuery);
    if (!selectResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        Q_ASSERT(false);
    }

    QStringList listIdComplaints;
    for (auto element : database::DBHelpers::queryToVariant(selectQuery))
        listIdComplaints << element.toMap()["id"].toString();

    if (!listIdComplaints.count())
        return QVariantList();

    QString table;
    switch (idWorker) {
    case Driver:
        table = "complaints_orders_drivers";
        break;
    case Operator:
        table = "complaints_orders_operators";
        break;
    case Logist:
        table = "complaints_orders_logists";
        break;
    default:
        break;
    }

    const auto sqlInfo = QString("SELECT * FROM complaints_schema.%1 WHERE id_complaint IN(%2)")
            .arg(table).arg(listIdComplaints.join(", "));

    selectQuery.prepare(sqlInfo);
    const auto selectInfoResult = wraper->execQuery(selectQuery);
    if (!selectInfoResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        Q_ASSERT(false);
    }
    return database::DBHelpers::queryToVariant(selectQuery);
}

QVariantList SelectComplaints::selectInfoCar(QStringList list)
{
    if (!list.count())
        return QVariantList();
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();
    const auto sqlInfo = QString("SELECT * FROM complaints_schema.complaints_orders_cars WHERE id_complaint IN(%1)")
            .arg(list.join(", "));

    selectQuery.prepare(sqlInfo);
    const auto selectInfoResult = wraper->execQuery(selectQuery);
    if (!selectInfoResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        Q_ASSERT(false);
    }
    return database::DBHelpers::queryToVariant(selectQuery);
}

QVariantList SelectComplaints::selectInfoPhones(QStringList list)
{
    if (!list.count())
        return QVariantList();
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();
    const auto sqlInfo = QString("SELECT * FROM complaints_schema.complaints_orders_driver_phones WHERE id_complaint IN(%1)")
            .arg(list.join(", "));

    selectQuery.prepare(sqlInfo);
    const auto selectInfoResult = wraper->execQuery(selectQuery);
    if (!selectInfoResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        Q_ASSERT(false);
    }
    return database::DBHelpers::queryToVariant(selectQuery);
}
