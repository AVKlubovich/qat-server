#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectComplaints
        : public core::Command
        , public core::CommandCreator<SelectComplaints>
    {
        friend class QSharedPointer<SelectComplaints>;

        SelectComplaints(const Context& newContext);
    public:
        enum Worker
        {
            Driver = 1,
            Operator,
            Logist
        };

        ~SelectComplaints() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        QVariantList selectIdWorker();
        QVariantList selectInfoWorker(const int idWorker, const QVariant dateStart,
                                      const QVariant dateEnd, const QString & findDepartment);
        QVariantList selectInfoCar(QStringList list);
        QVariantList selectInfoPhones(QStringList list);
    };

}
