#include "Common.h"
#include "SelectIdsComplaints.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"
#include "permissions/UserPermission.h"

#include "Definitions.h"

RegisterCommand(qat_server::SelectIdsComplaints, "get_select_ids_complaints")


using namespace qat_server;

SelectIdsComplaints::SelectIdsComplaints(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectIdsComplaints::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    QStringList stringListIds;
    const auto listIds  = incomingData.value("list_ids").toList();
    for (auto id : listIds)
        stringListIds << id.toString();


    QVariantMap resultMap;
    const QString &sqlQuery = QString("SELECT c.* FROM complaints_schema.complaints AS c "
                                      "WHERE c.id IN (%1)").arg(stringListIds.join(", "));

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectComplaintsQuery = wraper->query();

    selectComplaintsQuery.prepare(sqlQuery);
    const auto selectComplaintsResult = wraper->execQuery(selectComplaintsQuery);
    if (!selectComplaintsResult)
    {
        qDebug() << __FUNCTION__ << "error:" << selectComplaintsQuery.lastError();
        sendError(selectComplaintsQuery.lastError().text(), "db_error", signature());
        return QSharedPointer<network::Response>();
    }

    const auto resultList = database::DBHelpers::queryToVariant(selectComplaintsQuery);
    resultMap["complaints"] = QVariant::fromValue(resultList);

    QVariantList listInfoCar;
    QVariantList listInfoPhones;

    if (!selectInfoCar(stringListIds, listInfoCar))
        return QSharedPointer<network::Response>();
    resultMap["complaints_orders_cars"] = listInfoCar;

    if (!selectInfoPhones(stringListIds, listInfoPhones))
        return QSharedPointer<network::Response>();
    resultMap["complaints_orders_driver_phones"] = listInfoPhones;

    QVariantList listIdWorkers;
    if (!selectIdWorker(listIdWorkers))
        return QSharedPointer<network::Response>();

    for (auto element : listIdWorkers)
    {
        QVariantList listInfo;
        switch (element.toMap()["id"].toInt()) {
            case Driver:
                if (!selectInfoWorker(Driver,  stringListIds, listInfo))
                    return QSharedPointer<network::Response>();
                resultMap["complaints_orders_drivers"]   = listInfo;
                break;
            case Operator:
                if (!selectInfoWorker(Operator,  stringListIds, listInfo))
                    return QSharedPointer<network::Response>();
                resultMap["complaints_orders_operators"] = listInfo;
                break;
            case Logist:
                if (!selectInfoWorker(Logist,  stringListIds, listInfo))
                    return QSharedPointer<network::Response>();
                resultMap["complaints_orders_logists"]   = listInfo;
                break;
            default:
                break;
        }
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool SelectIdsComplaints::selectIdWorker(QVariantList &listIdWorkers)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();

    const auto sql = QString("SELECT id FROM complaints_schema.complaints_target");
    selectQuery.prepare(sql);
    const auto selectResult = wraper->execQuery(selectQuery);
    if (!selectResult)
    {
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        return false;
    }

    listIdWorkers = database::DBHelpers::queryToVariant(selectQuery);

    return true;
}

bool SelectIdsComplaints::selectInfoWorker(const int idWorker, const QStringList &listIdComplaints, QVariantList &listInfo)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();

    if (!listIdComplaints.count())
    {
        listInfo = QVariantList();
        return true;
    }

    QString table;
    switch (idWorker) {
    case Driver:
        table = "complaints_orders_drivers";
        break;
    case Operator:
        table = "complaints_orders_operators";
        break;
    case Logist:
        table = "complaints_orders_logists";
        break;
    default:
        break;
    }

    const auto sqlInfo = QString("SELECT * FROM complaints_schema.%1 WHERE id_complaint IN (%2)")
            .arg(table).arg(listIdComplaints.join(", "));

    selectQuery.prepare(sqlInfo);
    const auto selectInfoResult = wraper->execQuery(selectQuery);
    if (!selectInfoResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        return false;
    }

    listInfo = database::DBHelpers::queryToVariant(selectQuery);
    return true;
}

bool SelectIdsComplaints::selectInfoCar(const QStringList &list, QVariantList &listInfo)
{
    if (!list.count())
    {
        listInfo = QVariantList();
        return true;
    }
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();
    const auto sqlInfo = QString("SELECT * FROM complaints_schema.complaints_orders_cars WHERE id_complaint IN(%1)")
            .arg(list.join(", "));

    selectQuery.prepare(sqlInfo);
    const auto selectInfoResult = wraper->execQuery(selectQuery);
    if (!selectInfoResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        return false;
    }
    listInfo = database::DBHelpers::queryToVariant(selectQuery);
    return true;
}

bool SelectIdsComplaints::selectInfoPhones(const QStringList &list, QVariantList &listInfo)
{
    if (!list.count())
    {
        listInfo = QVariantList();
        return true;
    }
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();
    const auto sqlInfo = QString("SELECT * FROM complaints_schema.complaints_orders_driver_phones WHERE id_complaint IN (%1)")
            .arg(list.join(", "));

    selectQuery.prepare(sqlInfo);
    const auto selectInfoResult = wraper->execQuery(selectQuery);
    if (!selectInfoResult)
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
        sendError(selectQuery.lastError().text(), "db_error", signature());
        return false;
    }
    listInfo = database::DBHelpers::queryToVariant(selectQuery);
    return true;
}
