#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectIdsComplaints
        : public core::Command
        , public core::CommandCreator<SelectIdsComplaints>
    {
        friend class QSharedPointer<SelectIdsComplaints>;

        SelectIdsComplaints(const Context& newContext);
    public:
        ~SelectIdsComplaints() override = default;
        QSharedPointer<network::Response> exec() override;

    private:
        bool selectIdWorker(QVariantList &listIdWorkers);
        bool selectInfoWorker(const int idWorker, const QStringList &listIdComplaints, QVariantList &listInfo);
        bool selectInfoCar(const QStringList &list, QVariantList &listInfo);
        bool selectInfoPhones(const QStringList &list, QVariantList &listInfo);
    };

}
