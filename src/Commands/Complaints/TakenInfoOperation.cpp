#include "Common.h"
#include "TakenInfoOperation.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::TakenInfoOperation, "taken_info_operation")


using namespace qat_server;


TakenInfoOperation::TakenInfoOperation(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> TakenInfoOperation::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    auto listDepartments = permissions::UserPermission::instance().existCommand(incomingData["idUser"].toLongLong(), signature());
    if (!listDepartments.count())
    {
        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                  "Permissions", signature());
        return QSharedPointer<network::Response>();
    }

    QVariantMap resultMap;
    QVariantMap result;
    QVariantMap head;

    head["type_command"] = signature();

    if(!incomingData.contains("id") &&
            !incomingData.contains("id_user") &&
            !incomingData.contains("work"))
    {
        head["status"] = "-1";
    }
    else
    {
        const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectComplaintsQuery = wraper->query();

        selectComplaintsQuery.prepare("UPDATE complaints_schema.complaints SET \"is_work\"=:work WHERE id=:id");
        selectComplaintsQuery.bindValue(":work", incomingData.value("work"));
        selectComplaintsQuery.bindValue(":id", incomingData.value("id"));
        const auto selectComplaintsResult = wraper->execQuery(selectComplaintsQuery);
        if (!selectComplaintsResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << selectComplaintsQuery.lastError();
            sendError(selectComplaintsQuery.lastError().text(), "db_error", signature());
            //Q_ASSERT(false);

            return QSharedPointer<network::Response>();
        }

        addRecordHistoryLog(incomingData);
    }

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);
    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void TakenInfoOperation::addRecordHistoryLog(QVariantMap body)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertQuery = wraper->query();

    const auto idUser = body["idUser"];
    const auto work = body["work"].toBool();
    const auto idComplaint = body["id"];

    insertQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    insertQuery.bindValue(":idUser", idUser);
    const auto nameUserResult = wraper->execQuery(insertQuery);
    if (!nameUserResult)
    {
        // TODO: db_error
        sendError("name user command add_comment", "db_error", signature());
        return;
    }

    QString nameUser;
    if (insertQuery.first())
        nameUser = insertQuery.value("name").toString();

    QString comment;
    if (work)
        comment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                   " открыл(а) жалобу для редактирования";
    else
        comment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                   " закрыл(а) жалобу для редактирования";

    QString updateHistoryLogStr =
            "INSERT INTO complaints_schema.\"historyLog\" (id_complaint, id_user, signature, comment, date_create) "
            "VALUES (:id, :idUser, :signature, :comment, now())"
            ;
    const QString sqlQuery = updateHistoryLogStr;

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", idComplaint);
    insertQuery.bindValue(":idUser", idUser);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", comment);

    const auto updateComplaintResult = wraper->execQuery(insertQuery);
    if (!updateComplaintResult)
    {
        // TODO: db_error
        sendError("historyLog command take_info_operation", "db_error", signature());
        return;
    }
}
