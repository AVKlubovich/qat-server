#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class TakenInfoOperation :
            public core::Command,
            public core::CommandCreator<TakenInfoOperation>
    {
        friend class QSharedPointer<TakenInfoOperation>;

        TakenInfoOperation(const Context& newContext);
    public:
        ~TakenInfoOperation() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(QVariantMap body);
    };

}
