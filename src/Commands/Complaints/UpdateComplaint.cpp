#include "Common.h"
#include "UpdateComplaint.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange/WebRequest.h"
#include "permissions/UserPermission.h"

#include "Definitions.h"

RegisterCommand(qat_server::UpdateComplaint, "update_complaint")


using namespace qat_server;

UpdateComplaint::UpdateComplaint(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> UpdateComplaint::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    QString errorMsg;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    _wraper = database::DBManager::instance().getDBWraper();
    _wraper->startTransaction();

    const auto& data = _context._packet.body().toMap();
    const auto& incomingData = data["body"].toMap();

    const auto userIdPerm = incomingData["idUser"].toLongLong();
    const auto idComplaint = incomingData.value("id").toULongLong();

    const auto& listDepartments = permissions::UserPermission::instance()
                                  .existCommand(userIdPerm, signature());
    if (listDepartments.isEmpty())
    {
        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                                 "Permissions",
                                 signature());
        _wraper->rollback();
        return QSharedPointer<network::Response>();
    }

    const auto& origState = getCurrentState(idComplaint);
    if (origState.isEmpty())
    {
        sendError("Could not get cirrent state for complaint", "database error", signature());
        _wraper->rollback();
        return network::ResponseShp();
    }

    const auto userId = incomingData["id_user"].toLongLong();
    const auto idCompany = getIdCompany(idComplaint);
    const auto idOrder = incomingData.value("id_order").toULongLong();
    const auto idStatus = getNewStatus(incomingData, origState);
    const auto idDepartment = getNewDepartment(incomingData, origState);

    if (!checkStatus(incomingData, origState))
    {
        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                                 "Permissions",
                                 signature());
        _wraper->rollback();
        return network::ResponseShp();
    }

    QVariantMap result;
    QVariantMap head;

    const QString updateComplaintStr(
        "UPDATE complaints_schema.complaints "
        "SET "
        "id_user = :userId, "
        "id_nature = :natureId, "
        "id_type = :typeId, "
        "id_status = :statusId, "
        "id_department = :departmentId, "
        "is_work = :isWork, "
        "essence = :essence, "
        "id_order = :orderId, "
        "rating = :rating "
        "WHERE id = :id");

    auto updateComplaintQuery = _wraper->query();
    updateComplaintQuery.prepare(updateComplaintStr);
    updateComplaintQuery.bindValue(":userId", userId);
    updateComplaintQuery.bindValue(":natureId", incomingData.value("id_nature"));
    updateComplaintQuery.bindValue(":typeId", incomingData.value("id_type"));
    updateComplaintQuery.bindValue(":statusId", idStatus);
    updateComplaintQuery.bindValue(":departmentId", idDepartment);
    updateComplaintQuery.bindValue(":isWork", incomingData.value("isWork"));
    updateComplaintQuery.bindValue(":essence", incomingData.value("essence").toString());
    updateComplaintQuery.bindValue(":orderId", idOrder);
    updateComplaintQuery.bindValue(":rating", incomingData.value("rating"));
    updateComplaintQuery.bindValue(":id", idComplaint);

    const auto updateComplaintResult = updateComplaintQuery.exec();
    if (!updateComplaintResult)
    {
        qDebug() << __FUNCTION__ << "error:" << updateComplaintQuery.lastError().text();
        sendError("updateComplaintQuery", "db_error", signature());
        _wraper->rollback();
        return QSharedPointer<network::Response>();
    }

    if (!addRecordHistoryLog(incomingData, origState))
    {
        qDebug() << __FUNCTION__ << "error:" << "Could not write history logs";
        sendError("updateComplaintQuery", "db_error", signature());
        _wraper->rollback();
        return QSharedPointer<network::Response>();
    }

    QVariantMap orderData;
    if (idCompany > 0 &&
        idOrder > 0 &&
        idOrder != origState.value("id_order").toInt())
    {
        if (getOrderInfo(idCompany, idOrder, orderData))
        {
            if (!saveOrderInfo(idComplaint, orderData))
            {
                qDebug() << __FUNCTION__ << "error:" << "Do not get order info from remote server";
                errorMsg = QString("Не удалось получить данные о заказе с удалённого сервера");
            }
        }
    }

    if (!incomingData.value("comment").toString().isEmpty())
    {
        const auto& comment = incomingData.value("comment").toString();
        const auto& updateCommentsStr = QString(
            "INSERT INTO complaints_schema.\"complaints_сomment\" (id_complaint, comment, date_create, id_user) "
            "VALUES (:idComplaint, :comment, now(), :idUser)"
            );

        auto updateCommentsQuery = _wraper->query();
        updateCommentsQuery.prepare(updateCommentsStr);
        updateCommentsQuery.bindValue(":idComplaint", idComplaint);
        updateCommentsQuery.bindValue(":comment", comment);
        updateCommentsQuery.bindValue(":idUser", userIdPerm);

        const auto updateCommentsResult = updateCommentsQuery.exec();
        if (!updateCommentsResult)
        {
            qDebug() << __FUNCTION__ << "error:" << updateCommentsQuery.lastError().text();
            sendError("updateComplaintQuery", "db_error", signature());
            _wraper->rollback();
            return QSharedPointer<network::Response>();
        }
    }

    _wraper->commit();

    head["type"] = signature();
    head["status"] = 1;
    result["head"] = QVariant::fromValue(head);
    QVariantMap body;
    if (!errorMsg.isEmpty())
        body["error"] = errorMsg;
    body["id_complaint"] = idComplaint;
    result["body"] = body;
    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void UpdateComplaint::appendSqlParameter(const QString& parameter, QVariantMap& incomingData, QSqlQuery& query, QStringList& sqlParameters)
{
    if (incomingData.contains(parameter))
    {
        sqlParameters.append(parameter + "=:" + parameter);
        query.bindValue(":" + parameter, incomingData.value(parameter));
    }
}

QVariantMap UpdateComplaint::getCurrentState(const quint64 idComplaint)
{
    const auto& selectCurrentStateStr = QString(
        "SELECT * "
        "FROM complaints_schema.complaints "
        "WHERE id = :idComplaint"
        );

    auto selectCurrentStateQuery = _wraper->query();
    selectCurrentStateQuery.prepare(selectCurrentStateStr);
    selectCurrentStateQuery.bindValue(":idComplaint", idComplaint);

    const auto selectCurrentStateResult = selectCurrentStateQuery.exec();
    if (!selectCurrentStateResult)
    {
        qDebug() << __FUNCTION__ << "db_error" << selectCurrentStateQuery.lastError().text();
        sendError(selectCurrentStateQuery.lastError().text(), "db_error", signature());
        _wraper->rollback();
        return QVariantMap();
    }

    const auto& resultMap = database::DBHelpers::queryToVariantMap(selectCurrentStateQuery);
    if (resultMap.isEmpty())
    {
        qDebug() << __FUNCTION__ << "db_error" << "Complaint not found";
        sendError("Complaint not found", "db_error", signature());
        _wraper->rollback();
        return QVariantMap();
    }

    return resultMap.first();
}

bool UpdateComplaint::checkStatus(const QVariantMap &inData, const QVariantMap &origState)
{
    const auto idUserPer = inData["idUser"].toInt();
    const auto inStatus = inData["id_status"].toInt();
    const auto origStatus = origState["id_status"].toInt();
    const auto origDep = origState["id_department"].toInt();

    if (permissions::UserPermission::instance().isQATUser(idUserPer) ||
        permissions::UserPermission::instance().isAdmin(idUserPer))
    {
        if (inStatus == is_closed && origDep != ID_DEP_QAT)
        {
            return false;
        }

        return true;
    }

    if (inStatus == is_closed ||
        inStatus == _new)
    {
        return false;
    }

    if (inStatus < origStatus)
    {
        return false;
    }

    return true;
}

quint64 UpdateComplaint::getNewStatus(const QVariantMap& inData, const QVariantMap& origState)
{
    const auto idUserPer = inData["idUser"].toInt();
    const auto origDep = origState.value("id_department").toInt();
    const auto inDep = inData.value("id_department").toInt();
    const auto origStatus = origState.value("id_status").toInt();
    const auto inStatus = inData.value("id_status").toInt();

    if (inStatus == is_closed &&
        (permissions::UserPermission::instance().isQATUser(idUserPer) ||
         permissions::UserPermission::instance().isAdmin(idUserPer))
        )
    {
        return is_closed;
    }
    else if (inStatus == _new &&
        origDep == ID_DEP_QAT &&
        inDep != ID_DEP_QAT)
    {
        return in_the_work;
    }
    else if ((origStatus == _new || origStatus == in_the_work || origStatus == it_requires_notification) &&
             inDep == ID_DEP_QAT)
    {
        return demolished;
    }
    else
    {
        return inStatus;
    }
}

quint64 UpdateComplaint::getNewDepartment(const QVariantMap& inData, const QVariantMap& origState)
{
    const auto idStatus = inData.value("id_status").toInt();
    if (idStatus == demolished ||
        idStatus == is_closed)
    {
        return ID_DEP_QAT;
    }
    else
    {
        return inData.value("id_department").toInt();
    }
}

bool UpdateComplaint::getOrderInfo(const quint64 idCompany, const quint64 idOrder, QVariantMap& outData)
{
    const auto& selectUrlStr = QString(
        "SELECT api "
        "FROM overall_schema.company "
        "WHERE id = :companyId");

    auto selectUrlQuery = _wraper->query();
    selectUrlQuery.prepare(selectUrlStr);
    selectUrlQuery.bindValue(":companyId", idCompany);

    const auto selectUrlResult = selectUrlQuery.exec();
    if (!selectUrlResult)
    {
        qDebug() << endl << __FUNCTION__ << "error:" << selectUrlQuery.lastError().text();
        sendError(selectUrlQuery.lastError().text(), "db_error", signature());
        _wraper->rollback();
        return false;
    }

    const auto& resultList = database::DBHelpers::queryToVariant(selectUrlQuery);
    if (resultList.isEmpty())
    {
        qDebug() << endl << __FUNCTION__ << "error:" << "Urls list is empty";
        sendError(QObject::tr("Urls list is empty"), "db_error", signature());
        _wraper->rollback();
        return false;
    }

    const auto& url = resultList.first().toMap().value("api").toString();
    auto webRequest = QSharedPointer<network::WebRequest>::create("get_okk_info_by_order");

    QVariantMap arguments;
    arguments["city"] = "0";
    arguments["order_id"] = QString::number(idOrder);
    webRequest->setEncrypt(true);
    webRequest->setArguments(arguments);
    webRequest->setUrl(url);
    webRequest->setCallback(nullptr);

    network::WebRequestManager::instance()->sendRequestCurrentThread(webRequest);

    const auto data = webRequest->reply();
    webRequest->release();

    const auto doc = QJsonDocument::fromJson( data );
    auto jobj = doc.object();
    outData = jobj.toVariantMap();

    return true;
}

bool UpdateComplaint::saveOrderInfo(const quint64 idComplaint, const QVariantMap& orderData)
{
    const auto itObj = orderData.find("obj");
    if (itObj == orderData.end())
        return false;

    const auto& obj = itObj.value().toMap();
    const auto itOperator = obj.find("user_in");
    const auto itLogist = obj.find("user_out");
    const auto itCar = obj.find("auto");
    const auto itDriver = obj.find("driver");
    const auto itOrder = obj.find("order");
    const auto itObjEnd = obj.end();

    if (itOperator == itObjEnd ||
        itLogist == itObjEnd ||
        itCar == itObjEnd ||
        itDriver == itObjEnd ||
        itOrder == itObjEnd)
        return false;

    const auto operatorObj = itOperator.value().toMap();
    const auto logistObj = itLogist.value().toMap();
    const auto carObj = itCar.value().toMap();
    const auto driverObj = itDriver.value().toMap();
    const auto orderObj = itOrder.value().toMap();

    const auto& updateOrderStr = QString(
        "UPDATE complaints_schema.complaints "
        "SET date_order = :date_order "
        "WHERE id = :id_complaint"
        );
    auto updateOrderQuery = _wraper->query();
    updateOrderQuery.prepare(updateOrderStr);
    updateOrderQuery.bindValue(":date_order", orderObj["date"]);
    updateOrderQuery.bindValue(":id_complaint", idComplaint);

    const auto updateOrderQueryResult = updateOrderQuery.exec();
    if (!updateOrderQueryResult)
    {
        qDebug() << endl << __FUNCTION__ << "error:" << updateOrderQuery.lastError().text();
        sendError(QObject::tr("Do not update date_order"), "db_error", signature());
        _wraper->rollback();
        return false;
    }

    if (obj.contains("client"))
    {
        const QString& clientNumber = obj["client"].toMap()["phone"].toString();

        const auto updateOrderStr = QString(
            "UPDATE complaints_schema.complaints "
            "SET client_number = :clientNumber "
            "WHERE id = :complaintId"
            );
        auto updateClientNumberQuery = _wraper->query();
        updateClientNumberQuery.prepare(updateOrderStr);
        updateClientNumberQuery.bindValue(":clientNumber", clientNumber);
        updateClientNumberQuery.bindValue(":complaintId", idComplaint);

        if (!updateClientNumberQuery.exec())
        {
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "db_error:" << updateClientNumberQuery.lastError().text();
            return false;
        }
    }

    const auto itIdOperator = operatorObj.find("id");
    if (itIdOperator != operatorObj.end() &&
        itIdOperator.value().toULongLong() != 0)
    {
        const auto& insertOperatorStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_operators "
            "(id_complaint, id_worker, fio, phone) "
            "VALUES(:id_complaint, :id, :fio, :phone)"
            );
        auto insertOperatorQuery = _wraper->query();
        insertOperatorQuery.prepare(insertOperatorStr);
        insertOperatorQuery.bindValue(":id_complaint", idComplaint);
        insertOperatorQuery.bindValue(":id", operatorObj["id"]);
        insertOperatorQuery.bindValue(":fio", operatorObj["fio"]);
        insertOperatorQuery.bindValue(":phone", operatorObj["phone"]);

        const auto insertOperatorQueryResult = insertOperatorQuery.exec();
        if (!insertOperatorQueryResult)
        {
            qDebug() << endl << __FUNCTION__ << "error:" << insertOperatorQuery.lastError().text();
            sendError(QObject::tr("Do not insert info to complaints_orders_operators"), "db_error", signature());
            _wraper->rollback();
            return false;
        }
    }

    const auto itIdLogist = logistObj.find("id");
    if (itIdLogist != logistObj.end() &&
        itIdLogist.value().toULongLong() != 0)
    {
        const auto& insertLogistStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_logists "
            "(id_complaint, id_worker, fio, phone) "
            "VALUES(:id_complaint, :id, :fio, :phone)"
            );
        auto insertLogistQuery = _wraper->query();
        insertLogistQuery.prepare(insertLogistStr);
        insertLogistQuery.bindValue(":id_complaint", idComplaint);
        insertLogistQuery.bindValue(":id", logistObj["id"]);
        insertLogistQuery.bindValue(":fio", logistObj["fio"]);
        insertLogistQuery.bindValue(":phone", logistObj["phone"]);

        const auto insertLogistQueryResult = insertLogistQuery.exec();
        if (!insertLogistQueryResult)
        {
            qDebug() << endl << __FUNCTION__ << "error:" << insertLogistQuery.lastError().text();
            sendError(QObject::tr("Do not insert info to complaints_orders_logists"), "db_error", signature());
            _wraper->rollback();
            return false;
        }
    }

    const auto itIdCar = carObj.find("id");
    if (itIdCar != carObj.end() &&
        itIdCar.value().toULongLong() != 0)
    {
        const auto& insertCarStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_cars "
            "(id_complaint, id_car, number, model, color) "
            "VALUES(:id_complaint, :id, :number, :model, :color)"
            );
        auto insertCarQuery = _wraper->query();
        insertCarQuery.prepare(insertCarStr);
        insertCarQuery.bindValue(":id_complaint", idComplaint);
        insertCarQuery.bindValue(":id", carObj["id"]);
        insertCarQuery.bindValue(":number", carObj["number"]);
        insertCarQuery.bindValue(":model", carObj["model"]);
        insertCarQuery.bindValue(":color", carObj["color"]);

        const auto insertCarQueryResult = insertCarQuery.exec();
        if (!insertCarQueryResult)
        {
            qDebug() << endl << __FUNCTION__ << "error:" << insertCarQuery.lastError().text();
            sendError(QObject::tr("Do not insert info to complaints_orders_cars"), "db_error", signature());
            _wraper->rollback();
            return false;
        }
    }

    const auto itIdDriver = driverObj.find("id");
    if (itIdDriver != driverObj.end() &&
        itIdDriver.value().toULongLong() != 0)
    {
        const auto& insertDriverStr = QString(
            "INSERT INTO complaints_schema.complaints_orders_drivers "
            "("
            "id_complaint, "
            "id_worker, "
            "surname, "
            "firstname, "
            "middlename, "
            "date_come, "
            "whose, "
            "id_park, "
            "\"column\", "
            "id_city, "
            "form_type, "
            "rating, "
            "rating_count"
            ") VALUES("
            ":id_complaint, "
            ":id, "
            ":surname, "
            ":firstname, "
            ":middlename, "
            ":date_come, "
            ":whose, "
            ":id_park, "
            ":column, "
            ":id_city, "
            ":form_type, "
            ":rating, "
            ":rating_count"
            ")"
            );

        auto insertDriverQuery = _wraper->query();
        insertDriverQuery.prepare(insertDriverStr);
        insertDriverQuery.bindValue(":id_complaint", idComplaint);
        insertDriverQuery.bindValue(":id", driverObj["id"]);
        insertDriverQuery.bindValue(":surname", driverObj["family"]);
        insertDriverQuery.bindValue(":firstname", driverObj["name"]);
        insertDriverQuery.bindValue(":middlename", driverObj["sec_fam"]);
        insertDriverQuery.bindValue(":date_come", driverObj["date_come"]);
        insertDriverQuery.bindValue(":whose", driverObj["our"]);
        insertDriverQuery.bindValue(":id_park", driverObj["park"]);
        insertDriverQuery.bindValue(":column", driverObj["column"]);
        insertDriverQuery.bindValue(":id_city", driverObj.value("city", 0));
        insertDriverQuery.bindValue(":form_type", driverObj["form"]);
        insertDriverQuery.bindValue(":rating", driverObj.value("rating", 0));
        insertDriverQuery.bindValue(":rating_count", driverObj.value("cnt_rating", 0));

        const auto insertDriverQueryResult = insertDriverQuery.exec();
        if (!insertDriverQueryResult)
        {
            qDebug() << endl << __FUNCTION__ << "error:" << insertDriverQuery.lastError().text();
            sendError(QObject::tr("Do not insert info to complaints_orders_drivers"), "db_error", signature());
            _wraper->rollback();
            return false;
        }

        const auto& phones = driverObj.value("phones").toList();
        for (const auto& phone: phones)
        {
            const auto& strPhone = phone.toString();
            if (strPhone.isEmpty())
                continue;

            const auto& insertDriverPhoneStr = QString(
                "INSERT INTO complaints_schema.complaints_orders_driver_phones "
                "("
                "id_complaint, "
                "phone"
                ") VALUES("
                ":id_complaint, "
                ":phone"
                ")"
                );

            auto insertDriverPhoneQuery = _wraper->query();
            insertDriverPhoneQuery.prepare(insertDriverPhoneStr);
            insertDriverPhoneQuery.bindValue(":id_complaint", idComplaint);
            insertDriverPhoneQuery.bindValue(":phone", strPhone);

            const auto insertDriverPhoneQueryResult = insertDriverPhoneQuery.exec();
            if (!insertDriverPhoneQueryResult)
            {
                qDebug() << endl << __FUNCTION__ << "error:" << insertDriverPhoneQuery.lastError().text();
                sendError(QObject::tr("Do not insert info to complaints_orders_driver_phone"), "db_error", signature());
                _wraper->rollback();
                return false;
            }
        }
    }

    return true;
}

quint64 UpdateComplaint::getIdCompany(const quint64 idComplaint)
{
    const auto selectCompanyStr = QString(
        "SELECT id_company "
        "FROM complaints_schema.complaints "
        "WHERE id = :id_complaint");
    
    auto selectCompany = _wraper->query();
    selectCompany.prepare(selectCompanyStr);
    selectCompany.bindValue(":id_complaint", idComplaint);

    const auto selectCompanyResult = selectCompany.exec();
    if (!selectCompanyResult)
    {
        qDebug() << endl << __FUNCTION__ << "error:" << selectCompany.lastError().text();
        sendError(QObject::tr("Could not get id_company out of the table 'complaints'"), "db_error", signature());
        _wraper->rollback();
        return -1;
    }

    const auto& resultList = database::DBHelpers::queryToVariant(selectCompany);
    if (resultList.isEmpty())
    {
        qDebug() << endl << __FUNCTION__ << "error:" << "id_company list is empty";
        sendError(QObject::tr("id_company list is empty"), "db_error", signature());
        _wraper->rollback();
        return -1;
    }

    return resultList.first().toMap().value("id_company").toULongLong();
}

bool UpdateComplaint::addRecordHistoryLog(const QVariantMap& inData, const QVariantMap& origState)
{
    auto selectUserQuery = _wraper->query();

    const auto idUser = inData["idUser"].toULongLong();
    const auto idComplaint = inData["id"].toULongLong();
    const auto& comment = inData["comment"].toString();
    const auto new_id_user = inData["id_user"].toLongLong();
    const auto new_id_nature = inData["id_nature"].toInt();
    const auto new_id_type = inData["id_type"].toInt();
    const auto new_id_status = getNewStatus(inData, origState);
    const auto new_id_department = getNewDepartment(inData, origState);
    const auto new_id_order = inData["id_order"].toLongLong();
    const auto& new_essence = inData["essence"].toString();

    const auto old_id_user = origState["id_user"].toLongLong();
    const auto old_id_nature = origState["id_nature"].toInt();
    const auto old_id_type = origState["id_type"].toInt();
    const auto old_id_status = origState["id_status"].toInt();
    const auto old_id_department = origState["id_department"].toInt();
    const auto old_id_order = origState["id_order"].toLongLong();
    const auto& old_essence = origState["essence"].toString();

    selectUserQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    selectUserQuery.bindValue(":idUser", idUser);
    
    const auto nameUserResult = selectUserQuery.exec();
    if (!nameUserResult)
    {
        qDebug() << __FUNCTION__ << "error:" << selectUserQuery.lastError().text();
        return false;
    }
    
    QString nameUser;
    if (selectUserQuery.first())
        nameUser = selectUserQuery.value("name").toString();

    if (new_id_user != old_id_user)
    {
        const auto& text = nameFunction("users_schema.users", "name", new_id_user);
        const auto& strIdUser = QString("%1 %2 назначил(а) жалобу на %3")
                                .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                                .arg(nameUser)
                                .arg(text);
        const auto saveOk = saveCommentsHistoryLog(strIdUser, idUser, idComplaint);
        if (!saveOk)
            return false;
    }

    if (new_id_nature != old_id_nature)
    {
        const auto& text = nameFunction("complaints_schema.complaints_nature", "data_nature", new_id_nature);
        const auto& strNature = QString("%1 %2 изменил(а) характер жалобы на %3")
                                .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                                .arg(nameUser)
                                .arg(text);
        const auto saveOk = saveCommentsHistoryLog(strNature, idUser, idComplaint);
        if (!saveOk)
            return false;
    }

    if (new_id_department != old_id_department)
    {
        const auto& text = nameFunction("overall_schema.departments", "name", new_id_department);
        const auto& strDepartment = QString("%1 %2 изменил(а) департамент на %3")
                                    .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                                    .arg(nameUser)
                                    .arg(text);
        const auto saveOk = saveCommentsHistoryLog(strDepartment, idUser, idComplaint, new_id_department, 1);
        if (!saveOk)
            return false;
    }

    if (new_id_order != old_id_order)
    {
        const auto& strOrder = QString("%1 %2 изменил(а) номер заказа на %3")
                               .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                               .arg(nameUser)
                               .arg(new_id_order);
        const auto saveOk = saveCommentsHistoryLog(strOrder, idUser, idComplaint);
        if (!saveOk)
            return false;
    }

    if (new_id_status != old_id_status)
    {
        for (auto i = old_id_status + 1; i <= new_id_status; ++i)
        {
            const auto& text = nameFunction("complaints_schema.complaints_status", "name_status", i);
            const auto& strStatus = QString("%1 %2 изменил(а) статус на %3")
                                    .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                                    .arg(nameUser)
                                    .arg(text);
            const auto saveOk = saveCommentsHistoryLog(strStatus, idUser, idComplaint, i, 2);
            if (!saveOk)
                return false;
        }
    }

    if (new_id_type != old_id_type)
    {
        const auto& text = nameFunction("complaints_schema.complaints_type", "name", new_id_type);
        const auto& strType = QString("%1 %2 изменил(а) вид жалобы на %3")
                              .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                              .arg(nameUser)
                              .arg(text);
        const auto saveOk = saveCommentsHistoryLog(strType, idUser, idComplaint);
        if (!saveOk)
            return false;
    }

    if (new_essence != old_essence)
    {
        const auto& strEssence = QString("%1 %2 изменил(а) описание: %3")
                                 .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                                 .arg(nameUser)
                                 .arg(new_essence);
        const auto saveOk = saveCommentsHistoryLog(strEssence, idUser, idComplaint);
        if (!saveOk)
            return false;
    }

    if (!comment.isEmpty())
    {
        const auto& strComment = QString("%1 %2 добавил(а) комментарий: %3")
                                 .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"))
                                 .arg(nameUser)
                                 .arg(comment);
        const auto saveOk = saveCommentsHistoryLog(strComment, idUser, idComplaint);
        if (!saveOk)
            return false;
    }

    return true;
}

bool UpdateComplaint::saveCommentsHistoryLog(const QString& commentStr, const quint64 idUser, const quint64 idComplaint,
                                             const int arg, const int idType)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertQuery = wraper->query();

    QString sqlQuery;
    if (idType == -1)
    {
        sqlQuery = QString(
            "INSERT INTO complaints_schema.\"historyLog\" "
            "(id_complaint, id_user, signature, comment, date_create) "
            "VALUES (:id, :idUser, :signature, :comment, now())"
            );
    }
    else if (idType == 1)
    {
        sqlQuery = QString(
            "INSERT INTO complaints_schema.\"historyLog\" "
            "(id_complaint, id_user, signature, comment, id_department, date_create) "
            "VALUES (:id, :idUser, :signature, :comment, %1, now())")
                   .arg(arg);
    }
    else if (idType == 2)
    {
        sqlQuery = QString(
            "INSERT INTO complaints_schema.\"historyLog\" "
            "(id_complaint, id_user, signature, comment, id_status, date_create) "
            "VALUES (:id, :idUser, :signature, :comment, %2, now())")
                   .arg(arg);
    }

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", idComplaint);
    insertQuery.bindValue(":idUser", idUser);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", commentStr);

    const auto updateComplaintResult = insertQuery.exec();
    if (!updateComplaintResult)
    {
        qDebug() << __FUNCTION__ << "error:" << insertQuery.lastError().text();
        sendError("Could not add history log", "db_error", signature());
        _wraper->rollback();
        return false;
    }

    return true;
}

QString UpdateComplaint::nameFunction(const QString& table, const QString& nameColumn, const quint64 id)
{
    auto insertQuery = _wraper->query();

    const QString& sql = QString("SELECT %1 FROM %2 WHERE id = %3")
                         .arg(nameColumn)
                         .arg(table)
                         .arg(id);

    insertQuery.prepare(sql);

    if (!insertQuery.exec())
    {
        Q_ASSERT(false);
    }

    if (insertQuery.first())
        return insertQuery.value(0).toString();

    return QString();
}
