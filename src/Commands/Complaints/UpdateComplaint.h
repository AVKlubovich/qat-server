#pragma once

#include "server-core/Commands/UserCommand.h"


namespace database
{
    class DBWraper;
    typedef QSharedPointer<DBWraper> DBWraperShp;
}

namespace qat_server
{

    class UpdateComplaint :
            public core::Command,
            public core::CommandCreator<UpdateComplaint>
    {
        friend class QSharedPointer<UpdateComplaint>;

        UpdateComplaint(const Context& newContext);
    public:
        ~UpdateComplaint() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void appendSqlParameter(const QString & parameter, QVariantMap & incomingData, QSqlQuery & query, QStringList & sqlParameters);
        QVariantMap getCurrentState(const quint64 idComplaint);

        bool checkStatus(const QVariantMap& inData, const QVariantMap& origState);
        quint64 getNewStatus(const QVariantMap& inData, const QVariantMap& origState);
        quint64 getNewDepartment(const QVariantMap& inData, const QVariantMap& origState);

        bool getOrderInfo(const quint64 idCompany, const quint64 idOrder, QVariantMap & outData);
        bool saveOrderInfo(const quint64 idComplaint, const QVariantMap & orderData);

        quint64 getIdCompany(const quint64 idComplaint);
        bool addRecordHistoryLog(const QVariantMap& inData, const QVariantMap &origState);
        bool saveCommentsHistoryLog(const QString& commentStr, const quint64 idUser, const quint64 idComplaint,
                                    const int arg = -1, const int idType = -1); // 1 - department, 2 - status

        QString nameFunction(const QString& table, const QString& nameColumn, const quint64 id);

    private:
        database::DBWraperShp _wraper;
    };

}
