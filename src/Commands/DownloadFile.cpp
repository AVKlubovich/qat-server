#include "Common.h"
#include "DownloadFile.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/ResponseGeneric.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(qat_server::DownloadFile, "download_file")


namespace qat_server
{

    DownloadFile::DownloadFile(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> DownloadFile::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        //decode incoming data
        auto incomingData = _context._packet.body();
        QSharedPointer<network::DefaultRequest> requestPtr(new network::DefaultRequest("upload_file", incomingData));

        QVariantMap requestBody = incomingData.toMap()["body"].toMap();
        QString uuid = requestBody["uuid"].toString();
        QString fileName = requestBody["file_name"].toString();
        int chunkId = requestBody["chunk_id"].toInt();

        qDebug() << "Get complaint attachment chunk: " << chunkId << fileName;

        const QString sqlQueryStr = QString("SELECT uuid, file_name, chunk_num, chunk_data "
                                            "FROM complaints_schema.files "
                                            "WHERE uuid = '%1' AND chunk_num = '%2';")
                                    .arg(uuid).arg(chunkId);
        qDebug() << sqlQueryStr;
        QSharedPointer<database::DBWraper> wraper = database::DBManager::instance().getDBWraper();
        QSqlQuery query = wraper->query();
        query.prepare(sqlQueryStr);

        QSharedPointer<network::ResponseGeneric> response;
        QVariantMap resultMap;
        const auto queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            response.reset(new network::ResponseGeneric(requestPtr, network::ResponseGeneric::StatusFail));

            QString errMsg = QString("Database SQL query error: %1").arg(query.lastError().text());
            qDebug() << errMsg;
            response->setMessage(errMsg);
        }
        else
        {
            qDebug() << query.record();
            if (query.next())
            {
                QVariantMap data;
                data["uuid"] = query.value(0).toString();
                data["file_name"] = query.value(1).toString();
                data["chunk_num"] = query.value(2).toInt();
                data["chunk_data"] = query.value(3).toString();
                response.reset(new network::ResponseGeneric(requestPtr, network::ResponseGeneric::StatusSuccess));
                response->setData(data);
            }
        }

        return response;
    }

}
