#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class DownloadFile :
            public core::Command,
            public core::CommandCreator<DownloadFile>
    {
        friend class QSharedPointer<DownloadFile>;

    private:
        DownloadFile(const Context& newContext);
    public:
        ~DownloadFile() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
