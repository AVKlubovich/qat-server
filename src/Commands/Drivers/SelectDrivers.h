#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectDrivers
        : public core::Command
        , public core::CommandCreator<SelectDrivers>
    {
        friend class QSharedPointer<SelectDrivers>;

        SelectDrivers(const Context& newContext);
    public:
        ~SelectDrivers() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
