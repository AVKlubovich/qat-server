#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class DeleteEmailTemplate
        : public core::Command
        , public core::CommandCreator<DeleteEmailTemplate>
    {
        friend class QSharedPointer<DeleteEmailTemplate>;

        DeleteEmailTemplate(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
