#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetEmailTemplates
        : public core::Command
        , public core::CommandCreator<GetEmailTemplates>
    {
        friend class QSharedPointer<GetEmailTemplates>;

        GetEmailTemplates(const Context& newContext);

    public:
        network::ResponseShp exec() override;
    };

}
