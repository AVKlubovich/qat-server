#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class InsertEmailTemplate
        : public core::Command
        , public core::CommandCreator<InsertEmailTemplate>
    {
        friend class QSharedPointer<InsertEmailTemplate>;

        InsertEmailTemplate(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
