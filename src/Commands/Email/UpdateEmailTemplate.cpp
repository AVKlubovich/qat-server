#include "Common.h"
#include "UpdateEmailTemplate.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"


RegisterCommand(qat_server::UpdateEmailTemplate, "update_email_template")

using namespace qat_server;

UpdateEmailTemplate::UpdateEmailTemplate(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp UpdateEmailTemplate::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& data = _context._packet.body().toMap();
    const auto& inBody = data["body"].toMap();

    if (!checkInData(inBody))
    {
        return network::ResponseShp();
    }

    const quint64 emailId = inBody["id"].toInt();
    const QString& description = inBody["description"].toString();
    const QString& messageText = inBody["text_message"].toString();

    const QString& updateSmsTemplateStr = QString(
        "UPDATE overall_schema.email_templates "
        "SET description = :description, text_message = :messageText "
        "WHERE id = :emailId");

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto updateSmsTemplateQuery = wraper->query();
    updateSmsTemplateQuery.prepare(updateSmsTemplateStr);
    updateSmsTemplateQuery.bindValue(":emailId", emailId);
    updateSmsTemplateQuery.bindValue(":description", description);
    updateSmsTemplateQuery.bindValue(":messageText", messageText);

    if (!updateSmsTemplateQuery.exec())
    {
        sendError(updateSmsTemplateQuery.lastError().text(), "db_error", signature());
        qDebug() << __FUNCTION__ << updateSmsTemplateQuery.lastError().text();
        return network::ResponseShp();
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["id"] = emailId;
    body["description"] = description;
    body["text_message"] = messageText;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

bool UpdateEmailTemplate::checkInData(const QVariantMap& inBody)
{
    if (!inBody.contains("id"))
    {
        sendError("Can not find parameter id", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("description"))
    {
        sendError("Can not find parameter description", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("text_message"))
    {
        sendError("Can not find parameter text_message", "parameters_error", signature());
        return false;
    }

    return true;
}
