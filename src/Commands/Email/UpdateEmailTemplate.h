#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class UpdateEmailTemplate
        : public core::Command
        , public core::CommandCreator<UpdateEmailTemplate>
    {
        friend class QSharedPointer<UpdateEmailTemplate>;

        UpdateEmailTemplate(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
