#include "Common.h"
#include "StatisticRatingQat.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::StatisticRatingQat, "statistic_rating_qat")


using namespace qat_server;

StatisticRatingQat::StatisticRatingQat(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> StatisticRatingQat::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    auto body = _context._packet.body().toMap();
    auto incomingData = body["body"].toMap();

    /*
     incomingData parametrs

     count_day - колическтво дней для вычисления
     id_target - на кого получить список
     date_start - дата с которой вести подсчёт
     date_end - дата по которую вести подсчёт
     id_worker - на кого выгрузить таблицу
     */

    const auto idWorker = incomingData["id_worker"].toULongLong();

    QDateTime dateEnd = incomingData["date_end"].toDateTime();
    QDateTime dateStart;

    qint64 countDay;

    if (incomingData.contains("count_day"))
    {
        countDay  = incomingData["count_day"].toLongLong();
        dateStart = dateEnd.addDays(-countDay);
    }
    else
    {
        dateStart = incomingData["date_start"].toDateTime();
        countDay  = abs(dateEnd.daysTo(dateStart));
    }

    QString table;
    switch (idWorker) {
    case Driver:
        table = "complaints_orders_drivers";
        break;
    case Operator:
        table = "complaints_orders_operators";
        break;
    case Logist:
        table = "complaints_orders_logists";
        break;
    default:
        break;
    }

    const QString &query = QString("SELECT "
                                   "sum(c.rating)  AS rating"
                                   ", o.id_worker AS order_id "
                                   "FROM complaints_schema.complaints AS c "
                                   "INNER JOIN complaints_schema.complaints_nature AS n ON (n.id = c.id_nature) "
                                   "INNER JOIN complaints_schema.complaints_target AS t ON (t.id = n.id_target) "
                                   "INNER JOIN complaints_schema.%1 AS o ON (o.id_complaint = c.id) "
                                   "WHERE "
                                   "t.id  = :id_target "
                                   "AND c.date_create BETWEEN :date_start AND :date_end "
                                   "GROUP BY c.rating, order_id").arg(table);

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectQuery = wraper->query();

    selectQuery.prepare(query);
    selectQuery.bindValue(":id_target" , incomingData.value("id_target"));
    selectQuery.bindValue(":date_end"  , dateEnd.toString("yyyy.MM.dd HH:mm:ss"));
    selectQuery.bindValue(":date_start", dateStart.toString("yyyy.MM.dd HH:mm:ss"));

    const auto selectResult = wraper->execQuery(selectQuery);
    if (!selectResult)
    {
        qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError() << endl;
        sendError(selectQuery.lastError().text(), "db_error", signature());
        Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    QVariantMap resultMap;
    const auto resultQuery = database::DBHelpers::queryToVariant(selectQuery);

    for (const auto element : resultQuery)
    {
        const auto map  = element.toMap();
        if (!map.value("rating").isValid())
            continue;

        const QString id_order  = map.value("order_id").toString();
        const float rating      = map.value("rating").toFloat();

        if (!resultMap.contains(id_order))
        {
            resultMap[id_order] = rating;
            continue;
        }

        const float sum_rating = resultMap.value(id_order).toFloat() +rating;
        resultMap[id_order] = sum_rating;
    }

    QMap<QString, QVariant>::iterator it  = resultMap.begin();
    for (it  = resultMap.begin(); it != resultMap.end(); ++it)
        it.value() = QString::number(it.value().toFloat() / countDay, 'f', 2);


    resultMap["type_command"] = signature();

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
