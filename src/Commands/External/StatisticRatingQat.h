#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class StatisticRatingQat
        : public core::Command
        , public core::CommandCreator<StatisticRatingQat>
    {
        friend class QSharedPointer<StatisticRatingQat>;

        StatisticRatingQat(const Context& newContext);

        enum Worker
        {
            Driver = 1,
            Operator,
            Logist
        };

    public:
        ~StatisticRatingQat() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
