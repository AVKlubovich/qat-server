#include "Common.h"
#include "FetchNewData.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "permissions/UserPermission.h"


RegisterCommand(qat_server::FetchNewData, "fetch_new_data")

using namespace qat_server;

FetchNewData::FetchNewData(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> FetchNewData::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto incomingData = _context._packet.body().toMap();
    auto body = incomingData["body"].toMap();

//    auto listDepartments = permissions::UserPermission::instance().existCommand(body["idUser"].toLongLong(), signature());
//    if (!listDepartments.count())
//    {
//        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
//                                 "Permissions", signature());
//        return network::ResponseShp();
//    }

    QVariantMap result;

    if (body.contains("last_fetch_date"))
    {
        QVariantMap resultMap, complaints, reviews;
        selectSqlCommandData("complaints_schema", "complaints_log", body.value("last_fetch_date").toString(), complaints);
        if (complaints.count() > 0)
            resultMap["complaints"] = complaints;

        selectSqlCommandData("reviews_schema", "reviews_log", body.value("last_fetch_date").toString(), reviews);
        if (reviews.count() > 0)
            resultMap["reviews"] = reviews;

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectChangesQuery = wraper->query();
        selectChangesQuery.prepare("SELECT LOCALTIMESTAMP");
        if (!wraper->execQuery(selectChangesQuery))
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << selectChangesQuery.lastError();
            sendError(selectChangesQuery.lastError().text(), "db_error_date", signature());
            return network::ResponseShp();
        }

        selectChangesQuery.first();
        resultMap["last_fetch_date"] = selectChangesQuery.value("timestamp").toString();

        QVariantMap head;
        head["type"] = signature();
        head["status"] = 1;

        result["head"] = QVariant::fromValue(head);
        result["body"] = QVariant::fromValue(resultMap);
    }
    else
    {
        // TODO: need to add log
        qDebug() << __FUNCTION__ << "error: ERROR SEND PARAMETRS!";
        sendError("ERROR SEND PARAMETRS!");
        return network::ResponseShp();
    }

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

void FetchNewData::selectSqlCommandData(const QString & schema, const QString & table, const QString & last_fetch_date, QVariantMap & outData)
{
//    selectSqlCommandData("complaints_schema", "complaints_log", body.value("last_fetch_date").toString(), complaints);
    const auto selectChangesStr = QString() +
        "SELECT record, \"table\", command FROM " + schema + "." + table + " "
        "WHERE record IS NOT NULL AND date_create BETWEEN :date_start AND now() ORDER BY command";

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectChangesQuery = wraper->query();
    selectChangesQuery.prepare(selectChangesStr);
    selectChangesQuery.bindValue(":date_start", last_fetch_date);

    const auto selectChangesResult = wraper->execQuery(selectChangesQuery);
    if (!selectChangesResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << selectChangesQuery.lastError();
        return;
    }
    else
    {
        QMultiMap<QString, QVariantMap> data;
        QList<QVariant> inserted, updated, deleted;
        selectCommandData(selectChangesQuery, data);

        selectInsertedData(schema, data, inserted);
        selectUpdatedData(schema, data, updated);
        selectDeletedData(data, deleted);

        if (inserted.count() > 0)
            outData["insert"] = inserted;
        if (updated.count() > 0)
            outData["update"] = updated;
        if (deleted.count() > 0)
            outData["delete"] = deleted;

        selectChangesQuery.prepare("SELECT LOCALTIMESTAMP");
        if (!wraper->execQuery(selectChangesQuery))
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << selectChangesQuery.lastError();
            return;
        }
    }
}

void FetchNewData::selectCommandData(QSqlQuery & qry, QMultiMap<QString, QVariantMap> & outData)
{
    QSqlRecord rec = qry.record();

    while (qry.next())
    {
        QVariantMap data;

        for (int i = 0; i < rec.count(); ++i)
            data.insert(rec.fieldName(i), qry.value(i).toString());

        outData.insert(qry.value("command").toString().toLower().trimmed(), data);
    }
}

void FetchNewData::selectInsertedData(const QString & schema, const QMultiMap<QString, QVariantMap> & allData, QList<QVariant> & outData)
{
    const QString command = "insert";

    if (!allData.contains(command))
        return;

    QList<QVariantMap> inserted = allData.values(command);

    for (const auto map : inserted)
    {
        if (map.count() == 0)
            continue;

        const auto selectInsertedStr = QString() +
            "SELECT * FROM " + schema + "." + map.value("table").toString() + " WHERE id = :id";

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectInsertedQuery = wraper->query();
        selectInsertedQuery.prepare(selectInsertedStr);
        selectInsertedQuery.bindValue(":id", map.value("record"));

        const auto selectInsertedResult = wraper->execQuery(selectInsertedQuery);
        if (!selectInsertedResult)
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << selectInsertedQuery.lastError();
        }
        else
        {
            QVariantMap record;
            resultToMap(selectInsertedQuery, record);
            if (record.count() > 0)
            {
                QVariantMap obj;
                obj.insert("table", map.value("table").toString());
                obj.insert("data", record);
                outData.append(obj);
            }
        }
    }
}

void FetchNewData::selectUpdatedData(const QString & schema, const QMultiMap<QString, QVariantMap> & allData, QList<QVariant> & outData)
{
    const QString command = "update";

    if (!allData.contains(command))
        return;

    QList<QVariantMap> updated = allData.values(command);

    for (const auto map : updated)
    {
        if (map.count() == 0)
            continue;

        const auto selectUpdatedStr = QString() +
            "SELECT * FROM " + schema + "." + map.value("table").toString() + " WHERE id = :id";

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectUpdatedQuery = wraper->query();
        selectUpdatedQuery.prepare(selectUpdatedStr);
        selectUpdatedQuery.bindValue(":id", map.value("record"));

        const auto selectUpdatedResult = wraper->execQuery(selectUpdatedQuery);
        if (!selectUpdatedResult)
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << selectUpdatedQuery.lastError();
        }
        else
        {
            QVariantMap record;
            resultToMap(selectUpdatedQuery, record);
            if(record.count() > 0)
            {
                QVariantMap condition;
                condition.insert("id", map.value("record").toULongLong());

                QVariantMap obj;
                obj.insert("table", map.value("table").toString());
                obj.insert("data", record);
                obj.insert("condition", condition);
                outData.append(obj);
            }
        }
    }
}

void FetchNewData::selectDeletedData(const QMultiMap<QString, QVariantMap> & allData, QList<QVariant> & outData)
{
    const QString command = "delete";

    if (!allData.contains(command))
        return;

    QList<QVariantMap> deleted = allData.values(command);

    for (const auto map : deleted)
    {
        if (map.count() == 0)
            continue;

        QVariantMap condition;
        condition.insert("id", map.value("record").toULongLong());

        QVariantMap obj;
        obj.insert("table", map.value("table").toString());
        obj.insert("condition", condition);
        outData.append(obj);
    }
}

void FetchNewData::resultToMap(QSqlQuery & qry, QVariantMap & outData)
{
    QSqlRecord rec = qry.record();
    if (!qry.next())
        return;

    for (int i = 0; i < rec.count(); ++i)
        outData.insert(rec.fieldName(i), qry.value(i).toString());
}
