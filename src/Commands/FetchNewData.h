#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class FetchNewData :
            public core::Command,
            public core::CommandCreator<FetchNewData>
    {
        friend class QSharedPointer<FetchNewData>;

        FetchNewData(const Context& newContext);
    public:
        ~FetchNewData() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void selectSqlCommandData(const QString & schema, const QString & table, const QString & last_fetch_date, QVariantMap & outData);
        void selectCommandData(QSqlQuery & qry, QMultiMap<QString, QVariantMap> & outData);
        void selectInsertedData(const QString & schema, const QMultiMap<QString, QVariantMap> & allData, QList<QVariant> & outData);
        void selectUpdatedData(const QString & schema, const QMultiMap<QString, QVariantMap> & allData, QList<QVariant> & outData);
        void selectDeletedData(const QMultiMap<QString, QVariantMap> & allData, QList<QVariant> & outData);
        void resultToMap(QSqlQuery & qry, QVariantMap & outData);
    };

}
