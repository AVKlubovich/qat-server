#include "Common.h"
#include "AddFile.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

RegisterCommand(qat_server::AddFile, "add_file")


using namespace qat_server;

AddFile::AddFile(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> AddFile::exec()
{
    auto packet = _context._packet;
    auto& responce = _context._responce;
    responce->setHeaders(packet.headers());
    auto incomingData = _context._packet.body().toMap();

    if (!incomingData.contains("id_complaint") ||
        !incomingData.contains("file_url") ||
        !incomingData.contains("orig_file_name"))
        return network::ResponseShp();

    const auto idComplaint = incomingData["id_complaint"].toLongLong();
    const auto& fileUrl = incomingData["file_url"].toString();
    const auto& origFileName = incomingData["orig_file_name"].toString();

    const auto& addFileStr = QString(
        "INSERT INTO complaints_schema.new_files "
        "(id_complaint, file_url, orig_file_name)"
        "VALUES"
        "(:idComplaint, :fileUrl, :origFileName)");

    auto& wraper = database::DBManager::instance().getDBWraper();
    auto addFileQuery = wraper->query();
    addFileQuery.prepare(addFileStr);
    addFileQuery.bindValue(":idComplaint", idComplaint);
    addFileQuery.bindValue(":fileUrl", fileUrl);
    addFileQuery.bindValue(":origFileName", origFileName);

    const auto addFileResult = wraper->execQuery(addFileQuery);
    if (!addFileResult)
    {
        qDebug() << "Database error!";
        return network::ResponseShp();
    }

    const auto& insertedIdStr = QString(
        "SELECT * FROM complaints_schema.new_files "
        "WHERE id_complaint = :idComplaint AND file_url = :fileUrl AND orig_file_name = :origFileName");
    auto insertedIdQuery = wraper->query();
    insertedIdQuery.prepare(insertedIdStr);
    insertedIdQuery.bindValue(":idComplaint", idComplaint);
    insertedIdQuery.bindValue(":fileUrl", fileUrl);
    insertedIdQuery.bindValue(":origFileName", origFileName);

    const auto insertedIdResult = wraper->execQuery(insertedIdQuery);
    if (!insertedIdResult)
    {
        qDebug() << "Database error!";
        qDebug() << insertedIdQuery.lastError().text();
        return network::ResponseShp();
    }

    const auto& resultList = database::DBHelpers::queryToVariant(insertedIdQuery);
    auto resultMap = resultList.first().toMap();
    resultMap["type_command"] = signature();

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void AddFile::addRecordHistoryLog(QVariantMap body)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertQuery = wraper->query();

    const auto idUser = body["idUser"];
    const auto fileName = body["origFileName"];
    const auto idComplaint = body["id_complaint"];

    insertQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    insertQuery.bindValue(":idUser", idUser);
    const auto nameUserResult = wraper->execQuery(insertQuery);
    if (!nameUserResult)
    {
        // TODO: db_error
        sendError("name user command add_comment", "db_error", signature());
        return;
    }

    QString nameUser;
    if (insertQuery.first())
        nameUser = insertQuery.value("name").toString();

    QString comment;
    comment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
                   " добавил(а) файл " + fileName.toString();

    QString updateHistoryLogStr =
            "INSERT INTO complaints_schema.\"historyLog\" (id_complaint, id_user, signature, comment, date_create) "
            "VALUES (:id, :idUser, :signature, :comment, now())"
            ;
    const QString sqlQuery = updateHistoryLogStr;

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", idComplaint);
    insertQuery.bindValue(":idUser", idUser);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", comment);

    const auto updateComplaintResult = wraper->execQuery(insertQuery);
    if (!updateComplaintResult)
    {
        // TODO: db_error
        sendError("historyLog command take_info_operation", "db_error", signature());
        return;
    }
}

