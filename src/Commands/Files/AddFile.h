#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class AddFile
        : public core::Command
        , public core::CommandCreator<AddFile>
    {
        friend class QSharedPointer<AddFile>;

        AddFile(const Context& newContext);
    public:
        ~AddFile() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(QVariantMap body);
    };

}
