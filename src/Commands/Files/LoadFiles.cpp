#include "Common.h"
#include "LoadFiles.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

RegisterCommand(qat_server::LoadFiles, "load_files")


using namespace qat_server;

LoadFiles::LoadFiles(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> LoadFiles::exec()
{
    auto packet = _context._packet;
    auto& responce = _context._responce;
    responce->setHeaders(packet.headers());
    auto incomingData = _context._packet.body().toMap()["body"].toMap();

    if (!incomingData.contains("id_complaint"))
    {
        qDebug() << "Field do not sended";
        return network::ResponseShp();
    }

    const auto idComplaint = incomingData["id_complaint"].toLongLong();

    const auto& loadFilesStr = QString(
        "SELECT id, orig_file_name, file_url "
        "FROM complaints_schema.new_files "
        "WHERE id_complaint = :idComplaint");

    auto& wraper = database::DBManager::instance().getDBWraper();
    auto loadFilesQuery = wraper->query();
    loadFilesQuery.prepare(loadFilesStr);
    loadFilesQuery.bindValue(":idComplaint", idComplaint);

    const auto loadFilesResult = wraper->execQuery(loadFilesQuery);
    if (!loadFilesResult)
    {
        qDebug() << "Database error!";
        qDebug() << loadFilesQuery.lastError().text();
        qDebug() << loadFilesQuery.lastQuery();
        return network::ResponseShp();
    }

    const auto& resultList = database::DBHelpers::queryToVariant(loadFilesQuery);

    QVariantMap resultMap;
    resultMap["type_command"] = signature();
    QVariant tmp = QVariant::fromValue(resultList);
    resultMap["files"] = QVariant::fromValue(resultList);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
