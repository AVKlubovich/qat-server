#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class LoadFiles
        : public core::Command
        , public core::CommandCreator<LoadFiles>
    {
        friend class QSharedPointer<LoadFiles>;

        LoadFiles(const Context& newContext);
    public:
        ~LoadFiles() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
