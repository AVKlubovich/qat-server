#include "Common.h"
#include "RemoveFile.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

RegisterCommand(qat_server::RemoveFile, "remove_file")


using namespace qat_server;

RemoveFile::RemoveFile(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> RemoveFile::exec()
{
    auto packet = _context._packet;
    auto& responce = _context._responce;
    responce->setHeaders(packet.headers());
    auto incomingData = _context._packet.body().toMap();

    if (!incomingData.contains("id_file"))
    {
        qDebug() << "Field do not sended";
        return network::ResponseShp();
    }

    auto& wraper = database::DBManager::instance().getDBWraper();
    auto removeFileQuery = wraper->query();
    const auto idFile = incomingData["id_file"].toLongLong();
    const auto idUser = incomingData["idUser"].toLongLong();

    removeFileQuery.prepare("SELECT id_complaint, orig_file_name FROM  complaints_schema.new_files WHERE id = :idFile");
    removeFileQuery.bindValue(":idFile", idFile);
    const auto infoFileResult = wraper->execQuery(removeFileQuery);
    if (!infoFileResult)
    {
        qDebug() << "Database error!";
        return network::ResponseShp();
    }
    const auto& resultList = database::DBHelpers::queryToVariant(removeFileQuery);

    const auto& removeFileStr = QString(
        "DELETE "
        "FROM complaints_schema.new_files "
        "WHERE id = :idFile");

    removeFileQuery.prepare(removeFileStr);
    removeFileQuery.bindValue(":idFile", idFile);

    const auto removeFileResult = wraper->execQuery(removeFileQuery);
    if (!removeFileResult)
    {
        qDebug() << "Database error!";
        return network::ResponseShp();
    }

    addRecordHistoryLog(resultList, idUser);

    QVariantMap resultMap;
    resultMap["type_command"] = signature();

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void RemoveFile::addRecordHistoryLog(QVariantList body, const quint64 idUser)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertQuery = wraper->query();

    QString fileName;
    quint64 idComplaint;

    for (auto elem : body)
    {
        auto map = elem.toMap();
        fileName = map["orig_file_name"].toString();
        idComplaint = map["id_complaint"].toLongLong();
    }

    insertQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    insertQuery.bindValue(":idUser", idUser);
    const auto nameUserResult = wraper->execQuery(insertQuery);
    if (!nameUserResult)
    {
        // TODO: db_error
        sendError("name user command remove_file", "db_error", signature());
        return;
    }

    QString nameUser;
    if (insertQuery.first())
        nameUser = insertQuery.value("name").toString();

    const QString comment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser +
            " удалил(а) файл " + fileName;

    QString updateHistoryLogStr =
            "INSERT INTO complaints_schema.\"historyLog\" (id_complaint, id_user, signature, comment, date_create) "
            "VALUES (:id, :idUser, :signature, :comment, now())"
            ;
    const QString sqlQuery = updateHistoryLogStr;

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", idComplaint);
    insertQuery.bindValue(":idUser", idUser);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", comment);

    const auto updateComplaintResult = wraper->execQuery(insertQuery);
    if (!updateComplaintResult)
    {
        // TODO: db_error
        sendError("historyLog command remove_file", "db_error", signature());
        return;
    }
}
