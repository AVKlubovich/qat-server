#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class RemoveFile
        : public core::Command
        , public core::CommandCreator<RemoveFile>
    {
        friend class QSharedPointer<RemoveFile>;

        RemoveFile(const Context& newContext);
    public:
        ~RemoveFile() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(QVariantList body, const quint64 idUser);
    };

}
