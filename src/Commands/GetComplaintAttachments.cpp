#include "Common.h"
#include "GetComplaintAttachments.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/ResponseGeneric.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/ResponseServerError.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(qat_server::GetComplaintAttachments, "get_complaint_attachments")


namespace qat_server
{

    GetComplaintAttachments::GetComplaintAttachments(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> GetComplaintAttachments::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        // NOTE: decode incoming data
        auto incomingData = _context._packet.body();
        QSharedPointer<network::DefaultRequest> requestPtr(new network::DefaultRequest("get_complaint_attachments", incomingData));
        //requestPtr->fromVariant(incomingData);

        //create command response
        //DefaultResponse(const QString& type, const QVariant& response, const QSharedPointer<Request>& request);
        //QSharedPointer<network::ResponseGeneric> response(new network::ResponseGeneric());

        QVariantMap requestBody = incomingData.toMap()["body"].toMap();
        QString complaintId = requestBody["complaint_id"].toString();

        qDebug() << "Request complaint attachments list for complaint id:" << complaintId;

        // NOTE: create database query
        QSharedPointer<database::DBWraper> wraper = database::DBManager::instance().getDBWraper();
        QSqlQuery query = wraper->query();

        // NOTE: prepare query
        const QString sqlQueryStr = QString("SELECT file_uuid, file_name, file_size, file_chunks, time_create "
                                    "FROM complaints_schema.complaint_files "
                                    "WHERE id_complaint='%1'").arg(complaintId);
        qDebug() << sqlQueryStr;
        query.prepare(sqlQueryStr);

        QSharedPointer<network::ResponseGeneric> response;

        // NOTE: run query
        const auto queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            QString errMsg = QString("Database SQL query error: %1").arg(query.lastError().text());
            qWarning() << errMsg;
            response.reset(new network::ResponseGeneric(requestPtr, network::ResponseGeneric::StatusFail));
            response->setMessage(errMsg);
        }
        else
        {
            QVariantList attachmentsList;
            while (query.next()) {
                QVariantMap attachment;

                attachment["uuid"] = query.value(0).toString();
                attachment["file_name"] = query.value(1).toString();
                attachment["size"] = query.value(2).toString();
                attachment["chunks"] = query.value(3).toString();
                attachment["time_create"] = query.value(3).toString();
                attachmentsList.push_back(attachment);
            }

            response.reset(new network::ResponseGeneric(requestPtr, network::ResponseGeneric::StatusSuccess));
            response->setData(attachmentsList);
        }

        return response;
    }

}
