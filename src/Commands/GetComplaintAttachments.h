#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetComplaintAttachments :
            public core::Command,
            public core::CommandCreator<GetComplaintAttachments>
    {
        friend class QSharedPointer<GetComplaintAttachments>;

        GetComplaintAttachments(const Context& newContext);
    public:
        ~GetComplaintAttachments() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
