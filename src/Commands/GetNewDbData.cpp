#include "Common.h"
#include "GetNewDbData.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::GetNewDbData, "get_new_db_data")


using namespace qat_server;

GetNewDbData::GetNewDbData(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> GetNewDbData::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    auto incomingData = _context._packet.body().toMap();
    auto body = incomingData["body"].toMap();

    QVariantList listDepartments = permissions::UserPermission::instance().existCommand(body["idUser"].toLongLong(), signature());
    if (!listDepartments.count())
    {
        sendPermissionsUserErorr("Нет прав для команды %1. \nОбратитесь к администратору",
                  "Permissions", signature());
        return QSharedPointer<network::Response>();
    }

    QMap<QString, QString> mapTableComplaints;

    // complaints_schema
    mapTableComplaints["complaints"]                       =   "SELECT * FROM complaints_schema.complaints WHERE complaints_schema.complaints.date_create "
                                                               "BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["notification"]                     = "SELECT * FROM complaints_schema.notification";
    mapTableComplaints["complaint_files"]                  = "SELECT * FROM complaints_schema.complaint_files";
    mapTableComplaints["complaints_target"]                = "SELECT * FROM complaints_schema.complaints_target";
    mapTableComplaints["complaints_status"]                = "SELECT * FROM complaints_schema.complaints_status";
    mapTableComplaints["complaints_nature"]                = "SELECT * FROM complaints_schema.complaints_nature";
    mapTableComplaints["complaints_source"]                = "SELECT * FROM complaints_schema.complaints_source";
    mapTableComplaints["complaints_type"]                  = "SELECT * FROM complaints_schema.complaints_type";
    mapTableComplaints["notification_methods"]             = "SELECT * FROM complaints_schema.notification_methods";
    mapTableComplaints["complaint_to_review"]              = "SELECT * FROM complaints_schema.complaint_to_review";
    mapTableComplaints["complaints_orders_cars"]           = "SELECT orders.* FROM complaints_schema.complaints_orders_cars AS orders"
                                                             " INNER JOIN complaints_schema.complaints AS c ON(c.id = orders.id_complaint)"
                                                             " WHERE c.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["complaints_orders_driver_phones"]  = "SELECT orders.* FROM complaints_schema.complaints_orders_driver_phones AS orders"
                                                             " INNER JOIN complaints_schema.complaints AS c ON(c.id = orders.id_complaint)"
                                                             " WHERE c.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["complaints_orders_drivers"]        = "SELECT orders.* FROM complaints_schema.complaints_orders_drivers AS orders"
                                                             " INNER JOIN complaints_schema.complaints AS c ON(c.id = orders.id_complaint)"
                                                             " WHERE c.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["complaints_orders_logists"]        = "SELECT orders.* FROM complaints_schema.complaints_orders_logists AS orders"
                                                             " INNER JOIN complaints_schema.complaints AS c ON(c.id = orders.id_complaint)"
                                                             " WHERE c.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["complaints_orders_operators"]      = "SELECT orders.* FROM complaints_schema.complaints_orders_operators AS orders"
                                                             " INNER JOIN complaints_schema.complaints AS c ON(c.id = orders.id_complaint)"
                                                             " WHERE c.date_create BETWEEN now() - interval '5 day' AND now()";

    // overall_schema
    mapTableComplaints["company"]               =  "SELECT * FROM overall_schema.company";
    mapTableComplaints["customers"]             =  "SELECT * FROM overall_schema.customers";
    mapTableComplaints["employees"]             =  "SELECT * FROM overall_schema.employees";
    mapTableComplaints["departments"]           =  "SELECT * FROM overall_schema.departments";
    mapTableComplaints["parks"]                 =  "SELECT * FROM overall_schema.parks";

    // users_schema
    mapTableComplaints["users"]                 =  "SELECT id, name, id_department, id_company, \"isVisible\" FROM users_schema.users";
    mapTableComplaints["commands"]              =  "SELECT * FROM users_schema.commands";

    // reviews_schema
    mapTableComplaints["reviews"]               =  "SELECT * FROM reviews_schema.reviews WHERE reviews_schema.reviews.date_create "
                                                               "BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["reviews_source"]        =  "SELECT * FROM reviews_schema.reviews_source";
    mapTableComplaints["reviews_answer"]        =  "SELECT * FROM reviews_schema.reviews_answer";

    mapTableComplaints["reviews_files"]         =  "SELECT * FROM reviews_schema.reviews_files";
    mapTableComplaints["reviews_orders_cars"]           = "SELECT orders.* FROM reviews_schema.reviews_orders_cars AS orders"
                                                          " INNER JOIN reviews_schema.reviews AS r ON(r.id = orders.id_review)"
                                                          " WHERE r.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["reviews_orders_driver_phones"]  = "SELECT orders.* FROM reviews_schema.reviews_orders_driver_phones AS orders"
                                                          " INNER JOIN reviews_schema.reviews AS r ON(r.id = orders.id_review)"
                                                          " WHERE r.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["reviews_orders_drivers"]        = "SELECT orders.* FROM reviews_schema.reviews_orders_drivers AS orders"
                                                          " INNER JOIN reviews_schema.reviews AS r ON(r.id = orders.id_review)"
                                                          " WHERE r.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["reviews_orders_logists"]        = "SELECT orders.* FROM reviews_schema.reviews_orders_logists AS orders"
                                                          " INNER JOIN reviews_schema.reviews AS r ON(r.id = orders.id_review)"
                                                          " WHERE r.date_create BETWEEN now() - interval '5 day' AND now()";
    mapTableComplaints["reviews_orders_operators"]      = "SELECT orders.* FROM reviews_schema.reviews_orders_operators AS orders"
                                                          " INNER JOIN reviews_schema.reviews AS r ON(r.id = orders.id_review)"
                                                          " WHERE r.date_create BETWEEN now() - interval '5 day' AND now()";


    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectComplaintsQuery = wraper->query();

    QVariantMap resultMap;

    QMapIterator<QString, QString> iteratorMapTableComplaints(mapTableComplaints);
    while (iteratorMapTableComplaints.hasNext())
    {
        iteratorMapTableComplaints.next();

        selectComplaintsQuery.prepare(iteratorMapTableComplaints.value());
        const auto selectComplaintsResult = wraper->execQuery(selectComplaintsQuery);
        if (!selectComplaintsResult)
        {
            qDebug() << iteratorMapTableComplaints.key() << endl;
            qDebug() << __FUNCTION__ << "error:" << selectComplaintsQuery.lastError() << endl;
            sendError(selectComplaintsQuery.lastError().text(), "db_error", signature());
            // NOTE: uncomment to debug
            Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(selectComplaintsQuery);
        resultMap[iteratorMapTableComplaints.key()] = QVariant::fromValue(resultList);
    }

    resultMap["last_fetch_date"] = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    resultMap["type_command"] = signature();

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
