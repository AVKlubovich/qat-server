#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetNewDbData
        : public core::Command
        , public core::CommandCreator<GetNewDbData>
    {
        friend class QSharedPointer<GetNewDbData>;

        GetNewDbData(const Context& newContext);
    public:
        ~GetNewDbData() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
