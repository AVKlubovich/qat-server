#include "Common.h"
#include "GetNewDbDataProxy.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::GetNewDbDataProxy, "get_new_db_data_proxy")


using namespace qat_server;

GetNewDbDataProxy::GetNewDbDataProxy(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> GetNewDbDataProxy::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    auto incomingData = _context._packet.body().toMap();
    auto body = incomingData["body"].toMap();

//    QVariantList listDepartments = permissions::UserPermission::instance().existCommand(body["idUser"].toLongLong(), signature());
//    if (!listDepartments.count())
//    {
//        sendPermissionsUserErorr("Нет прав для команды %1. \nОбратитесь к администратору",
//                  "Permissions", signature());
//        return QSharedPointer<network::Response>();
//    }

    QMap<QString, QString> mapTableComplaints;

    // complaints_schema
    mapTableComplaints["complaints_target"] = "SELECT * FROM complaints_schema.complaints_target";
    mapTableComplaints["complaints_status"] = "SELECT * FROM complaints_schema.complaints_status";
    mapTableComplaints["complaints_nature"] = "SELECT * FROM complaints_schema.complaints_nature";
    mapTableComplaints["company"] =           "SELECT * FROM overall_schema.company";
    mapTableComplaints["departments"] =       "SELECT * FROM overall_schema.departments";

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectComplaintsQuery = wraper->query();

    QVariantMap resultMap;

    QMapIterator<QString, QString> iteratorMapTableComplaints(mapTableComplaints);
    while (iteratorMapTableComplaints.hasNext())
    {
        iteratorMapTableComplaints.next();

        selectComplaintsQuery.prepare(iteratorMapTableComplaints.value());
        const auto selectComplaintsResult = selectComplaintsQuery.exec();
        if (!selectComplaintsResult)
        {
            qDebug() << iteratorMapTableComplaints.key() << endl;
            qDebug() << "error:" << selectComplaintsQuery.lastError() << endl;
            sendError(selectComplaintsQuery.lastError().text(), "db_error", signature());
            // NOTE: uncomment to debug
            Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        const auto resultList = database::DBHelpers::queryToVariant(selectComplaintsQuery);
        resultMap[iteratorMapTableComplaints.key()] = QVariant::fromValue(resultList);
    }

    resultMap["last_fetch_date"] = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    resultMap["type_command"] = signature();

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
