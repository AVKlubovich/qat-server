#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetNewDbDataProxy
        : public core::Command
        , public core::CommandCreator<GetNewDbDataProxy>
    {
        friend class QSharedPointer<GetNewDbDataProxy>;

        GetNewDbDataProxy(const Context& newContext);
    public:
        ~GetNewDbDataProxy() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
