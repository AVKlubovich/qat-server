#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectLogists
        : public core::Command
        , public core::CommandCreator<SelectLogists>
    {
        friend class QSharedPointer<SelectLogists>;

        SelectLogists(const Context& newContext);
    public:
        ~SelectLogists() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
