#include "Common.h"
#include "SelectOperators.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::SelectOperators, "get_select_operators")


namespace qat_server
{

    SelectOperators::SelectOperators(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> SelectOperators::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        auto& responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        auto incomingData = _context._packet.body().toMap();
        const auto dateStart = incomingData.value("date_start");
        const auto dateEnd = incomingData.value("date_end");

        QVariantMap resultMap;

        if(incomingData.contains("date_start") &&
           incomingData.contains("date_end"))
        {
            const auto wraper = database::DBManager::instance().getDBWraper();
            auto selectQuery = wraper->query();

            selectQuery.prepare("SELECT driver.* FROM complaints_schema.complaints_orders_operators AS driver "
                                "INNER JOIN complaints_schema.complaints AS complaints ON (driver.id_complaint = complaints.id) "
                                "WHERE complaints.date_create BETWEEN :dateStart AND :dateEnd");
            selectQuery.bindValue(":dateStart", dateStart);
            selectQuery.bindValue(":dateEnd", dateEnd);
            const auto selectReviewsResult = wraper->execQuery(selectQuery);
            if (!selectReviewsResult)
            {
                // TODO: need to add log
                qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
                sendError(selectQuery.lastError().text(), "db_error", signature());
                Q_ASSERT(false);
                return QSharedPointer<network::Response>();
            }

            const auto resultList = database::DBHelpers::queryToVariant(selectQuery);
            resultMap["operators"] = QVariant::fromValue(resultList);
        }

        QVariantMap head;
        head["type"] = signature();
        head["status"] = 1;

        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = QVariant::fromValue(resultMap);

        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
