#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectOperators
        : public core::Command
        , public core::CommandCreator<SelectOperators>
    {
        friend class QSharedPointer<SelectOperators>;

        SelectOperators(const Context& newContext);
    public:
        ~SelectOperators() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
