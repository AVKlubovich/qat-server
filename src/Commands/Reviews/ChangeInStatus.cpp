#include "Common.h"
#include "ChangeInStatus.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::ChangeInStatus, "change_in_status")

using namespace qat_server;


ChangeInStatus::ChangeInStatus(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> ChangeInStatus::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    auto listDepartments = permissions::UserPermission::instance().existCommand(incomingData["idUser"].toLongLong(), signature());
    if (!listDepartments.count())
    {
        sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                                 "Permissions", signature());
        return QSharedPointer<network::Response>();
    }

    QVariantMap resultMap;
    QVariantMap result;
    QVariantMap head;

    head["type_command"] = signature();

    if(!incomingData.contains("id") &&
       !incomingData.contains("idStatus"))
    {
        head["status"] = "-1";
    }
    else
    {
        const auto wraper = database::DBManager::instance().getDBWraper();
        auto selectRecallsQuery = wraper->query();

        selectRecallsQuery.prepare("UPDATE reviews_schema.reviews SET id_user=:id_user, id_status=:idStatus WHERE id=:id");
        selectRecallsQuery.bindValue(":id_user", incomingData.value("idUser"));
        selectRecallsQuery.bindValue(":idStatus", incomingData.value("idStatus"));
        selectRecallsQuery.bindValue(":id", incomingData.value("id"));
        const auto selectComplaintsResult = wraper->execQuery(selectRecallsQuery);
        if (!selectComplaintsResult)
        {
            // TODO: need to add log
            qDebug() << __FUNCTION__ << "error:" << selectRecallsQuery.lastError();
            sendError(selectRecallsQuery.lastError().text(), "db_error", signature());
            //Q_ASSERT(false);

            return QSharedPointer<network::Response>();
        }
        addRecordHistoryLog(incomingData);
    }

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);
    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void ChangeInStatus::addRecordHistoryLog(QVariantMap body)
{
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertQuery = wraper->query();

    const auto idUser = body["idUser"];
    const auto id = body["id"];

    insertQuery.prepare("SELECT name FROM users_schema.users WHERE id = :idUser");
    insertQuery.bindValue(":idUser", idUser);
    const auto nameUserResult = wraper->execQuery(insertQuery);
    if (!nameUserResult)
    {
        // TODO: db_error
        wraper->rollback();
        sendError("name user command add_comment", "db_error", signature());
        return;
    }

    QString nameUser;
    if (insertQuery.first())
        nameUser = insertQuery.value("name").toString();

    const auto &comment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + nameUser + " закрыл(а) отзыв";

    const auto & sqlQuery = QString(
            "INSERT INTO reviews_schema.\"historyLog\" "
            "(id_reviews, signature, comment) "
            "VALUES "
            "(:id, :signature, :comment)");

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", id);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", comment);

    const auto Result = wraper->execQuery(insertQuery);
    if (!Result)
    {
        // TODO: db_error
        sendError("historyLog command add_comment", "db_error", signature());
        return;
    }
}

