#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class ChangeInStatus :
            public core::Command,
            public core::CommandCreator<ChangeInStatus>
    {
        friend class QSharedPointer<ChangeInStatus>;

        ChangeInStatus(const Context& newContext);
    public:
        ~ChangeInStatus() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(QVariantMap body);
    };

}
