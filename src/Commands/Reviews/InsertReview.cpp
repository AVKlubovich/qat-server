#include "Common.h"
#include "InsertReview.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange/WebRequest.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::InsertReview, "insert_review")


using namespace qat_server;

InsertReview::InsertReview(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> InsertReview::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

    auto & responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    QVariantMap incomingData;
    incomingData = _context._packet.body().toMap();

    QString comment;
    if (incomingData.contains("comment"))
        comment = incomingData["comment"].toString();
    else
        comment = incomingData["message_text"].toString();


    QVariantMap resultMap;
    resultMap["type_command"] = signature();

    _wraper = database::DBManager::instance().getDBWraper();
    auto query = _wraper->query();

    query.prepare("INSERT INTO "
                  "reviews_schema.reviews "
                  "("
                  "id_company,"
                  "id_order,"
                  "id_source,"
                  "date_create,"
                  "comment,"
                  "type,"
                  "user_phone,"
                  "user_email"
                  ") "
                  "VALUES ("
                  ":id_company,"
                  ":id_order,"
                  ":id_source,"
                  "now(),"
                  ":comment,"
                  "1,"
                  ":user_phone,"
                  ":user_email"
                  ") RETURNING id");

    query.bindValue(":id_company", 3);

    if (incomingData.contains("order_id"))
        query.bindValue(":id_order", incomingData["order_id"]);
    else
        query.bindValue(":id_order", 0);

    if (incomingData.contains("user_email"))
        query.bindValue(":user_email", incomingData["user_email"].toString());
    else
        query.bindValue(":user_email", "");

    query.bindValue(":id_source", incomingData["id_source"]);
    query.bindValue(":comment", comment);
    query.bindValue(":user_phone", incomingData["user_phone"].toString());

    if (!_wraper->execQuery(query))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error", signature());
        return network::ResponseShp();
    }

    query.first();
    _idReview = query.value(0).toLongLong();

    // TODO ANSWER
    if (incomingData.contains("answer"))
    {
        QVariantMap answers;
        QString answersStr = incomingData["answer"].toString();

        QStringList answersGroups = answersStr.split("[|::|]", QString::SkipEmptyParts);
        for (const QString& answer : answersGroups)
        {
            QStringList answerGroup = answer.split(":0:");
            const QString& key = answerGroup.first();
            const QString& value = answerGroup.last();
            answers[key] = value;
        }

        if (!createAnsver(answers))
            return network::ResponseShp();

        if (incomingData.contains("driver"))
        {
            if (!createDriver(incomingData["driver"].toMap()))
                return network::ResponseShp();
        }

        if (incomingData.contains("driver"))
        {
            if (!createCar(incomingData["car"].toMap()))
                return network::ResponseShp();
        }
    }

    // TODO FILES
    if (incomingData.contains("files"))
    if (!createFiles(incomingData))
        return network::ResponseShp();

    // TODO info order
    const auto idOrder = incomingData["order_id"].toULongLong();
    QVariantMap orderData;

    quint64 idCompany = 3;
    if (incomingData.contains("id_company"))
        idCompany = incomingData["id_company"].toULongLong();

    if (idOrder != 0 &&
       getOrderInfo(idCompany,
                    incomingData["order_id"].toULongLong(),
                    orderData))
    {
        if (!saveOrderInfo(_idReview, orderData))
        {
            qDebug() << "1111111111111111111" << "error";
        }
    }

    addRecordHistoryLog(_idReview);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    resultMap["status"] = 1;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    // this field for Piter
    result["status"] = "1";

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool InsertReview::createCar(const QVariantMap &map)
{
    auto query = _wraper->query();

    const auto insertCarStr = QString(
        "INSERT INTO reviews_schema.reviews_orders_cars "
        "(id_review, id, number, model, color) "
        "VALUES(:id_review, :id, :number, :model, :color)"
        );

    query.prepare(insertCarStr);
    query.bindValue(":id_review", _idReview);
    query.bindValue(":id",        map["id"]);
    query.bindValue(":number",    map["number"]);
    query.bindValue(":model",     map["model"]);
    query.bindValue(":color",     map["color"]);

    const auto insertCarQueryResult = _wraper->execQuery(query);
    if (!insertCarQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        return false;
    }

    return true;
}

bool InsertReview::createDriver(const QVariantMap &map)
{
    auto query = _wraper->query();

    query.prepare(
                "INSERT INTO reviews_schema.reviews_orders_drivers "
                "("
                "id_review, "
                "id_worker, "
                "surname, "
                "firstname, "
                "middlename, "
                "date_come, "
                "whose, "
                "id_park, "
                "\"column\", "
                "id_city, "
                "form_type, "
                "rating, "
                "rating_count"
                ") VALUES("
                ":id_review, "
                ":id, "
                ":surname, "
                ":firstname, "
                ":middlename, "
                ":date_come, "
                ":whose, "
                ":id_park, "
                ":column, "
                ":id_city, "
                ":form_type, "
                ":rating, "
                ":rating_count"
                ")"
                );

    query.bindValue(":id_review",    _idReview);
    query.bindValue(":id",           map["id"]);
    query.bindValue(":surname",      map["family"]);
    query.bindValue(":firstname",    map["name"]);
    query.bindValue(":middlename",   map["sec_fam"]);
    query.bindValue(":date_come",    map["date_come"]);
    query.bindValue(":whose",        map["our"]);
    query.bindValue(":id_park",      map["park"]);
    query.bindValue(":column",       map["column"]);
    query.bindValue(":id_city",      map["city"]);
    query.bindValue(":form_type",    map["form"]);
    query.bindValue(":rating",       map["rating"]);
    query.bindValue(":rating_count", map["cnt_rating"]);

    const auto queryResult = _wraper->execQuery(query);
    if (!queryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        return false;
    }

    return true;
}

bool InsertReview::createAnsver(const QVariantMap &map)
{
    bool flagNegativeReview = false;
    auto query = _wraper->query();

    if (map.count() != 6) // TODO 6 - count questions
        flagNegativeReview = true;

    for (auto answer = map.begin(); answer != map.end(); ++answer)
    {
        query.prepare(
                    "INSERT INTO reviews_schema.reviews_answer"
                    " (id_reviews, id_question, answer)"
                    " VALUES (:id_reviews, :id_question, :answer)");

        query.bindValue(":id_reviews",  _idReview);
        query.bindValue(":id_question", answer.key());

        QString answerValueStr;
        switch (answer.value().toInt()) {
        case 0:
            answerValueStr = "Нет";
            flagNegativeReview = true;
            break;
        case 1:
            answerValueStr = "Да";
            break;
        default:
            answerValueStr = "Не определено";
            break;
        }

        query.bindValue(":answer", answerValueStr);
        if (!_wraper->execQuery(query))
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return false;
        }
    }

    if (flagNegativeReview)
    {
         query.prepare("UPDATE reviews_schema.reviews SET type = 0 WHERE id = :id");
         query.bindValue(":id", _idReview);
         if (!_wraper->execQuery(query))
         {
             // TODO: db_error
             qDebug() << __FUNCTION__ << "error:" << query.lastError();
             sendError(query.lastError().text(), "db_error", signature());
             return false;
         }
    }
    else
    {
        query.prepare("UPDATE reviews_schema.reviews SET type = 1, id_status = 1 WHERE id = :id");
        query.bindValue(":id", _idReview);
        if (!_wraper->execQuery(query))
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return false;
        }
    }

    return true;
}

bool InsertReview::createFiles(const QVariantMap &map)
{
    auto files = map["files"].toList();

    const auto userName  = map["user_name"].toString();
    const auto userPhone = map["user_phone"].toString();
    const auto userEmail = map["user_email"].toString();

    auto query = _wraper->query();

    for (auto file : files)
    {
        auto mapFile = file.toMap();
        query.prepare("INSERT INTO "
                      "reviews_schema.reviews_files "
                      "("
                      "id_reviews,"
                      "file_name,"
                      "date_create,"
                      "client_name,"
                      "client_phone,"
                      "client_email"
                      ") "
                      "VALUES ("
                      ":id_reviews,"
                      ":file_name,"
                      "now(),"
                      ":client_name,"
                      ":client_phone,"
                      ":client_email"
                      ")");

        query.bindValue(":id_reviews",   _idReview);
        query.bindValue(":file_name",    mapFile["file_url"]);
        query.bindValue(":client_name",  userName);
        query.bindValue(":client_phone", userPhone);
        query.bindValue(":client_email", userEmail);

        if (!_wraper->execQuery(query))
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << query.lastError();
            sendError(query.lastError().text(), "db_error", signature());
            return false;
        }
    }


    return true;
}

bool InsertReview::getOrderInfo(const quint64 idCompany, const quint64 idOrder, QVariantMap & outData)
{
    const auto selectUrlStr = QString(
        "SELECT api "
        "FROM overall_schema.company "
        "WHERE id = :companyId");
    auto selectUrl = _wraper->query();
    selectUrl.prepare(selectUrlStr);
    selectUrl.bindValue(":companyId", idCompany);

    const auto selectUrlResult = selectUrl.exec();
    if (!selectUrlResult)
    {
        _wraper->rollback();
        qDebug() << __FUNCTION__ << "error:" << selectUrl.lastError();
        sendError(selectUrl.lastError().text(), "db_error", signature());
        return false;
    }

    const auto resultList = database::DBHelpers::queryToVariant(selectUrl);
    if (resultList.isEmpty())
    {
        _wraper->rollback();
        qDebug() << __FUNCTION__ << "Urls list is empty";
        sendError(QObject::tr("Urls list is empty"), "db_error", signature());
        return false;
    }

    const auto url = resultList.first().toMap().value("api").toString();
    auto webRequest = QSharedPointer<network::WebRequest>::create("get_okk_info_by_order");

    QVariantMap arguments;
    arguments["order_id"] = QString::number(idOrder);
    webRequest->setEncrypt(true);
    webRequest->setArguments(arguments);
    webRequest->setUrl(url);
    webRequest->setCallback(nullptr);

    network::WebRequestManager::instance()->sendRequestCurrentThread(webRequest);

    const auto data = webRequest->reply();
    webRequest->release();

    const auto doc = QJsonDocument::fromJson(data);
    auto jobj = doc.object();
    outData = jobj.toVariantMap();

    return true;
}

bool InsertReview::saveOrderInfo(const quint64 idReview, const QVariantMap & orderData)
{
    const auto itObj = orderData.find("obj");
    if (itObj == orderData.end())
    {
        qDebug() << "Error: " << "itObj == orderData.end()";
        return false;
    }

    const auto obj = itObj.value().toMap();
    const auto itOperator = obj.find("user_in");
    const auto itLogist = obj.find("user_out");
    const auto itCar = obj.find("auto");
    const auto itDriver = obj.find("driver");
    const auto itOrder = obj.find("order");
    const auto itObjEnd = obj.end();

    if (itOperator == itObjEnd ||
        itLogist == itObjEnd ||
        itCar == itObjEnd ||
        itDriver == itObjEnd ||
        itOrder == itObjEnd)
    {
        qDebug() << "Error: " << "itOperator == itObjEnd || itLogist == itObjEnd || itCar == itObjEnd || itDriver == itObjEnd || itOrder == itObjEnd";
        qDebug() << obj;
        return false;
    }

    const auto operatorObj = itOperator.value().toMap();
    const auto logistObj = itLogist.value().toMap();
    const auto carObj = itCar.value().toMap();
    const auto driverObj = itDriver.value().toMap();
    const auto orderObj = itOrder.value().toMap();


    const auto itIdOperator = operatorObj.find("id");
    if (itIdOperator != operatorObj.end() &&
        itIdOperator.value().toULongLong() != 0)
    {
        const auto insertOperatorStr = QString(
            "INSERT INTO reviews_schema.reviews_orders_operators "
            "(id_review, id_worker, fio, phone) "
            "VALUES(:id_review, :idWorker, :fio, :phone)"
            );
        auto insertOperatorQuery = _wraper->query();
        insertOperatorQuery.prepare(insertOperatorStr);
        insertOperatorQuery.bindValue(":id_review", idReview);
        insertOperatorQuery.bindValue(":idWorker", operatorObj["id"]);
        insertOperatorQuery.bindValue(":fio", operatorObj["fio"]);
        insertOperatorQuery.bindValue(":phone", operatorObj["phone"]);
        const auto insertOperatorQueryResult = _wraper->execQuery(insertOperatorQuery);
        if (!insertOperatorQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertOperatorQuery.lastError().text();
            return false;
        }
    }

    const auto itIdLogist = logistObj.find("id");
    if (itIdLogist != logistObj.end() &&
        itIdLogist.value().toULongLong() != 0)
    {
        const auto insertLogistStr = QString(
            "INSERT INTO reviews_schema.reviews_orders_logists "
            "(id_review, id_worker, fio, phone) "
            "VALUES(:id_review, :idWorker, :fio, :phone)"
            );
        auto insertLogistQuery = _wraper->query();
        insertLogistQuery.prepare(insertLogistStr);
        insertLogistQuery.bindValue(":id_review", idReview);
        insertLogistQuery.bindValue(":idWorker", logistObj["id"]);
        insertLogistQuery.bindValue(":fio", logistObj["fio"]);
        insertLogistQuery.bindValue(":phone", logistObj["phone"]);
        const auto insertLogistQueryResult = _wraper->execQuery(insertLogistQuery);
        if (!insertLogistQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertLogistQuery.lastError().text();
            return false;
        }
    }

    const auto itIdCar = carObj.find("id");
    if (itIdCar != carObj.end() &&
        itIdCar.value().toULongLong() != 0)
    {
        const auto insertCarStr = QString(
            "INSERT INTO reviews_schema.reviews_orders_cars "
            "(id_review, id_car, number, model, color) "
            "VALUES(:id_review, :idCar, :number, :model, :color)"
            );
        auto insertCarQuery = _wraper->query();
        insertCarQuery.prepare(insertCarStr);
        insertCarQuery.bindValue(":id_review", idReview);
        insertCarQuery.bindValue(":idCar", carObj["id"]);
        insertCarQuery.bindValue(":number", carObj["number"]);
        insertCarQuery.bindValue(":model", carObj["model"]);
        insertCarQuery.bindValue(":color", carObj["color"]);
        const auto insertCarQueryResult = _wraper->execQuery(insertCarQuery);
        if (!insertCarQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertCarQuery.lastError().text();
            return false;
        }
    }

    const auto itIdDriver = driverObj.find("id");
    if (itIdDriver != driverObj.end() &&
        itIdDriver.value().toULongLong() != 0)
    {
        const auto insertDriverStr = QString(
            "INSERT INTO reviews_schema.reviews_orders_drivers "
            "("
            "id_review, "
            "id_worker, "
            "surname, "
            "firstname, "
            "middlename, "
            "date_come, "
            "whose, "
            "id_park, "
            "\"column\", "
            "%1 "
            "%3 "
            "form_type "
            ") "
            "VALUES"
            "("
            ":id_review, "
            ":id, "
            ":surname, "
            ":firstname, "
            ":middlename, "
            ":date_come, "
            ":whose, "
            ":id_park, "
            ":column, "
            "%2 "
            "%4 "
            ":form_type "
            ")"
            )
            .arg(driverObj.contains("city") ? "id_city, " : "")
            .arg(driverObj.contains("city") ? ":id_city, " : "")
            .arg(driverObj.contains("rating") ? "rating, rating_count, " : "")
            .arg(driverObj.contains("rating") ? ":rating, :rating_count, " : "");

        auto insertDriverQuery = _wraper->query();
        insertDriverQuery.prepare(insertDriverStr);
        insertDriverQuery.bindValue(":id_review", idReview);
        insertDriverQuery.bindValue(":id", driverObj["id"]);
        insertDriverQuery.bindValue(":surname", driverObj["family"]);
        insertDriverQuery.bindValue(":firstname", driverObj["name"]);
        insertDriverQuery.bindValue(":middlename", driverObj["sec_fam"]);
        insertDriverQuery.bindValue(":date_come", driverObj["date_come"]);
        insertDriverQuery.bindValue(":whose", driverObj["our"]);
        insertDriverQuery.bindValue(":id_park", driverObj["park"]);
        insertDriverQuery.bindValue(":column", driverObj["column"]);
        if (driverObj.contains("city"))
            insertDriverQuery.bindValue(":id_city", driverObj["city"]);
        insertDriverQuery.bindValue(":form_type", driverObj["form"]);
        if (driverObj.contains("rating"))
        {
            insertDriverQuery.bindValue(":rating", driverObj["rating"]);
            insertDriverQuery.bindValue(":rating_count", driverObj["cnt_rating"]);
        }
        const auto insertDriverQueryResult = _wraper->execQuery(insertDriverQuery);
        if (!insertDriverQueryResult)
        {
            // TODO: db_error
            _wraper->rollback();
            qDebug() << __FUNCTION__ << "error:" << insertDriverQuery.lastError().text();
            return false;
        }

        const auto phones = driverObj.value("phones").toList();
        for (const auto phone: phones)
        {
            const auto strPhone = phone.toString();
            if (strPhone.isEmpty())
                continue;

            const auto insertDriverPhoneStr = QString(
                "INSERT INTO reviews_schema.reviews_orders_driver_phones "
                "("
                "id_review, "
                "phone"
                ") VALUES("
                ":id_review, "
                ":phone"
                ")"
                );
            auto insertDriverPhoneQuery = _wraper->query();
            insertDriverPhoneQuery.prepare(insertDriverPhoneStr);
            insertDriverPhoneQuery.bindValue(":id_review", idReview);
            insertDriverPhoneQuery.bindValue(":phone", strPhone);
            const auto insertDriverPhoneQueryResult = _wraper->execQuery(insertDriverPhoneQuery);
            if (!insertDriverPhoneQueryResult)
            {
                // TODO: db_error
                _wraper->rollback();
                qDebug() << __FUNCTION__ << "error:" << insertDriverPhoneQuery.lastError().text();
                return false;
            }
        }
    }
    return true;
}

void InsertReview::addRecordHistoryLog(quint64 _idReview)
{
    auto insertQuery = _wraper->query();

    const auto &comment = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + " " + " создали отзыв";

    const auto & sqlQuery = QString(
        "INSERT INTO reviews_schema.\"historyLog\" "
        "(id_reviews, signature, comment) "
        "VALUES "
        "(:id, :signature, :comment)");

    insertQuery.prepare(sqlQuery);
    insertQuery.bindValue(":id", _idReview);
    insertQuery.bindValue(":signature", signature());
    insertQuery.bindValue(":comment", comment);

    const auto Result = _wraper->execQuery(insertQuery);
    if (!Result)
    {
        // TODO: db_error
        sendError("historyLog command add_comment", "db_error", signature());
        return;
    }
}
