#pragma once

#include "server-core/Commands/UserCommand.h"


namespace database
{
    class DBWraper;
    typedef QSharedPointer<DBWraper> DBWraperShp;
}

namespace qat_server
{

    class InsertReview :
            public core::Command,
            public core::CommandCreator<InsertReview>
    {
        friend class QSharedPointer<InsertReview>;

        InsertReview(const Context& newContext);

    public:
        ~InsertReview() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        void addRecordHistoryLog(quint64 idReview);
        bool createCar(const QVariantMap &map);
        bool createDriver(const QVariantMap &map);
        bool createAnsver(const QVariantMap &map);
        bool createFiles(const QVariantMap &map);

        bool getOrderInfo(const quint64 idCompany, const quint64 idOrder, QVariantMap & outData);
        bool saveOrderInfo(const quint64 idComplaint, const QVariantMap & orderData);

        quint64 _idReview;
        database::DBWraperShp _wraper;
    };

}
