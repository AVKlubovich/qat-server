#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectReviews
        : public core::Command
        , public core::CommandCreator<SelectReviews>
    {
        friend class QSharedPointer<SelectReviews>;

        SelectReviews(const Context& newContext);
    public:
        ~SelectReviews() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
