#include "Common.h"
#include "DeleteSmsTemplate.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"


RegisterCommand(qat_server::DeleteSmsTemplate, "delete_sms_template")

using namespace qat_server;

DeleteSmsTemplate::DeleteSmsTemplate(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp DeleteSmsTemplate::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& data = _context._packet.body().toMap();
    const auto& inBody = data["body"].toMap();

    if (!checkInData(inBody))
    {
        return network::ResponseShp();
    }

    const quint64 smsId = inBody["id"].toInt();

    const QString& updateSmsTemplateStr = QString(
        "DELETE FROM complaints_schema.sms_templates "
        "WHERE id = :smsId");

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto deleteSmsTemplateQuery = wraper->query();
    deleteSmsTemplateQuery.prepare(updateSmsTemplateStr);
    deleteSmsTemplateQuery.bindValue(":smsId", smsId);

    if (!deleteSmsTemplateQuery.exec())
    {
        sendError(deleteSmsTemplateQuery.lastError().text(), "db_error", signature());
        qDebug() << __FUNCTION__ << deleteSmsTemplateQuery.lastError().text();
        return network::ResponseShp();
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["id"] = smsId;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

bool DeleteSmsTemplate::checkInData(const QVariantMap& inBody)
{
    if (!inBody.contains("id"))
    {
        sendError("Can not find parameter id", "parameters_error", signature());
        return false;
    }

    return true;
}
