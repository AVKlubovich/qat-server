#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class DeleteSmsTemplate
        : public core::Command
        , public core::CommandCreator<DeleteSmsTemplate>
    {
        friend class QSharedPointer<DeleteSmsTemplate>;

        DeleteSmsTemplate(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
