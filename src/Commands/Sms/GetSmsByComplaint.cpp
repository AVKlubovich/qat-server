#include "Common.h"
#include "GetSmsByComplaint.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

RegisterCommand(qat_server::GetSmsByComplaint, "get_sms_by_complaint")


using namespace qat_server;

GetSmsByComplaint::GetSmsByComplaint(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp GetSmsByComplaint::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& data = _context._packet.body().toMap();
    const auto& inBody = data["body"].toMap();

    if (!checkInData(inBody))
    {
        return network::ResponseShp();
    }

    const quint64 complaintId = inBody["id_complaint"].toInt();
    const QString& selectSmsStr = QString(
        "SELECT * "
        "FROM complaints_schema.sms_by_complaints "
        "WHERE id_complaint = :complaintId");

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectSmsQuery = wraper->query();
    selectSmsQuery.prepare(selectSmsStr);
    selectSmsQuery.bindValue(":complaintId", complaintId);

    if (!selectSmsQuery.exec())
    {
        sendError(selectSmsQuery.lastError().text(), "db_error", signature());
        qDebug() << __FUNCTION__ << selectSmsQuery.lastError().text();
        return network::ResponseShp();
    }

    auto resultMap = database::DBHelpers::queryToVariant(selectSmsQuery);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["sms"] = QVariant::fromValue(resultMap);

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

bool GetSmsByComplaint::checkInData(const QVariantMap& inBody)
{
    if (!inBody.contains("id_complaint"))
    {
        sendError("Can not find parameter id_complaint", "parameters_error", signature());
        return false;
    }

    return true;
}
