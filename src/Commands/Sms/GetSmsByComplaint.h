#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetSmsByComplaint
        : public core::Command
        , public core::CommandCreator<GetSmsByComplaint>
    {
        friend class QSharedPointer<GetSmsByComplaint>;

        GetSmsByComplaint(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
