#include "Common.h"
#include "GetSmsTemplates.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"


RegisterCommand(qat_server::GetSmsTemplates, "get_sms_templates")

using namespace qat_server;

GetSmsTemplates::GetSmsTemplates(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp GetSmsTemplates::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& data = _context._packet.body().toMap();
    const auto& inBody = data["body"].toMap();

    if (!inBody.contains("id_company"))
    {
        sendError("Can not find parameter id_company", "parameters_error", signature());
        return network::ResponseShp();
    }

    const quint64 companyId = inBody["id_company"].toInt();
    const QString& selectSmsTemplatesStr = QString(
        "SELECT * "
        "FROM complaints_schema.sms_templates "
        "WHERE id_company = :companyId");

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectSmsTemplatesQuery = wraper->query();
    selectSmsTemplatesQuery.prepare(selectSmsTemplatesStr);
    selectSmsTemplatesQuery.bindValue(":companyId", companyId);

    if (!selectSmsTemplatesQuery.exec())
    {
        sendError(selectSmsTemplatesQuery.lastError().text(), "db_error", signature());
        qDebug() << __FUNCTION__ << selectSmsTemplatesQuery.lastError().text();
        return network::ResponseShp();
    }

    auto resultMap = database::DBHelpers::queryToVariant(selectSmsTemplatesQuery);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["templates"] = QVariant::fromValue(resultMap);

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}
