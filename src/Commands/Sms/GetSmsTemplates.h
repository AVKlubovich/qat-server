#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetSmsTemplates
        : public core::Command
        , public core::CommandCreator<GetSmsTemplates>
    {
        friend class QSharedPointer<GetSmsTemplates>;

        GetSmsTemplates(const Context& newContext);

    public:
        network::ResponseShp exec() override;
    };

}
