#include "Common.h"
#include "InsertSmsTemplate.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"


RegisterCommand(qat_server::InsertSmsTemplate, "insert_sms_template")

using namespace qat_server;

InsertSmsTemplate::InsertSmsTemplate(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp InsertSmsTemplate::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& data = _context._packet.body().toMap();
    const auto& inBody = data["body"].toMap();

    if (!checkInData(inBody))
    {
        return network::ResponseShp();
    }

    const quint64 companyId = inBody["id_company"].toInt();
    const QString& description = inBody["description"].toString();
    const QString& messageText = inBody["text_message"].toString();

    const QString& insertSmsTemplateStr = QString(
        "INSERT INTO complaints_schema.sms_templates "
        "(id_company, description, text_message) "
        "VALUES "
        "(:companyId, :description, :messageText)");

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto insertSmsTemplateQuery = wraper->query();
    insertSmsTemplateQuery.prepare(insertSmsTemplateStr);
    insertSmsTemplateQuery.bindValue(":companyId", companyId);
    insertSmsTemplateQuery.bindValue(":description", description);
    insertSmsTemplateQuery.bindValue(":messageText", messageText);

    if (!insertSmsTemplateQuery.exec())
    {
        sendError(insertSmsTemplateQuery.lastError().text(), "db_error", signature());
        qDebug() << __FUNCTION__ << insertSmsTemplateQuery.lastError().text();
        return network::ResponseShp();
    }

    const quint64 lastId = insertSmsTemplateQuery.lastInsertId().toInt();

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["id"] = lastId;
    body["description"] = description;
    body["text_message"] = messageText;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

bool InsertSmsTemplate::checkInData(const QVariantMap& inBody)
{
    if (!inBody.contains("id_company"))
    {
        sendError("Can not find parameter id_company", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("description"))
    {
        sendError("Can not find parameter description", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("text_message"))
    {
        sendError("Can not find parameter text_message", "parameters_error", signature());
        return false;
    }

    return true;
}
