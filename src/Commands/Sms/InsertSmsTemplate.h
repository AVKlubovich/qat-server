#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class InsertSmsTemplate
        : public core::Command
        , public core::CommandCreator<InsertSmsTemplate>
    {
        friend class QSharedPointer<InsertSmsTemplate>;

        InsertSmsTemplate(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
