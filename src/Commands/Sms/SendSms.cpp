#include "Common.h"
#include "SendSms.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "SmsSender/SmsSender.h"


RegisterCommand(qat_server::SendSms, "send_sms")

using namespace qat_server;

SendSms::SendSms(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp SendSms::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& inData = _context._packet.body().toMap();
    const auto& inBody = inData["body"].toMap();

    if (!checkInData(inBody))
    {
        return network::ResponseShp();
    }

    const quint64 complaintId = inBody["id_complaint"].toULongLong();
    const QString& phoneNumber = inBody["phone_number"].toString();
    const QString& smsText = inBody["sms_text"].toString();
    const QDateTime& time = QDateTime::fromString(inBody["time_of_send"].toString(), "yyyy-MM-dd hh:mm:ss");
    const quint32 idCompany = inBody["id_company"].toUInt();
    const quint32 idType = inBody["type_message"].toUInt();
    const double mark = inBody["mark"].toDouble();

    SmsSender::instance().sendSms(complaintId, phoneNumber, smsText, time, idCompany, idType, mark);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

bool SendSms::checkInData(const QVariantMap& inBody)
{
    if (!inBody.contains("id_complaint"))
    {
        sendError("Can not find parameter id_complaint", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("phone_number"))
    {
        sendError("Can not find parameter phone_number", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("sms_text"))
    {
        sendError("Can not find parameter sms_text", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("time_of_send"))
    {
        sendError("Can not find parameter time_of_send", "parameters_error", signature());
        return false;
    }

    return true;
}
