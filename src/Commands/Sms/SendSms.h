#pragma once

#include "server-core/Commands/UserCommand.h"


namespace network
{
    class WebRequest;
    class WebRequestManager;
}

namespace qat_server
{

    class SendSms :
            public core::Command,
            public core::CommandCreator<SendSms>
    {
        friend class QSharedPointer<SendSms>;

    private:
        SendSms(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody) override;

    private:
        QSharedPointer<network::WebRequestManager> _webManager;
    };

}
