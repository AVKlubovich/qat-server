#include "Common.h"
#include "UpdateSmsTemplate.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"


RegisterCommand(qat_server::UpdateSmsTemplate, "update_sms_template")

using namespace qat_server;

UpdateSmsTemplate::UpdateSmsTemplate(const Context& newContext)
    : Command(newContext)
{
}

network::ResponseShp UpdateSmsTemplate::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    const auto& data = _context._packet.body().toMap();
    const auto& inBody = data["body"].toMap();

    if (!checkInData(inBody))
    {
        return network::ResponseShp();
    }

    const quint64 smsId = inBody["id"].toInt();
    const QString& description = inBody["description"].toString();
    const QString& smsText = inBody["text_message"].toString();

    const QString& updateSmsTemplateStr = QString(
        "UPDATE complaints_schema.sms_templates "
        "SET description = :description, text_message = :smsText "
        "WHERE id = :smsId");

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto updateSmsTemplateQuery = wraper->query();
    updateSmsTemplateQuery.prepare(updateSmsTemplateStr);
    updateSmsTemplateQuery.bindValue(":smsId", smsId);
    updateSmsTemplateQuery.bindValue(":description", description);
    updateSmsTemplateQuery.bindValue(":smsText", smsText);

    if (!updateSmsTemplateQuery.exec())
    {
        sendError(updateSmsTemplateQuery.lastError().text(), "db_error", signature());
        qDebug() << __FUNCTION__ << updateSmsTemplateQuery.lastError().text();
        return network::ResponseShp();
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap body;
    body["id"] = smsId;
    body["description"] = description;
    body["text_message"] = smsText;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

bool UpdateSmsTemplate::checkInData(const QVariantMap& inBody)
{
    if (!inBody.contains("id"))
    {
        sendError("Can not find parameter id", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("description"))
    {
        sendError("Can not find parameter description", "parameters_error", signature());
        return false;
    }
    else if (!inBody.contains("text_message"))
    {
        sendError("Can not find parameter text_message", "parameters_error", signature());
        return false;
    }

    return true;
}
