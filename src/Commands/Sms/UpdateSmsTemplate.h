#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class UpdateSmsTemplate
        : public core::Command
        , public core::CommandCreator<UpdateSmsTemplate>
    {
        friend class QSharedPointer<UpdateSmsTemplate>;

        UpdateSmsTemplate(const Context& newContext);

    public:
        network::ResponseShp exec() override;

    private:
        bool checkInData(const QVariantMap& inBody);
    };

}
