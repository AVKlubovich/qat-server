#include "Common.h"
#include "SelectComplaintsStatistic.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::SelectComplaintsStatistic, "statistic_complaints")


using namespace qat_server;

SelectComplaintsStatistic::SelectComplaintsStatistic(const Context& newContext)
    : Command(newContext)
{
}

void SelectComplaintsStatistic::calculate()
{
    QMap< int, quint64 > mapStatus;
    mapStatus.insert(1, 0);
    mapStatus.insert(2, 0);
    mapStatus.insert(3, 0);
    mapStatus.insert(4, 0);
    mapStatus.insert(5, 0);

    for (const auto& value : _listNatures)
    {
        const auto &map = value.toMap();
        const auto idCompany  = map.value("id_company").toInt();
        const auto idNature   = map.value("id").toInt();
        const auto nameNature = map.value("data_nature").toString();

        if (!_mapNaturesToCompany.contains(idCompany))
            _mapNaturesToCompany.insert( idCompany, QMap< quint64, QString >() );
        auto &natures = _mapNaturesToCompany[idCompany];
        natures.insert(idNature, nameNature);
    }

    for (const auto& value : _listComplaints)
    {
        const auto&map = value.toMap();
        const auto idCompany       = map.value("name").toString();
        const auto idNature        = map.value("data_nature").toString();
        const auto statusComplaint = map.value("id_status").toInt();

        // company
        if (!_mapComplaintsToComapnies.contains(idCompany))
            _mapComplaintsToComapnies[idCompany] = QMap< QString, QMap< int, quint64 > >();
        QMap< QString,QMap< int, quint64 > >& complaintsToNatures = _mapComplaintsToComapnies[idCompany];

        // nature
        if (!complaintsToNatures.contains(idNature))
            complaintsToNatures[idNature] = mapStatus;
        QMap< int, quint64 >& complaintsStatus = complaintsToNatures[idNature];

        // status
        auto& complaintCount = complaintsStatus[statusComplaint];
        ++complaintCount;
    }
}

QVariantMap SelectComplaintsStatistic::handleResult(const QStringList & companies)
{
    QVariantMap body;

    //columns
    QVariantList listColumnStatus;
    for (auto i = 1; i <= 5; ++i)
    {
        QVariantMap map;
        map["id"] = i;
        switch (i) {
            case 1:
                map["name"] = "Новая";
                break;
            case 2:
                map["name"] = "В работе";
                break;
            case 3:
                map["name"] = "Разобрана";
                break;
            case 4:
                map["name"] = "Требует оповещения";
                break;
            case 5:
                map["name"] = "Закрыта";
                break;
            default:
                break;
        }
        listColumnStatus.append(map);
    }
    body["columns"] = listColumnStatus;

    // companies_values
    QVariantMap companyMap;
    for (auto company = _mapComplaintsToComapnies.begin(); company != _mapComplaintsToComapnies.end(); ++company)
    {
        QVariantMap natureMap;
        for (auto nature = company.value().begin(); nature != company.value().end(); ++nature)
        {
            QVariantMap statusMap;
            for (auto status = nature.value().begin(); status != nature.value().end(); ++status)
                statusMap.insert(QString::number(status.key()), status.value());

            natureMap.insert(nature.key(), statusMap);
        }

        companyMap.insert(company.key(), natureMap);
    }
    body["companies_values"] = companyMap;

    // departments
    body["departments"] = _listCountByDepartments;

    // source
    body["source"] = _listCountBySource;


    return body;
}

QSharedPointer<network::Response> SelectComplaintsStatistic::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto dateStart = incomingData.value("date_start");
    const auto dateEnd   = incomingData.value("date_end");

    QStringList companies;
    for (auto company : incomingData.value("companies").toList())
        companies << company.toString();

    QStringList targets;
    for (auto target : incomingData.value("targets").toList())
        targets << target.toString();

    if (companies.isEmpty())
    {
        QVariantMap head;
        head["type"] = signature();
        head["status"] = -1;
        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = QVariant();
        responce->setBody(QVariant::fromValue(result));
        return QSharedPointer<network::Response>();
    }

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto query = wraper->query();
//---------------------------------------------------------------------------------------------------------------------------------------------------------------//
    // complaints
    const auto selectComplaintsStr = QString("SELECT h.id_complaint, h.id_status, com.name, n.data_nature "
                                             "FROM complaints_schema.\"historyLog\" AS h "
                                             "INNER JOIN complaints_schema.complaints AS c ON (h.id_complaint = c.id) "
                                             "INNER JOIN overall_schema.company AS com ON (com.id = c.id_company) "
                                             "INNER JOIN complaints_schema.complaints_nature AS n ON (n.id = c.id_nature) "
                                             "WHERE h.id_status <> 0 AND (h.date_create BETWEEN :dateStart AND :dateEnd) AND c.id_company IN (%1)"
                                             "AND n.id_target IN (%2)")
                                     .arg(companies.join(", "))
                                     .arg(targets.join(", "));

    query.prepare(selectComplaintsStr);
    query.bindValue(":dateStart", dateStart);
    query.bindValue(":dateEnd", dateEnd);
    if (!wraper->execQuery(query))
    {
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error_date", signature());
        return QSharedPointer<network::Response>();
    }
    _listComplaints = database::DBHelpers::queryToVariant(query);

    // natures
    const auto selectNaturesStr = QString("SELECT id, id_company, data_nature "
                                          "FROM complaints_schema.complaints_nature "
                                          "WHERE id_company IN (%1)").arg(companies.join(", "));

    query.prepare(selectNaturesStr);
    if (!wraper->execQuery(query))
    {
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error_date", signature());
        return QSharedPointer<network::Response>();
    }
    _listNatures = database::DBHelpers::queryToVariant(query);

    // departments
    const auto selectDComplaintsStr = QString("SELECT c.id_department, d.name, COUNT(*) AS complaints_count "
                                              "FROM complaints_schema.complaints AS c "
                                              "INNER JOIN overall_schema.departments AS d ON (d.id = c.id_department) "
                                              "WHERE c.id_company IN (%1) AND c.id_status <> 5 "
                                              "GROUP BY c.id_department, d.name").arg(companies.join(", "));

    query.prepare(selectDComplaintsStr);
    if (!wraper->execQuery(query))
    {
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error_date", signature());
        return QSharedPointer<network::Response>();
    }
    _listCountByDepartments = database::DBHelpers::queryToVariant(query);

    // source
    const auto selectSourceStr = QString("SELECT c.id_source, s.name_source, COUNT(*) AS complaints_count "
                                         "FROM complaints_schema.complaints AS c "
                                         "INNER JOIN complaints_schema.complaints_source AS s ON (c.id_source = s.id) "
                                         "WHERE c.id_company IN (%1) AND (c.date_create BETWEEN :dateStart AND :dateEnd) "
                                         "GROUP BY c.id_source, s.name_source")
                                 .arg(companies.join(", "));

    query.prepare(selectSourceStr); 
    query.bindValue(":dateStart", dateStart);
    query.bindValue(":dateEnd", dateEnd);
    if (!wraper->execQuery(query))
    {
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error_date", signature());
        return QSharedPointer<network::Response>();
    }
    _listCountBySource = database::DBHelpers::queryToVariant(query);
//---------------------------------------------------------------------------------------------------------------------------------------------------------------//

    calculate();
    const auto body = handleResult(companies);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;
    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
