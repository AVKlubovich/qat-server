#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectComplaintsStatistic
        : public core::Command
        , public core::CommandCreator<SelectComplaintsStatistic>
    {
        friend class QSharedPointer<SelectComplaintsStatistic>;

        SelectComplaintsStatistic(const Context& newContext);
        void calculate();
        QVariantMap handleResult(const QStringList &companies);
    public:
        ~SelectComplaintsStatistic() override = default;

        QSharedPointer<network::Response> exec() override;

    private:
        QVariantList _listComplaints;
        QVariantList _listNatures;
        QVariantList _listCountByDepartments;
        QVariantList _listCountBySource;

        QMap <quint64, QMap< quint64, QString > > _mapNaturesToCompany;
//        QMap <quint64, QMap< quint64, QList<quint64> > > _mapComplaintsToComapnies;

        QMap <QString, QMap< QString, QMap< int, quint64 > > > _mapComplaintsToComapnies;
    };

}
