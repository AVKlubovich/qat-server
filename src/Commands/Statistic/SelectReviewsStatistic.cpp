#include "Common.h"
#include "SelectReviewsStatistic.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::SelectReviewsStatistic, "statistic_reviews")


using namespace qat_server;

SelectReviewsStatistic::SelectReviewsStatistic(const Context& newContext)
    : Command(newContext)
{
}

QVariantMap SelectReviewsStatistic::convertVariantMap(QMap<QString, QMap<QString, int> > mapParkStatistic)
{
    QVariantMap mapPark;

    for (auto park = mapParkStatistic.begin(); park != mapParkStatistic.end(); ++park)
    {
        auto countMap = park.value();
        QVariantMap countVariantMap;
        for (auto count = countMap.begin(); count != countMap.end(); ++count)
            countVariantMap[count.key()] = QVariant(count.value());

        mapPark[park.key()] = countVariantMap;
    }

    return mapPark;
}

QSharedPointer<network::Response> SelectReviewsStatistic::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());
    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto dateStart = incomingData.value("date_start");
    const auto dateEnd   = incomingData.value("date_end");

    QStringList whoseDrivers;
    for (auto driver : incomingData.value("driver").toList())
        whoseDrivers << driver.toString();

    if (whoseDrivers.isEmpty())
    {
        QVariantMap head;
        head["type"] = signature();
        head["status"] = -1;
        QVariantMap result;
        result["head"] = QVariant::fromValue(head);
        result["body"] = QVariant();
        responce->setBody(QVariant::fromValue(result));
        return QSharedPointer<network::Response>();
    }

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto query = wraper->query();

    const auto& selectReviewsStr =
            QString("SELECT p.park_name, r.type FROM reviews_schema.reviews AS r "
                    "INNER JOIN reviews_schema.reviews_orders_drivers AS d ON (d.id_review = r.id) "
                    "INNER JOIN overall_schema.parks AS p ON (p.id = d.id_park) "
                    "WHERE (r.date_create BETWEEN :dateStart AND :dateEnd) "
                    "AND d.whose IN (%1)"
                    ).arg(whoseDrivers.join(", "));

    query.prepare(selectReviewsStr);
    query.bindValue(":dateStart", dateStart);
    query.bindValue(":dateEnd", dateEnd);
    if (!wraper->execQuery(query))
    {
        qDebug() << __FUNCTION__ << "error:" << query.lastError();
        sendError(query.lastError().text(), "db_error_date", signature());
        return QSharedPointer<network::Response>();
    }
    const auto listReviews = database::DBHelpers::queryToVariant(query);

    QMap <QString, QMap<QString, int>> mapParkStatistic;
    QMap<QString, int> mapStatistic;
    mapStatistic["Положительных"] = 0;
    mapStatistic["Отрицательных"] = 0;

    for (auto review : listReviews)
    {
        auto mapReview = review.toMap();
        const auto& parkName = mapReview["park_name"].toString();
        const auto type = mapReview["type"].toInt();

        if (!mapParkStatistic.contains(parkName))
            mapParkStatistic[parkName] = mapStatistic;
        QMap<QString, int>& statistic = mapParkStatistic[parkName];

        if (type)
        {
            auto& count = statistic["Отрицательных"];
            ++count;
        }
        else
        {
            auto& count = statistic["Положительных"];
            ++count;
        }
    }

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;
    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = convertVariantMap(mapParkStatistic);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
