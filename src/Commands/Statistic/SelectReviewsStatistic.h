#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class SelectReviewsStatistic
        : public core::Command
        , public core::CommandCreator<SelectReviewsStatistic>
    {
        friend class QSharedPointer<SelectReviewsStatistic>;

        SelectReviewsStatistic(const Context& newContext);
        QVariantMap convertVariantMap(QMap <QString, QMap<QString, int>> mapParkStatistic);

    public:
        ~SelectReviewsStatistic() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
