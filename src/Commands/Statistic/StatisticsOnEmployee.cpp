#include "Common.h"
#include "StatisticsOnEmployee.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "Definitions.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::StatisticsOnEmployee, "worker_statistic")


using namespace qat_server;

StatisticsOnEmployee::StatisticsOnEmployee(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> StatisticsOnEmployee::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto query = wraper->query();

    const QString tableName = QString("complaints_schema.complaints_orders_%1").arg(incomingData["worker"].toString());
    const QString id = incomingData["id"].toString();

    Workers worker = Driver;
    if (incomingData["worker"].toString() == "operators")
        worker = Operator;
    else if (incomingData["worker"].toString() == "logists")
        worker = Logist;

    QVariantMap resultMap;
    for (auto i = 0; i < 3; ++i)
    {
        auto sQuery = QString(
            "SELECT count(*) AS rez"
            " FROM %1 AS o"
            " INNER JOIN complaints_schema.complaints AS c "
            " ON (c.id = o.id_complaint)"
            " INNER JOIN complaints_schema.complaints_nature AS n "
            " ON (n.id = c.id_nature)"
            " WHERE o.id_worker = %2 %3 AND n.id_target=%4"
            );

        switch (i)
        {
            case 0:
                sQuery = sQuery.arg(tableName).arg(id).arg("").arg(worker);
                break;
            case 1: // пОложительныХ
                sQuery = sQuery.arg(tableName).arg(id).arg(" AND c.id_type = 1").arg(worker);
                break;
            case 2: // отрицательных
                sQuery = sQuery.arg(tableName).arg(id).arg(" AND c.id_type = 2").arg(worker);
                break;
            default:
                break;
        }

        query.prepare(sQuery);

        if (!query.exec())
            qDebug() << query.lastError();

        if (query.first())
            resultMap[QString::number(i)] = query.value("rez");
    }

    const auto& sQuery = QString("SELECT c.id, n.data_nature AS nature FROM"
        " %1 AS o"
        " INNER JOIN complaints_schema.complaints AS c "
        " ON (c.id = o.id_complaint)"
        " INNER JOIN complaints_schema.complaints_nature AS n "
        " ON (n.id = c.id_nature)"
        " WHERE o.id_worker = %2 AND c.id_type = 2 "
        "AND n.id_target = %3"
        ).arg(tableName).arg(id).arg(QString::number(worker));
    query.prepare(sQuery);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        Q_ASSERT(true);
    }

    const auto resultList = database::DBHelpers::queryToVariant(query);

    QVariantMap mapVariant;
    for (auto item : resultList)
    {
        QVariantMap map = item.toMap();

        const QString &nature = map["nature"].toString();
        if (!mapVariant.contains(nature))
            mapVariant[nature] = QVariantList();

        QVariantList listId = mapVariant[nature].toList();
        listId.append(map["id"]);

        mapVariant[nature] = listId;
    }

    resultMap["natures"] = mapVariant;

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
