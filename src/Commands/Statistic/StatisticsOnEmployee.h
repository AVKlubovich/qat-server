#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class StatisticsOnEmployee
        : public core::Command
        , public core::CommandCreator<StatisticsOnEmployee>
    {
        friend class QSharedPointer<StatisticsOnEmployee>;

        StatisticsOnEmployee(const Context& newContext);
    public:
        ~StatisticsOnEmployee() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
