#include "Common.h"
#include "TestCommand.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

RegisterCommand(qat_server::TestCommand, "test_command")


namespace qat_server
{

    TestCommand::TestCommand(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> TestCommand::exec()
    {
        return QSharedPointer<network::Response>();
    }

    void TestCommand::execSlow(const std::function<void ()>& callback)
    {
        Command::execSlow(callback);

        _response = network::ResponseShp();

        // NOTE: webManager юзать так же как и в старом сервере
        // на данный момент он singleton

        _callback();
    }

    network::ResponseShp TestCommand::getSlowResponse()
    {
        return _response;
    }

    core::Command::TypeCommand TestCommand::type() const
    {
        return TypeCommand::SlowCommand;
    }

}
