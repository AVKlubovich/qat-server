#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class TestCommand
        : public core::Command
        , public core::CommandCreator<TestCommand>
    {
        friend class QSharedPointer<TestCommand>;

        TestCommand(const Context& newContext);
    public:
        ~TestCommand() override = default;

        QSharedPointer<network::Response> exec() override;

        void execSlow(const std::function<void()>& callback) override;
        network::ResponseShp getSlowResponse() override;
        TypeCommand type() const override;

    private:
        network::ResponseShp _response;
    };

}
