#include "Common.h"
#include "UploadFile.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/ResponseGeneric.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(qat_server::UploadFile, "upload_file")


namespace qat_server
{

    UploadFile::UploadFile(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> UploadFile::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        //decode incoming data
        auto incomingData = _context._packet.body();
        QSharedPointer<network::DefaultRequest> requestPtr(new network::DefaultRequest("upload_file", incomingData));
        //requestPtr->fromVariant(incomingData);

        //create command response
        //DefaultResponse(const QString& type, const QVariant& response, const QSharedPointer<Request>& request);
        //QSharedPointer<network::ResponseGeneric> response(new network::ResponseGeneric());

        QVariantMap requestBody = incomingData.toMap()["body"].toMap();
        QString uuid = requestBody["uuid"].toString();
        QString fileName = requestBody["file_name"].toString();
        int chunkId = requestBody["chunk_id"].toInt();
        QString chunkData = requestBody["chunk_data"].toString();

        qDebug() << "Add complaint attachment chunk: " << chunkId << fileName;

        const QString sqlQueryStr = QString("INSERT INTO complaints_schema.files ("
                                            "uuid, file_name, chunk_num, chunk_data"
                                            ") VALUES ('%1', '%2', '%3', '%4')")
                                    .arg(uuid).arg(fileName).arg(chunkId).arg(chunkData);

        qDebug() << sqlQueryStr;
        QSharedPointer<database::DBWraper> wraper = database::DBManager::instance().getDBWraper();
        QSqlQuery query = wraper->query();


        QSharedPointer<network::ResponseGeneric> response;

        QVariantMap resultMap;
        const auto queryResult = wraper->execQuery(sqlQueryStr);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response.reset(new network::ResponseGeneric(requestPtr, network::ResponseGeneric::StatusFail));

            QString errMsg = QString("Database SQL query error, see server log");
            response->setMessage(errMsg);
        }
        else
        {
            response.reset(new network::ResponseGeneric(requestPtr, network::ResponseGeneric::StatusSuccess));

            QVariantMap data;
            data["uuid"] = uuid;
            data["file_name"] = fileName;
            data["chunk_id"] = chunkId;
            response->setData(data);
        }

        return response;
    }

}
