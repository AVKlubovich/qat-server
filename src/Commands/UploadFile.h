#pragma once

#include "server-core/Commands/UserCommand.h"

namespace qat_server
{

    class UploadFile :
            public core::Command,
            public core::CommandCreator<UploadFile>
    {
        friend class QSharedPointer<UploadFile>;

    private:
        UploadFile(const Context& newContext);
    public:
        ~UploadFile() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
