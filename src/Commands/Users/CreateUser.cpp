#include "Common.h"
#include "CreateUser.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/ResponseGeneric.h"
#include "network-core/RequestsManager/Users/RequestCreateUser.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::CreateUser, "createUser")


namespace qat_server
{

    CreateUser::CreateUser(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> CreateUser::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        //decode incoming data
        auto incomingData = _context._packet.body();
        QSharedPointer<network::RequestCreateUser> requestCreateUserPtr(new network::RequestCreateUser(incomingData));
        requestCreateUserPtr->fromVariant(incomingData);

        //create command response
        QSharedPointer<network::ResponseGeneric> response(new network::ResponseGeneric(requestCreateUserPtr));

        QString userLogin = requestCreateUserPtr->login();
        QString userName = requestCreateUserPtr->name();
        QString userPassword = requestCreateUserPtr->password();
        QString userPosition = requestCreateUserPtr->position();
        QString userCompany = requestCreateUserPtr->company();
        QString userDepartment = requestCreateUserPtr->department();
        int idCompany;
        int idDepartment;

        qDebug() << "Create user: " << requestCreateUserPtr->login();

        if (userName.isNull() || userName.isEmpty())
        {
            response->setStatus(network::ResponseGeneric::StatusFail);
            response->setMessage("User name can not be empty.");
            return response;
        }

        QSharedPointer<database::DBWraper> wraper = database::DBManager::instance().getDBWraper();
        QSqlQuery query = wraper->query();

        // find id company
        const QString sqlQueryCompany = QString("SELECT id FROM overall_schema.company WHERE name = '%1'")
                .arg(userCompany);
        query.prepare(sqlQueryCompany);

        auto queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response->setStatus(network::ResponseGeneric::StatusFail);

            QString errMsg = QString("Database SQL query error, see server log file");
            response->setMessage(errMsg);
        }
        else
        {
            query.first();
            idCompany = query.value("id").toInt();
        }

        // find id department
        const QString sqlQueryDepartment = QString("SELECT id FROM overall_schema.departments "
                                                   "WHERE id_company = %1 AND name = '%2'")
                .arg(idCompany).arg(userDepartment);
        query.prepare(sqlQueryDepartment);

        queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response->setStatus(network::ResponseGeneric::StatusFail);

            QString errMsg = QString("Database SQL query error, see server log file");
            response->setMessage(errMsg);
        }
        else
        {
            query.first();
            idDepartment = query.value("id").toInt();
        }

        // insert new user
        const QString sqlQueryStr =
                    QString("INSERT INTO users_schema.users (name, login, password, officer, id_company, id_department) "
                            "VALUES ('%1', '%2', '%3', '%4', %5, %6);")
                    .arg(userName).arg(userLogin).arg(userPassword).arg(userPosition).arg(idCompany).arg(idDepartment);

        qDebug() << sqlQueryStr;

//        QVariantMap resultMap;
        queryResult = wraper->execQuery(sqlQueryStr);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response->setStatus(network::ResponseGeneric::StatusFail);

            QString errMsg = QString("Запись с таким логином существует.\nИзмените логин и продолжите сохранение");
            response->setMessage(errMsg);
        }
        else
        {
            response->setStatus(network::ResponseGeneric::StatusSuccess);
        }

        return response;
    }

}
