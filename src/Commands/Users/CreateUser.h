#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class CreateUser :
            public core::Command,
            public core::CommandCreator<CreateUser>
    {
        friend class QSharedPointer<CreateUser>;

    private:
        CreateUser(const Context& newContext);
    public:
        ~CreateUser() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
