#include "Common.h"
#include "DeleteUser.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/ResponseGeneric.h"
#include "network-core/RequestsManager/Users/RequestUpdateUser.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::DeleteUser, "delete_user")


namespace qat_server
{

    DeleteUser::DeleteUser(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> DeleteUser::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        auto & responce = _context._responce;
        responce->setHeaders(_context._packet.headers());

        auto data = _context._packet.body().toMap();
        auto incomingData = data["body"].toMap();

        auto listDepartments = permissions::UserPermission::instance().existCommand(incomingData["idUser"].toLongLong(), signature());
        if (!listDepartments.count())
        {
            sendPermissionsUserErorr("Нет прав для данной команды. \nОбратитесь к администратору",
                      "Permissions", signature());
            return QSharedPointer<network::Response>();
        }

        QVariantMap resultMap;
        QVariantMap result;
        QVariantMap head;

        head["type_command"] = signature();

        if(!incomingData.contains("id"))
        {
            head["status"] = "-1";
        }
        else
        {
            const auto wraper = database::DBManager::instance().getDBWraper();
            auto selectQuery = wraper->query();

            selectQuery.prepare("UPDATE users_schema.users SET \"isVisible\" = 1 "
                                          "WHERE id = :id");
            selectQuery.bindValue(":id", incomingData.value("id"));
            const auto selectResult = wraper->execQuery(selectQuery);
            if (!selectResult)
            {
                // TODO: need to add log
                qDebug() << __FUNCTION__ << "error:" << selectQuery.lastError();
                sendError(selectQuery.lastError().text(), "db_error", signature());
                //Q_ASSERT(false);

                return QSharedPointer<network::Response>();
            }
        }

        result["head"] = QVariant::fromValue(head);
        result["body"] = QVariant::fromValue(resultMap);
        responce->setBody(QVariant::fromValue(result));

        return QSharedPointer<network::Response>();
    }

}
