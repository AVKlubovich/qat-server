#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class DeleteUser :
            public core::Command,
            public core::CommandCreator<DeleteUser>
    {
        friend class QSharedPointer<DeleteUser>;

    private:
        DeleteUser(const Context& newContext);
    public:
        ~DeleteUser() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
