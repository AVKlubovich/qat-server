#include "Common.h"
#include "GetUserList.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/RequestUserList.h"
#include "network-core/RequestsManager/DefaultResponse.h"
#include "network-core/RequestsManager/ResponseServerError.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "permissions/UserPermission.h"

RegisterCommand(qat_server::GetUserList, "get_user_list")


namespace qat_server
{

    GetUserList::GetUserList(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> GetUserList::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        // NOTE: decode incoming data
        auto incomingData = _context._packet.body();
        auto body = incomingData.toMap()["body"].toMap();

        auto listDepartments = permissions::UserPermission::instance().existCommand(body["idUser"].toLongLong(), signature());
        if (!listDepartments.count())
        {
            sendError("Нет прав для данной команды. \nОбратитесь к администратору",
                      "Permissions", signature());
            return QSharedPointer<network::Response>();
        }

        QSharedPointer<network::RequestUserList> requestUserListPtr(new network::RequestUserList(incomingData));
        requestUserListPtr->fromVariant(incomingData);

        // NOTE: create database query
        QSharedPointer<database::DBWraper> wraper = database::DBManager::instance().getDBWraper();
        QSqlQuery query = wraper->query();

        // NOTE: prepare query
        const QString sqlQueryStr =
                "SELECT "
                "users.id, users.login, users.name, users.officer, departments.name AS department, "
                "company.name AS company, users.password "
                "FROM users_schema.users "
                "INNER JOIN overall_schema.departments ON (departments.id = users.id_department) "
                "INNER JOIN overall_schema.company ON (company.id = users.id_company) "
                "WHERE users_schema.users.\"isVisible\" = 0"
                ;

        query.prepare(sqlQueryStr);

        QSharedPointer<network::Response> response;

        // NOTE: run query
        const auto queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            QString errMsg = QString("Database SQL query error: %1").arg(query.lastError().text());
            qWarning() << errMsg;
            response.reset(new network::ResponseServerError(errMsg));
        }
        else
        {
            network::Users users;
            while (query.next())
            {
                //process user data
                network::User user;
                //user id
                user.setId(query.value(0).toLongLong());
                //user login
                user.setLogin(query.value(1).toString());
                //user name
                user.setName(query.value(2).toString());
                //user officer
                user.setOfficer(query.value(3).toString());
                //user department
                user.setDepartment(query.value(4).toString());
                //user company
                user.setCompany(query.value(5).toString());
                //user password
                user.setPassword(query.value(6).toString());
                users.push_back(user);
            }

            QSharedPointer<network::DefaultResponse> responseUserListPtr(
                        new network::DefaultResponse(requestUserListPtr->type(), users.toVariant(), requestUserListPtr));

            response = responseUserListPtr;
        }

        return response;
    }

}
