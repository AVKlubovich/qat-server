#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class GetUserList :
            public core::Command,
            public core::CommandCreator<GetUserList>
    {
        friend class QSharedPointer<GetUserList>;

        GetUserList(const Context& newContext);
    public:
        ~GetUserList() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
