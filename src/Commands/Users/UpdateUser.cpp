#include "Common.h"
#include "UpdateUser.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/ResponseGeneric.h"
#include "network-core/RequestsManager/Users/RequestUpdateUser.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(qat_server::UpdateUser, "updateUser")


namespace qat_server
{

    UpdateUser::UpdateUser(const Context& newContext)
        : Command(newContext)
    {
    }

    QSharedPointer<network::Response> UpdateUser::exec()
    {
        qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime() << endl;

        // NOTE: decode incoming data
        auto incomingData = _context._packet.body();
        QSharedPointer<network::RequestUpdateUser> requestUpdateUserPtr(new network::RequestUpdateUser(incomingData));
        requestUpdateUserPtr->fromVariant(incomingData);

        // NOTE: create command response
        QSharedPointer<network::ResponseGeneric> response(new network::ResponseGeneric(requestUpdateUserPtr));

        quint64 userId = requestUpdateUserPtr->id();
        QString userLogin = requestUpdateUserPtr->login();
        QString userName = requestUpdateUserPtr->name();
        QString userPassword = requestUpdateUserPtr->password();
        QString userPosition = requestUpdateUserPtr->position();
        QString userCompany = requestUpdateUserPtr->company();
        QString userDepartment = requestUpdateUserPtr->department();
        int idCompany;
        int idDepartment;

        qDebug() << "Create user: " << requestUpdateUserPtr->login();

        if (userName.isNull() || userName.isEmpty())
        {
            response->setStatus(network::ResponseGeneric::StatusFail);
            response->setMessage("User name can not be empty.");
            return response;
        }

        QSharedPointer<database::DBWraper> wraper = database::DBManager::instance().getDBWraper();
        QSqlQuery query = wraper->query();

        // find id company
        const QString sqlQueryCompany = QString("SELECT id FROM overall_schema.company WHERE name = '%1'")
                .arg(userCompany);
        query.prepare(sqlQueryCompany);

        auto queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response->setStatus(network::ResponseGeneric::StatusFail);

            QString errMsg = QString("Database SQL query error, see server log file");
            response->setMessage(errMsg);
        }
        else
        {
            query.first();
            idCompany = query.value("id").toInt();
        }

        // NOTE: find id department
        const QString sqlQueryDepartment = QString("SELECT id FROM overall_schema.departments "
                                                   "WHERE id_company = %1 AND name = '%2'")
                .arg(idCompany).arg(userDepartment);
        query.prepare(sqlQueryDepartment);

        queryResult = wraper->execQuery(query);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response->setStatus(network::ResponseGeneric::StatusFail);

            QString errMsg = QString("Database SQL query error, see server log file");
            response->setMessage(errMsg);
        }
        else
        {
            query.first();
            idDepartment = query.value("id").toInt();
        }

        // NOTE: update user
        const QString sqlQueryStr =
                    QString("UPDATE users_schema.users SET name = '%1', login = '%2', password = '%3'"
                            ", officer = '%4', id_company = %5, id_department = %6 "
                            "WHERE id = %7")
                    .arg(userName).arg(userLogin).arg(userPassword).arg(userPosition).arg(idCompany).arg(idDepartment).arg(userId);

        queryResult = wraper->execQuery(sqlQueryStr);
        if (!queryResult)
        {
            qDebug() << query.lastError();

            response->setStatus(network::ResponseGeneric::StatusFail);

            QString errMsg = QString("Database SQL query error, see server log file");
            response->setMessage(errMsg);
        }
        else
        {
            response->setStatus(network::ResponseGeneric::StatusSuccess);
        }

        return response;
    }

}
