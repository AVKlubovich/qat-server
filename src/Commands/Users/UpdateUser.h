#pragma once

#include "server-core/Commands/UserCommand.h"

namespace qat_server
{

    class UpdateUser :
            public core::Command,
            public core::CommandCreator<UpdateUser>
    {
        friend class QSharedPointer<UpdateUser>;

    private:
        UpdateUser(const Context& newContext);
    public:
        ~UpdateUser() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
