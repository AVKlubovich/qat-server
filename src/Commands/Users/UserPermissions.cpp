#include "Common.h"
#include "UserPermissions.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "network-core/Packet/JsonConverter.h"

RegisterCommand(qat_server::UserPermissions, "get_user_permissions")


using namespace qat_server;

UserPermissions::UserPermissions(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> UserPermissions::exec()
{
    auto& responce = _context._responce;
    responce->setHeaders(_context._packet.headers());

    auto data = _context._packet.body().toMap();
    auto incomingData = data["body"].toMap();

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto query = wraper->query();

    const auto idUser = incomingData["id_user"];

    auto sQuery = QString(
                "SELECT id_department, id_command FROM users_schema.permissions WHERE id_user = :idUser"
                );

    query.prepare(sQuery);
    query.bindValue(":idUser", idUser);
    if (!wraper->execQuery(query))
    {
        qDebug() << query.lastError();
        Q_ASSERT(true);
    }

    const auto resultList = database::DBHelpers::queryToVariant(query);

    QVariantMap head;
    head["type"] = signature();
    head["status"] = 1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultList);

    responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}
