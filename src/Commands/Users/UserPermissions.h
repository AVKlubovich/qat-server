#pragma once

#include "server-core/Commands/UserCommand.h"


namespace qat_server
{

    class UserPermissions
        : public core::Command
        , public core::CommandCreator<UserPermissions>
    {
        friend class QSharedPointer<UserPermissions>;

        UserPermissions(const Context& newContext);
    public:
        ~UserPermissions() override = default;

        QSharedPointer<network::Response> exec() override;
    };

}
