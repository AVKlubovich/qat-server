#include "Common.h"
#include "Core.h"

#include "server-core/Server/Server.h"
#include "server-core/Commands/CommandFactory.h"

#include "Request/ServerRequestHandler.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"

#include "utils/Logging/Logger.h"
#include "utils/Logging/Devices/FileDevice.h"
#include "utils/Logging/Devices/DebuggerDevice.h"
#include "utils/Logging/Devices/ConsoleDevice.h"

#include "database/DBManager.h"

#include "permissions/PermissionManager.h"
#include "permissions/PermissionDownloader.h"
#include "permissions/UserPermission.h"

#include "web-exchange/WebRequestManager.h"

#include "SmsSender/SmsSender.h"


using namespace qat_server;

Core::Core()
{
}

bool Core::init()
{
    readConfig();

    if (!initLoger())
    {
        qWarning() << "Could not initialize logger";
        return false;
    }

    if (!initServer())
    {
        qWarning() << "Could not initialize server";
        return false;
    }

    if (!initDBManager())
    {
        qWarning() << "Could not initialize dbmanager";
        return false;
    }

    if (!initPermissions())
    {
        qWarning() << "Could not initialize users permissions";
        return false;
    }

    if (!initCommandFactory())
    {
        qWarning() << "Could not initialize command factory";
        return false;
    }

    if (!initWebManager())
    {
        qWarning() << "Could not initialize web manager";
        return false;
    }

    if (!initSmsSender())
    {
        qWarning() << "Could not initialize sms sender";
        return false;
    }

    qDebug() << "Core is initialize";
    return true;
}

void Core::done()
{
    _permissionManager.reset();
    _server.reset();
    _logger.reset();
}

void Core::readConfig()
{
    QDir::setCurrent(QCoreApplication::applicationDirPath());

    utils::Settings::Options config = { "configuration/server.ini", true };
    utils::SettingsFactory::instance().registerSettings("server", config);

    auto settings = utils::SettingsFactory::instance().settings("server");
    settings =
    {
        // FastCGI
        { "FastCGI/ConnectionString", "localhost:26000" },
        { "FastCGI/Backlog", 1 },      // Number of requests in queue
        { "FastCGI/MaxJobs", 1 },      // Maximum number of jobs
        { "FastCGI/LogTraffic", true },// Logging of HTTP traffic enabled

        // Multi-threading (0 - auto detect)
        { "MultiThreading/FastThreadCound", 0 },
        { "MultiThreading/LongThreadCound", 0 },

        // Logs
        { "Log/FlushInterval", 1000 },
        { "Log/PrefixName", "qat_server.log" },
        { "Log/Dir", "./logs/" },
        { "Log/MaxSize", 134217728 },   // 100 mb

        // DBManager
        { "DBManager/Host", "192.168.202.222" },
        { "DBManager/Name", "okk_db" },
        { "DBManager/Type", "QPSQL" },
        { "DBManager/Port", 5432 },
        { "DBManager/User", "postgres" },
        { "DBManager/Password", "11" },

        // SmsSender
        { "SmsSender/Interval", 5000 },

        // WebRequestManager
        { "ApiGeneral/Login", "okk" },
    #ifdef QT_DEBUG
        { "ApiGeneral/Password", "ufhe_e7bbbewGhed16t" },
    #else
        { "ApiGeneral/Password", "" },
    #endif
    };

    utils::SettingsFactory::instance().setCurrentSettings("server");

#ifdef QT_DEBUG
    utils::Settings::Options config222 = { "configuration/server_222.ini", true };
    utils::SettingsFactory::instance().registerSettings("server_222", config222);

    utils::SettingsFactory::instance().setCurrentSettings("server_222");
#endif
}

bool Core::initLoger()
{
    return true;
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Log");

    _logger = QSharedPointer<utils::Logger>::create();

    const auto loggerOptions = QSharedPointer<utils::LoggerMessages::Options>::create();
    loggerOptions->timerInterval = settings["FlushInterval"].toInt();
    if (!_logger->init(loggerOptions))
        return false;

    // FileDevice
    const auto fileOptions = QSharedPointer<utils::FileDevice::FileOptions>::create();
    fileOptions->maxSize = settings["MaxSize"].toLongLong();
    fileOptions->prefixName = settings["PrefixName"].toString();
    fileOptions->directory = settings["Dir"].toString();

    if (!_logger->addDevice(fileOptions))
        return false;

    // DebuggerDevice
    const auto debuggerDevice = QSharedPointer<utils::DebuggerDevice::DebuggerOptions>::create();

    if (!_logger->addDevice(debuggerDevice))
        return false;

    // ConsoleDevice
    const auto consoleDevice = QSharedPointer<utils::ConsoleDevice::ConsoleOptions>::create();

    if (!_logger->addDevice(consoleDevice))
        return false;

    settings.endGroup();
    qDebug() << "Logger was init";
    return true;
}

bool Core::initServer()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();

    _server = core::ServerShp::create();

    core::Server::Options options;
    options.balancerOptions.fastThreadCount = settings.value("MultiThreading/FastThreadCound").toInt();
    options.balancerOptions.longThreadCount = settings.value("MultiThreading/LongThreadCound").toInt();
    options.fastCgiOptions.path = settings.value("FastCGI/ConnectionString").toString().toLatin1();
    options.fastCgiOptions.backlog = settings.value("FastCGI/Backlog").toInt();
    options.fastCgiOptions.maxConnections = settings.value("FastCGI/MaxJobs").toInt();
    options.fastCgiOptions.logTraffic = settings.value("FastCGI/LogTraffic").toBool();

    const auto factory = QSharedPointer<ServerRequestHandlerFactory>::create();
    _server->setHandlerFactory(factory);
    qDebug() << "handler factory is created";

    _server->start(options);
    if (!_server->isStarted())
        return false;

    qDebug() << "Server  was init";
    return true;
}

bool Core::initDBManager()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("DBManager");

    database::DBManager::DBSettings dbSettings;
    dbSettings.hostName = settings["Host"].toString();
    dbSettings.database = settings["Name"].toString();
    dbSettings.dbType = settings["Type"].toString();
    dbSettings.port = settings["Port"].toInt();
    dbSettings.user = settings["User"].toString();
    dbSettings.password = settings["Password"].toString();
    auto& dbManager = database::DBManager::instance();
    dbManager.initSettings(dbSettings);

    qDebug() << "DBManager  was init";
    return true;
}

bool Core::initPermissions()
{
    // NOTE: temporary solution for PhotoValidator
//    return true;

    qDebug() << "Permissions was init";
//    _permissionManager = permissions::PermissionManagerShp::create();
//    permissions::PermissionDownloader pDownloader;
//    const auto result = pDownloader.downloadePermissions(_permissionManager);

    const auto result = permissions::UserPermission::instance().setUsersPermissions();
    return result;
}

bool Core::initCommandFactory()
{
    qDebug() << "CommandFactory  was init";
    core::CommandFactory::instance().registerSignatures();
    return true;
}

bool Core::initWebManager()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("ApiGeneral");

    const auto login = settings["Login"].toString();
    const auto password = settings["Password"].toString();
    const auto url = settings["Url"].toString();

    network::WebRequestManager::createSingleton();
    const auto& webManager = network::WebRequestManager::instance();
    webManager->setCredentials(login, password);
    webManager->setUrl(url);

    webManager->init();

    qDebug() << "WebManager was init";

    return webManager->isInitialized();
}

bool Core::initSmsSender()
{
    auto& sender = SmsSender::instance();
    if (!sender.run())
    {
        return false;
    }

    QThread* smsThread = new QThread();
    sender.moveToThread(smsThread);
    QObject::connect(smsThread, &QThread::finished, &sender, &QObject::deleteLater);
    smsThread->start();

    qDebug() << "SmsSender was init";

    return true;
}
