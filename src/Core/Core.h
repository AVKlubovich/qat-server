#pragma once

#include "server-core/Core/Core.h"
#include "utils/BaseClasses/Singleton.h"


namespace utils
{
    class Logger;
    typedef QSharedPointer<Logger> LoggerShp;
}

namespace permissions
{
    class PermissionManager;
    typedef QSharedPointer<PermissionManager> PermissionManagerShp;
}

namespace core
{
    class Server;
    typedef QSharedPointer<Server> ServerShp;

    class RequestHandlerFactory;
    typedef QSharedPointer<RequestHandlerFactory> RequestHandlerFactoryShp;
}

namespace qat_server
{

    class Core : public utils::Singleton<Core>
    {
    public:
        Core();
        ~Core() = default;

        bool init();
        void done();

    private:
        void readConfig();
        bool initLoger();
        bool initServer();
        bool initDBManager();
        bool initPermissions();
        bool initCommandFactory();
        bool initWebManager();
        bool initSmsSender();

    private:
        utils::LoggerShp _logger;
        core::ServerShp _server;

        permissions::PermissionManagerShp _permissionManager;
    };

}
