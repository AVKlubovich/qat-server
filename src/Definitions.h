#pragma once


#define ID_DEP_QAT 1

#define SMS_TABLE "complaints_schema.sms_by_complaints"
#define COMPANY_ID_FOR_SEND_SMS 3


namespace qat_server
{

    enum Statuses
    {
        _new = 1,
        in_the_work,
        demolished,
        it_requires_notification,
        is_closed
    };

    enum Workers
    {
        Driver = 1,
        Operator,
        Logist
    };

}
