#include "Common.h"
#include "SmsSender.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange/WebRequest.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"

#include "Definitions.h"


using namespace qat_server;

SmsObject::SmsObject(const quint64 id, const quint64 complaintId, const QString& phoneNumber,
                     const QString& smsText, const QDateTime& time, const quint32 idCompany)
    : _id(id)
    , _complaintId(complaintId)
    , _phoneNumber(phoneNumber)
    , _smsText(smsText)
    , _time(time)
    , _idCompany(idCompany)
{
}

bool SmsObject::checkTime() const
{
    if (_time < QDateTime::currentDateTime())
    {
        return true;
    }

    return false;
}


SmsSender::SmsSender()
    : QObject(nullptr)
{
}

bool SmsSender::run()
{
    if (!readSmsFromDB())
    {
        return false;
    }

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("SmsSender");
    const quint64 interval = settings["Interval"].toULongLong();

    _timer.start(interval);
    connect(&_timer, &QTimer::timeout, this, &SmsSender::checkSms);

    connect(this, &SmsSender::sendSmsSignal, this, &SmsSender::sendSms, Qt::QueuedConnection);

    return true;
}

void SmsSender::sendSms(const quint64 complaintId, const QString& phoneNumber, const QString& smsText,
                        const QDateTime& time, const quint32 companyId, const quint32 typeMessage, const double mark)
{
    if (thread() != QThread::currentThread())
    {
        emit sendSmsSignal(complaintId, phoneNumber, smsText, time, companyId, typeMessage, mark);
        return;
    }

    const database::DBWraperShp& dbWraper = database::DBManager::instance().getDBWraper();

    const QString& insertSmsStr = QString(
        "INSERT INTO %1 "
        "(id_complaint, phone_number, sms_text, time_of_send, id_company, type_message, mark) "
        "VALUES "
        "(:complaintId, :phoneNumber, :smsText, :timeOfSend, :idCompany, :type, :mark)"
        ).arg(SMS_TABLE);

    QSqlQuery insertSmsQuery = dbWraper->query();
    insertSmsQuery.prepare(insertSmsStr);
    insertSmsQuery.bindValue(":complaintId", complaintId);
    insertSmsQuery.bindValue(":phoneNumber", phoneNumber);
    insertSmsQuery.bindValue(":smsText", smsText);
    insertSmsQuery.bindValue(":timeOfSend", QVariant::fromValue(time));
    insertSmsQuery.bindValue(":idCompany", companyId);
    insertSmsQuery.bindValue(":type", typeMessage);
    insertSmsQuery.bindValue(":mark", mark);

    if (!insertSmsQuery.exec())
    {
        qDebug() << __FUNCTION__ << "Can not insert sms";
        qDebug() << insertSmsQuery.lastError().text();
        qDebug() << insertSmsQuery.lastQuery();
        return;
    }

    const quint64 lastId = insertSmsQuery.lastInsertId().toULongLong();

    SmsObject sms(lastId, complaintId, phoneNumber, smsText, time, companyId);
    _sms << sms;
}

void SmsSender::checkSms()
{
    if (_flagSendSMS)
        return;

    _flagSendSMS = true;

    int i = 0;
    while (i < _sms.count())
    {
        const SmsObject& sms = _sms.at(i);
        if (sms.checkTime())
        {
            sendToServer(sms);
            _sms.removeAt(i);
            continue;
        }

        ++i;
    }

    _flagSendSMS = false;
}

bool SmsSender::readSmsFromDB()
{
    const database::DBWraperShp& dbWraper = database::DBManager::instance().getDBWraper();

    const QString& selectSmsStr = QString(
        "SELECT * "
        "FROM %1 "
        "WHERE status = 0"
        ).arg(SMS_TABLE);

    QSqlQuery selectSmsQuery = dbWraper->query();
    selectSmsQuery.prepare(selectSmsStr);

    if (!selectSmsQuery.exec())
    {
        qDebug() << __FUNCTION__ << "Can not read sms";
        qDebug() << selectSmsQuery.lastError().text();
        qDebug() << selectSmsQuery.lastQuery();
        return false;
    }

    const QList<QVariantMap> results = database::DBHelpers::queryToVariantMap(selectSmsQuery);
    for (const QVariantMap& smsData : results)
    {
        const quint64 smsId = smsData["id"].toULongLong();
        const quint64 complaintId = smsData["id_complaint"].toULongLong();
        const QString phoneNumber = smsData["phone_number"].toString();
        const QString smsText = smsData["sms_text"].toString();
        const QDateTime timeOfSend = QDateTime::fromString(smsData["time_of_send"].toString(), "yyyy-MM-dd hh:mm:ss");
        const quint32 idCompany = smsData["id_company"].toUInt();

        const SmsObject sms(smsId, complaintId, phoneNumber, smsText, timeOfSend, idCompany);
        _sms << sms;
    }

    return true;
}

void SmsSender::sendToServer(const SmsObject& sms)
{
    const database::DBWraperShp& dbWraper = database::DBManager::instance().getDBWraper();

#ifdef QT_DEBUG
    QString url;
    switch (sms.getIdCompany()) {
        case 3:
            url = "http://192.168.211.30:81/api/api_general_taxi_spb.php";
            break;
        default:
            url = "http://192.168.211.30:81/api/api_general_gruz_spb.php";
            break;
    }
#else
    const auto selectUrlStr = QString(
        "SELECT api "
        "FROM overall_schema.company "
        "WHERE id = :companyId");
    auto selectUrlQuery = dbWraper->query();
    selectUrlQuery.prepare(selectUrlStr);
    selectUrlQuery.bindValue(":companyId", COMPANY_ID_FOR_SEND_SMS);

    if (!selectUrlQuery.exec())
    {
        qDebug() << __FUNCTION__ << "Can not read url";
        qDebug() << selectUrlQuery.lastError().text();
        return;
    }

    const auto resultList = database::DBHelpers::queryToVariant(selectUrlQuery);
    if (resultList.isEmpty())
    {
        qDebug() << __FUNCTION__ << "Urls list is empty";
        return;
    }

    const auto url = resultList.first().toMap().value("api").toString();
#endif


    auto webManager = network::WebRequestManager::instance();
    auto webRequest = network::WebRequestShp::create("type_query");

    QVariantMap userData;
    userData["type_query"] = "send_sms_api";
    userData["sms_text"] = sms.getSmsText();
    userData["sms_phone"] = sms.getPhoneNumber();
    webRequest->setArguments(userData);
    webRequest->setUrl(url);
    webRequest->setCallback(nullptr);
    webRequest->setEncrypt(true);

    webManager->sendRequestCurrentThread(webRequest);

    const auto data = webRequest->reply();
    webRequest->release();

    const auto& doc = QJsonDocument::fromJson(data);
    const auto& jObj = doc.object();
    const auto& map = jObj.toVariantMap();

    if (!map.contains("status"))
    {
        qDebug() << __FUNCTION__ << "error: Bad response from remote server";
        return;
    }

    const auto status = map["status"].toInt();
    if (status != 1)
    {
        QString errorStr;
        if (map.contains("error"))
        {
            const auto& errorList = map["error"].toList();
            errorStr = errorList.first().toString();
        }
        else
        {
            errorStr = QString("Неизвестная ошибка");
        }
        qDebug() << errorStr;
        return;
    }

    const QString& updateSmsStr = QString(
        "UPDATE %1 "
        "SET status = 1, time_of_send = now() "
        "WHERE id = :smsId"
        ).arg(SMS_TABLE);

    QSqlQuery updateSmsQuery = dbWraper->query();
    updateSmsQuery.prepare(updateSmsStr);
    updateSmsQuery.bindValue(":smsId", sms.getId());

    if (!updateSmsQuery.exec())
    {
        qDebug() << QString("Can not change status from 0 to 1 for sms id = %1").arg(sms.getId());
        return;
    }
}
