#pragma once

#include "utils/BaseClasses/Singleton.h"


namespace qat_server
{

    class SmsObject
    {
    public:
        SmsObject(const quint64 id, const quint64 complaintId, const QString& phoneNumber,
                  const QString& smsText, const QDateTime& time, const quint32 idCompany);
        ~SmsObject() = default;

        inline const quint64 getId() const {return _id;}
        inline const quint64 getComplaintId() const {return _complaintId;}
        inline const quint32 getIdCompany() const {return _idCompany;}
        inline const QString getPhoneNumber() const {return _phoneNumber;}
        inline const QString getSmsText() const {return _smsText;}

        bool checkTime() const;

    private:
        quint64 _id;
        quint64 _complaintId;
        QString _phoneNumber;
        QString _smsText;
        QDateTime _time;
        quint32 _idCompany;
    };

    class SmsSender : public QObject, public utils::Singleton<SmsSender>
    {
        Q_OBJECT

        friend class utils::Singleton<SmsSender>;
        friend class QSharedPointer<SmsSender>;

    public:
        SmsSender();
        virtual ~SmsSender() = default;

        bool run();

    public slots:
        void sendSms(const quint64 complaintId, const QString& phoneNumber, const QString& smsText,
                     const QDateTime& time, const quint32 companyId, const quint32 typeMessage = 0, const double mark = 0);

    signals:
        void sendSmsSignal(const quint64 complaintId, const QString& phoneNumber, const QString& smsText,
                           const QDateTime& time, const quint32 companyId, const quint32 typeMessage = 0, const double mark = 0);

    private slots:
        void checkSms();

    private:
        bool readSmsFromDB();
        void sendToServer(const SmsObject& sms);

    private:
        QTimer _timer;
        QList<SmsObject> _sms; // number, text
        bool _flagSendSMS = false;
    };

}
