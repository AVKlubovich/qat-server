<?php

class Commands
{
    const ADD_FILE = "add_file";
    const ADD_FILE_REVIEW = "add_file_review";
    const REMOVE_FILE = "remove_file";
    const GET_DRIVER_PHOTO = "get_driver_photo";
    const GET_DRIVER_CV = "get_driver_cv";
}

?>