<?php

class AddFileReview extends BaseFile
{
    public function exec(&$assoc)
    {
        print_r($assoc);

        $url_files = "http://$_SERVER[SERVER_ADDR]/".Config::TARGET_DIR_FILES;

        $is_send_file = false;
        foreach ($_FILES as $name=>$file)
        {
            if ($name == "uploaded_file" &&
                !empty($file["size"]))
            {
                $is_send_file = true;
                break;
            }
        }

        if (!$is_send_file)
        {
            $assoc["status"] = 1;
            $assoc["type_command"] = "insert_review";

            $result = NetworkManager::sendJson(Config::HOST_CPP, $assoc);
            if ($result == Errors::NOT_SEND_DATA_TO_REMORE_SERVER)
            {
                Utils::printData(
                    array('status' => $result,
                        'error' => Errors::instance()->data($result)));
            }

            echo $result;
            exit();
        }

        $files = array();
        foreach ($_FILES as $name=>$file)
        {
            if (is_array($file["name"]))
            {
                $nameArray = $file["name"];
                for ($i = 0; $i < count($nameArray); ++$i) {
                    $orig_file_name = $nameArray[$i];
                    $fileInfo = array
                    (
                        "name" => $orig_file_name,
                        "tmp_name" => $file["tmp_name"][$i],
                        "size" => $file["size"][$i]
                    );

                    $file_name = $this->saveFile(Config::TARGET_DIR_FILES, $fileInfo);
                    $file_url = "$url_files$file_name";

                    $fileArray = array();
                    $fileArray["original_name"] = $orig_file_name;
                    $fileArray["file_name"] =  $file_name;
                    $fileArray["file_url"] =  $file_url;

                    array_push($files, $fileArray);
                }
            }
            else
            {
                $orig_file_name = $file["name"];
                $file_name = $this->saveFile(Config::TARGET_DIR_FILES, $file);
                $file_url = "$url_files$file_name";

                $fileArray = array();
                $fileArray["original_name"] = $orig_file_name;
                $fileArray["file_name"] =  $file_name;
                $fileArray["file_url"] =  $file_url;

                array_push($files, $fileArray);
            }
        }

        $assoc["files"] = $files;
        $assoc["status"] = 1;
        $assoc["type_command"] = "insert_review";

        $result = NetworkManager::sendJson(Config::HOST_CPP, $assoc);
        if ($result == Errors::NOT_SEND_DATA_TO_REMORE_SERVER)
        {
            Utils::printData(
                array('status' => $result,
                    'error' => Errors::instance()->data($result)));
        }

        echo $result;
        exit();
    }
}

?>